import { Pool } from 'pg';

const connectionString = process.env.POSTGRES_DATABASE;
const ssl = process.env.NODE_ENV === 'production';

// export const dbPool = new Pool({ connectionString, ssl: true, max: 20 });
export const dbPool = new Pool({
  connectionString,
  ssl,
  max: 100,
  connectionTimeoutMillis: 10000,
  idleTimeoutMillis: 10000,
});

export const query = (text, params) => dbPool.query(text, params);

export const closeDbConnections = () => dbPool.end();
