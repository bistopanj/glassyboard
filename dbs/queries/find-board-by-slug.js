const findBoardBySlugQuery = `
WITH
  the_board AS (
    SELECT
      *
    FROM
      boards
    WHERE
      slug = ($1)
  ),
  the_user AS (
    SELECT
      *
    FROM
      users
    WHERE
      ex_id = ($2)
  ),
  board_answers_extended AS (
    SELECT
      a.*,
      COALESCE( 
        (a.created_by = (SELECT id FROM the_user) )
          OR
        ((SELECT email FROM the_user) = 'farid.khaheshi@gmail.com')
        , FALSE
      ) AS editable,
      count(CASE WHEN up.active THEN 1 END) AS upvotes,
      count(CASE WHEN tu.id IS NOT NULL THEN 1 END) > 0 AS upvoted
    FROM
      answers a
        JOIN
          the_board tb
            ON a.board_id = tb.id
        LEFT JOIN
          upvotes up
            ON a.id = up.answer_id AND up.active
        LEFT JOIN
          the_user tu
            ON tu.id = up.created_by
    WHERE
      a.active = TRUE
    GROUP BY
      a.id
  )

SELECT
  bb.ex_id AS id,
  bb.slug,
  bb.topic, 
  COALESCE( 
    (bb.created_by = (SELECT id FROM the_user) )
      OR
    ((SELECT email FROM the_user) = 'farid.khaheshi@gmail.com')
    , FALSE
  ) AS editable,
  bb.created_at,
  bb.updated_at,
  COALESCE (
    json_agg(
      json_build_object(
        'id', bae.ex_id,
        'answer', bae.answer,
        'created_at', bae.created_at,
        'updated_at', bae.updated_at,
        'editable', bae.editable,
        'upvotes', bae.upvotes,
        'upvoted', bae.upvoted
      ) ORDER BY bae.created_at DESC
    ) FILTER (
        WHERE bae.id IS NOT NULL
    )
    , '[]'
  ) AS answers
FROM
  the_board bb
    LEFT JOIN
      board_answers_extended bae
        ON  bb.id = bae.board_id
GROUP BY
  bb.ex_id,
  bb.slug,
  bb.topic, 
  bb.created_at,
  bb.updated_at,
  bb.created_by
;
`;

export default findBoardBySlugQuery;
