const insertCompanyQuery = `
WITH 
  input AS 
  (
    SELECT
      ($1) AS subdomain,
      (($1)||'-')::text AS subdomain1
  ),
  free_subd AS
  (
    SELECT
      i.subdomain
    FROM
      input i
        LEFT JOIN
          companies c 
            USING (subdomain)
    WHERE
      c.subdomain IS NULL

    UNION ALL
    (
      SELECT
        i.subdomain1 || COALESCE(right(c.subdomain, length(i.subdomain1)*-1)::int + 1, 1)
      FROM
        input i
          LEFT JOIN
            companies c
              ON c.subdomain LIKE (i.subdomain1 || '%')
                AND
                  right(c.subdomain, length(i.subdomain1) * -1) ~ '^\\d+$'
      ORDER BY right(c.subdomain, length(i.subdomain1) * -1)::int DESC
    )
    LIMIT 1
  )
INSERT INTO
  companies
  (
    subdomain,
    created_by
  )
  VALUES
  (
    (SELECT subdomain FROM free_subd),
    (SELECT id FROM users WHERE ex_id=($2))
  )
RETURNING
  ex_id AS id,
  subdomain,
  ($2) AS created_by,
  primary_board,
  created_at,
  updated_at
;
`;

export default insertCompanyQuery;
