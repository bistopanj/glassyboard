const deactivateAnswerQuery = `
UPDATE
  answers
SET
  active = FALSE
WHERE
  ex_id = ($1)
  AND
  (
    ($2) = uuid('00000000-0000-0000-0000-000000000000')
      OR
    created_by = (
      SELECT
        id
      FROM
        users
      WHERE
        ex_id = ($2)
    )
  )
RETURNING
  ex_id as id,
  answer,
  active,
  ($2) AS updated_by,
  created_at,
  updated_at
`;

export default deactivateAnswerQuery;
