const upsertPanelBoardRelQuery = `
INSERT INTO
  panel_board
  (
    board_id,
    panel_id,
    created_by,
    active
  )
VALUES
  (
    (SELECT id FROM boards WHERE slug = ($1)),
    (SELECT id FROM panels WHERE ex_id = ($2)),
    (SELECT id FROM users WHERE ex_id = ($3)),
    TRUE
  )
ON CONFLICT ON CONSTRAINT max_one_per_board_per_panel
DO
  UPDATE
  SET
    active = TRUE AND panel_board.created_by = EXCLUDED.created_by
RETURNING
  ex_id AS id,
  ($1) AS slug,
  ($2) AS panel_id,
  ($3) AS created_by,
  active,
  created_at,
  updated_at
`;

export default upsertPanelBoardRelQuery;
