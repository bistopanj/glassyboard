import findBoardByExIdQuery from './find-board-by-ex-id';
import findAnswerByExIdQuery from './find-answer-by-ex-id';
import findPanelByExIdQuery from './find-panel-by-ex-id';
import findBoardBySlugQuery from './find-board-by-slug';
import findBoardBySubdomainQuery from './find-board-by-sumdomain';
import findUpvoteByExIdQuery from './find-upvote-by-ex-id';
import findCompanyByExIdQuery from './find-company-by-ex-id';
import findPanelBoardRelByExIdQuery from './find-panel-board-rel-by-ex-id';
import deactivatePanelBoardRelQuery from './deactivate-panel-board-rel';
import deactivateAnswerQuery from './deactivate-answer';
import insertBoardQuery from './insert-board';
import insertCompanyBoardQuery from './insert-company-board';
import insertAnswerQuery from './insert-answer';
import insertUpvoteQuery from './insert-upvote';
import insertPanelQuery from './insert-panel';
import insertCompanyQuery from './insert-company';
import upsertUserQuery from './upsert-user';
import upsertPanelBoardRelQuery from './upsert-panel-board-rel';
import updateBoardBySlugQuery from './update-board-by-slug';
import updatePanelByExIdQuery from './update-panel-by-ex-id';
import updateCompanyPrimaryBoardQuery from './update-company-primary-board';
import deleteBoardByExIdQuery from './delete-board-by-ex-id';
import deleteAnswerByExIdQuery from './delete-answer-by-ex-id';
import deleteUpvoteByExIdQuery from './delete-upvote-by-ex-id';
import deleteCompanyByExIdQuery from './delete-company-by-ex-id';
import deletePanelByExIdQuery from './delete-panel-by-ex-id';
import deletePanelBoardRelByExIdQuery from './delete-panel-board-rel-by-ex-id';
import getFeaturedPanelQuery from './get-featured-panel';
import getFeaturedPanelsQuery from './get-featured-panels';

export {
  findBoardByExIdQuery,
  findAnswerByExIdQuery,
  findBoardBySlugQuery,
  findBoardBySubdomainQuery,
  findUpvoteByExIdQuery,
  findPanelByExIdQuery,
  findPanelBoardRelByExIdQuery,
  findCompanyByExIdQuery,
  deactivatePanelBoardRelQuery,
  deactivateAnswerQuery,
  insertBoardQuery,
  insertCompanyBoardQuery,
  insertAnswerQuery,
  insertUpvoteQuery,
  insertPanelQuery,
  insertCompanyQuery,
  upsertUserQuery,
  upsertPanelBoardRelQuery,
  updateBoardBySlugQuery,
  updatePanelByExIdQuery,
  updateCompanyPrimaryBoardQuery,
  deleteCompanyByExIdQuery,
  deleteBoardByExIdQuery,
  deletePanelBoardRelByExIdQuery,
  deleteAnswerByExIdQuery,
  deleteUpvoteByExIdQuery,
  deletePanelByExIdQuery,
  getFeaturedPanelQuery,
  getFeaturedPanelsQuery,
};
