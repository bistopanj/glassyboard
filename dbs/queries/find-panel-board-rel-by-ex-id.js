const findPanelBoardRelByExIdQuery = `
SELECT
  pb.ex_id AS id,
  u.ex_id AS created_by,
  b.ex_id AS board_id,
  b.slug AS board_slug,
  p.ex_id AS panel_id,
  pb.active,
  pb.created_at,
  pb.updated_at
FROM
  panel_board pb
    JOIN
      users u
        ON pb.created_by = u.id
    JOIN
      boards b
        ON pb.board_id = b.id
    JOIN
      panels p
        ON pb.panel_id = p.id
WHERE
  pb.ex_id=($1)
`;

export default findPanelBoardRelByExIdQuery;
