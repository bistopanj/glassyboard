const getFeaturedPanelQuery = `
WITH 
  top_boards AS (
    SELECT *
    FROM boards
    WHERE active = true
    ORDER BY created_at DESC
    LIMIT 5
  ),
  the_user AS (
    SELECT
      *
    FROM
      users
    WHERE
      ex_id = ($1)
  ),
  board_answers_extended AS (
    SELECT
      a.*,
      count(CASE WHEN up.active THEN 1 END) AS upvotes,
      count(CASE WHEN tu.id IS NOT NULL THEN 1 END) > 0 AS upvoted
    FROM
      answers a
        JOIN
          top_boards tb
            ON a.board_id = tb.id
        LEFT JOIN
          upvotes up
            ON a.id = up.answer_id AND up.active
        LEFT JOIN
          the_user tu
            ON tu.id = up.created_by
    GROUP BY
      a.id
  )
SELECT
  bb.ex_id AS id,
  bb.slug,
  bb.topic, 
  bb.created_at,
  bb.updated_at,
  COALESCE (
    json_agg(
      json_build_object(
        'id', bae.ex_id,
        'answer', bae.answer,
        'created_at', bae.created_at,
        'updated_at', bae.updated_at,
        'upvotes', bae.upvotes,
        'upvoted', bae.upvoted
      ) ORDER BY bae.created_at DESC
    ) FILTER (
        WHERE bae.id IS NOT NULL
    )
    , '[]'
  ) AS answers
FROM
  top_boards bb
    LEFT JOIN
      board_answers_extended bae
        ON bb.id = bae.board_id
GROUP BY
  bb.ex_id,
  bb.slug,
  bb.topic, 
  bb.created_at,
  bb.updated_at
ORDER BY bb.created_at DESC
;
`;

const old = `
SELECT
  b.ex_id AS id,
  b.slug,
  b.topic,
  b.created_at,
  b.updated_at,
  COALESCE (
    json_agg(
      json_build_object(
        'id', a.ex_id,
        'answer', a.answer,
        'created_at', a.created_at,
        'updated_at', a.updated_at
        )
        ORDER BY a.created_at DESC
    ) FILTER (
      WHERE a.id IS NOT NULL
    )
    , '[]'
  ) AS answers
FROM boards b
  LEFT JOIN answers a
    ON a.board_id = b.id
  WHERE b.id IN (
    SELECT id
    FROM boards
    WHERE active = true
    ORDER BY created_at DESC
    LIMIT 5
  )
  GROUP BY b.id
ORDER BY b.created_at DESC
;
`;

export default getFeaturedPanelQuery;
