const insertAnswerQuery = `
INSERT INTO answers
  (
    answer,
    created_by,
    board_id
  )
VALUES
  ( 
    ($1), 
    (SELECT id FROM users WHERE ex_id=($2)), 
    (SELECT id FROM boards WHERE ex_id=($3))
  )
RETURNING
*,
TRUE AS editable;
`;

export default insertAnswerQuery;
