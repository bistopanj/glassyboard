const deletePanelBoardRelByExIdQuery = `
DELETE FROM
  panel_board
WHERE
  ex_id=($1)
`;

export default deletePanelBoardRelByExIdQuery;
