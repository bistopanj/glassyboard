const deletePanelByExIdQuery = `
DELETE FROM
  panels
WHERE
  ex_id=($1)
`;

export default deletePanelByExIdQuery;
