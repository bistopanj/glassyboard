const deactivatePanelBoardRelQuery = `
UPDATE
  panel_board
SET
  active = FALSE
WHERE
  board_id = ( SELECT id FROM boards WHERE slug = ($1) )
    AND
  panel_id = ( SELECT id FROM panels WHERE ex_id = ($2) )
RETURNING
  ex_id AS id,
  ($1) AS slug,
  ($2) AS panel_id,
  active,
  created_at,
  updated_at
`;

export default deactivatePanelBoardRelQuery;
