const findUpvoteByExIdQuery = `
SELECT 
  u.ex_id AS id,
  u.active,
  u.created_at,
  u.updated_at,
  us.ex_id AS created_by,
  b.ex_id AS board_id,
  a.ex_id AS answer_id
FROM upvotes u
  JOIN users us
    ON us.id = u.created_by
  JOIN boards b
    ON u.board_id = b.id
  JOIN answers a
    ON u.answer_id = a.id
WHERE u.ex_id=($1);
`;

export default findUpvoteByExIdQuery;
