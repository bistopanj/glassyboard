const insertPanelQuery = `
INSERT INTO
  panels
    (
      title,
      featured,
      created_by
    )
VALUES
  (
    ($1),
    ($2),
    (SELECT id FROM users WHERE ex_id=($3))
  )
RETURNING
  ex_id as id,
  title,
  featured,
  active,
  ($3) AS created_by,
  created_at,
  updated_at
`;

export default insertPanelQuery;
