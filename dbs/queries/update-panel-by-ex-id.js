const updatePanelByExIdQuery = `
UPDATE
  panels
SET
  title = ($1)
WHERE
  ex_id = ($2)
  AND
  (
    ($3) = uuid('00000000-0000-0000-0000-000000000000')
      OR
    created_by = (
      SELECT
        id
      FROM
        users
      WHERE
        ex_id = ($3)
    )
  )
RETURNING
  ex_id AS id,
  title,
  ($3) AS updated_by,
  featured,
  created_at,
  updated_at
`;

export default updatePanelByExIdQuery;
