const findPanelByExIdQuery = `
SELECT
  p.ex_id as id,
  p.title,
  p.featured,
  p.active,
  u.ex_id AS created_by,
  p.created_at,
  p.updated_at
FROM
  panels p
    JOIN
      users u
        ON p.created_by = u.id
WHERE
  p.ex_id=($1);
`;

export default findPanelByExIdQuery;
