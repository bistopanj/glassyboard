const insertBoardQuery = `
WITH input AS (
  SELECT 
    ($3)::text AS slug,
    (($3)||'-')::text AS slug1
)

,free_slug AS (
  SELECT i.slug
  FROM input i
  LEFT JOIN boards b USING (slug)
  WHERE b.slug IS NULL
  
  UNION ALL
  (
    SELECT i.slug1 || COALESCE(right(b.slug, length(i.slug1)*-1)::int + 1, 1)
    FROM input i
    LEFT JOIN boards b ON b.slug LIKE (i.slug1 || '%')
                          AND right(b.slug, length(i.slug1) * -1) ~ '^\\d+$'
    ORDER BY right(b.slug, length(i.slug1)*-1)::int DESC
  )
  LIMIT 1
)

INSERT INTO boards (topic, created_by, slug)
VALUES (($1), (SELECT id FROM users WHERE ex_id=($2)), ( SELECT slug FROM free_slug))
RETURNING 
*,
TRUE AS editable
;
`;

export default insertBoardQuery;
