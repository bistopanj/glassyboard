const upsertUserQuery = `
WITH
  input_rows (email, first_name, last_name) AS (
    VALUES (
      ($1),
      ($2),
      ($3)
    )
  ),
  inserts AS (
    INSERT INTO
      users
      (
        email,
        first_name,
        last_name
      )
    SELECT
      *
    FROM
      input_rows
    ON CONFLICT (email)
        DO UPDATE 
          SET 
            first_name = EXCLUDED.first_name, 
            last_name = EXCLUDED.last_name
          WHERE
            users.first_name IS NULL 
              AND
            users.last_name IS NULL
    RETURNING *
  )
SELECT
  TRUE AS inserted, id, email, ex_id, first_name, last_name, created_at, updated_at
FROM
  inserts
UNION ALL
SELECT
  FALSE AS inserted, u.id, u.email, u.ex_id, u.first_name, u.last_name, u.created_at, u.updated_at
FROM
  input_rows
    JOIN
      users u
        USING (email)
LIMIT 1
`;

const odlQuery = `
WITH 
  input_rows(email) AS (
    VALUES( $1 )
  ),
  ins AS (
    INSERT INTO users (email)
    SELECT * FROM input_rows
    ON CONFLICT (email) DO NOTHING
    RETURNING *
  )
SELECT true AS inserted, id, email, ex_id, first_name, last_name, created_at, updated_at
FROM ins
UNION ALL
SELECT false AS inserted, c.id, c.email, c.ex_id,c.first_name, c.last_name, c.created_at, c.updated_at
FROM input_rows
JOIN users c USING (email);
`;

export default upsertUserQuery;
