const updateCompanyPrimaryBoardQuery = `
UPDATE
  companies
SET
  primary_board = (SELECT id FROM boards WHERE ex_id=($2))
WHERE
  ex_id = ($1)
RETURNING
  ex_id AS id,
  subdomain,
  ($2)::uuid AS primary_board,
  created_at,
  updated_at
`;

export default updateCompanyPrimaryBoardQuery;
