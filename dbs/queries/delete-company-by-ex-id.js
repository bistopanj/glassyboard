const deleteCompanyByExIdQuery = `
DELETE FROM
  companies
WHERE
  ex_id=($1)
;`;

export default deleteCompanyByExIdQuery;
