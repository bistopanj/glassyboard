const testQuery = `
WITH
  the_user AS (
    SELECT
      *
    FROM
      users
    WHERE
      ex_id = 'fcfc05d2-04e7-4ffa-8afd-8f11a30ff7e4'
  ),
  featured_panels AS (
    SELECT
      b.*,
      ppp.id AS panel_id,
      ppp.ex_id AS panel_ex_id,
      ppp.title AS panel_title,
      ppp.created_at AS panel_created_at,
      ppp.created_by AS panel_created_by,
      ppp.updated_at AS panel_updated_at
    FROM
      panels ppp
        LEFT JOIN
          panel_board pb
            ON pb.panel_id = ppp.id AND pb.active = TRUE
        LEFT JOIN
          boards b
            ON b.id = pb.board_id AND b.active = TRUE
    WHERE
      ppp.featured = TRUE AND ppp.active = TRUE
  ),
  featured_boards_answers_extended AS (
    SELECT
      a.*,
      COALESCE( 
        (a.created_by = (SELECT id FROM the_user) )
          OR
        ((SELECT email FROM the_user) = 'farid.khaheshi@gmail.com')
        , FALSE
      ) AS editable,
      count(CASE WHEN up.active THEN 1 END) AS upvotes,
      count(CASE WHEN tu.id IS NOT NULL THEN 1 END) > 0 AS upvoted
    FROM
      answers a
        JOIN
          featured_panels fp
            ON a.board_id = fp.id
        LEFT JOIN
          upvotes up
            ON  a.id = up.answer_id AND up.active
        LEFT JOIN
          the_user tu
            ON  tu.id = up.created_by
    WHERE
      a.active = TRUE
    GROUP By
      a.id,
      tu.id
  ),
  featured_panels_extended AS (
    SELECT
      fp.ex_id AS id,
      fp.slug,
      fp.topic, 
      fp.created_by,
      COALESCE (
        (fp.created_by = (SELECT id FROM the_user))
          OR
        ((SELECT email FROM the_user) = 'farid.khaheshi@gmail.com')
        , FALSE
      ) AS editable,
      fp.created_at,
      fp.updated_at,
      fp.panel_id,
      fp.panel_ex_id,
      fp.panel_title,
      fp.created_by,
      fp.panel_created_at,
      fp.panel_created_by,
      fp.panel_updated_at,
      COALESCE (
        json_agg(
          json_build_object(
            'id', fbae.ex_id,
            'answer', fbae.answer,
            'created_at', fbae.created_at,
            'updated_at', fbae.updated_at,
            'editable', fbae.editable,
            'upvotes', fbae.upvotes,
            'upvoted', fbae.upvoted
          ) ORDER BY fbae.created_at DESC
        ) FILTER (
            WHERE fbae.id IS NOT NULL
        )
        , '[]'
      ) AS answers
    FROM
      featured_panels fp
        LEFT JOIN
            featured_boards_answers_extended fbae
              ON fp.id = fbae.board_id
    GROUP BY
      fp.ex_id,
      fp.slug,
      fp.topic, 
      fp.created_at,
      fp.updated_at,
      fp.created_by,
      fp.panel_id,
      fp.panel_ex_id,
      fp.panel_title,
      fp.panel_created_at,
      fp.panel_created_by,
      fp.panel_updated_at
    ORDER BY fp.created_at DESC
  )
SELECT
  fpe.panel_ex_id,
  fpe.panel_title,
  fpe.panel_created_at,
  fpe.panel_updated_at,
  COALESCE(
    fpe.panel_created_by = (SELECT id FROM the_user)
      OR
    ((SELECT email FROM the_user) = 'farid.khaheshi@gmail.com')
    ,
    FALSE
  ) AS editable,
  COALESCE (
    json_agg(
      json_build_object(
        'id', fpe.id,
        'slug', fpe.slug,
        'created_at', fpe.created_at,
        'updated_at', fpe.updated_at,
        'editable', fpe.editable,
        'topic', fpe.topic,
        'answers', fpe.answers
      ) ORDER BY fpe.created_at ASC
    ) FILTER (
        WHERE fpe.id IS NOT NULL
    )
    , '[]'
  ) AS boards
FROM
  featured_panels_extended fpe
GROUP BY
  fpe.panel_ex_id,
  fpe.panel_title,
  fpe.panel_created_at,
  fpe.panel_created_by,
  fpe.panel_updated_at
ORDER BY
  fpe.panel_created_at DESC
`;

export default testQuery;
