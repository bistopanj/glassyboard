const insertUpvoteQuery = `
INSERT INTO upvotes
  (
    answer_id,
    board_id,
    created_by
  )
VALUES
  ( 
    (SELECT id FROM answers WHERE ex_id=($1)),
    (SELECT id FROM boards WHERE ex_id=($2)),
    (SELECT id FROM users WHERE ex_id=($3))
  )
ON CONFLICT ON CONSTRAINT max_one_per_user_per_answer
DO
    UPDATE
    SET active = NOT upvotes.active
RETURNING
    ex_id as id,
    created_at,
    updated_at,
    active,
    ($1) as answer_id,
    ($2) as board_id,
    ($3) as created_by
;`;

export default insertUpvoteQuery;
