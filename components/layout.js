import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import TopNavbar from './top-navbar';
import { initGA, logPageView } from '../utils/analytics-utils';

const Layout = ({ children, noNavbarList = [] }) => {
  const router = useRouter();
  useEffect(() => {
    if (window && !window.GA_INITIALIZED) {
      initGA();
      window.GA_INITIALIZED = true;
    }
    logPageView();
  }, []);
  const { pathname } = router || {};
  return (
    <>
      {!noNavbarList.includes(pathname) && <TopNavbar data-testid="top-navbar" />}
      {children}
    </>
  );
};

Layout.propTypes = {
  children: PropTypes.element,
  noNavbarList: PropTypes.arrayOf(PropTypes.string),
};
Layout.defaultProps = {
  children: null,
  noNavbarList: [],
};

export default Layout;
