import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import './upvote-button.css';
import { upvote } from '../actions';
import { makeUserInfoSelector } from '../selectors';

const UpvoteButton = ({ upvotes, boardId, answerId, slug, upvoted = false }) => {
  const dispatch = useDispatch();
  const selectUserInfo = useMemo(makeUserInfoSelector, []);
  const userInfo = useSelector(state => selectUserInfo(state));
  const upvoteTheAnswer = useCallback(() => {
    dispatch(upvote(userInfo, { answerId, boardId, slug }));
  }, [answerId, upvote, boardId]);

  return (
    <button
      type="button"
      className={upvoted ? 'upvote-btn upvoted' : 'upvote-btn'}
      data-testid="upvote-btn"
      aria-label="upvote"
      onClick={upvoteTheAnswer}
    >
      ▲
      <br />
      {upvotes}
    </button>
  );
};

UpvoteButton.propTypes = {
  answerId: PropTypes.string.isRequired,
  boardId: PropTypes.string.isRequired,
  upvoted: PropTypes.bool,
  upvotes: PropTypes.number.isRequired,
  slug: PropTypes.string.isRequired,
};

UpvoteButton.defaultProps = {
  upvoted: false,
};

const MemoizedUpvoteButton = React.memo(UpvoteButton);
MemoizedUpvoteButton.WrappedComponent = UpvoteButton;
MemoizedUpvoteButton.displayName = 'UpvoteButton';

export default MemoizedUpvoteButton;
