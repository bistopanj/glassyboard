import { useCallback, useRef, useState, useMemo, useEffect } from 'react';
import Router from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { Input, Form, FormGroup, Label } from 'reactstrap';
import AsyncButton from './async-button';
import { makeUserInfoSelector, makeNewBoardStateSelector } from '../selectors';
import * as actions from '../actions';
import './add-board-form.css';

const formTopic = 'Raise a Topic';
const formSubtopic = `and collect people's opinion about it`;
const topicLabel = 'Topic:';
const topicPlaceholder = 'enter a simple and short question or phrase';
const addTopicButtonText = 'Add Topic';

const topicPattern = '.{5,}';
const topicRegEx = new RegExp(topicPattern);

const AddBoardForm = () => {
  const [isValid, setIsValid] = useState(false);
  const selectUserInfo = useMemo(makeUserInfoSelector, []);
  const selectNewBoardState = useMemo(makeNewBoardStateSelector, []);
  const userInfo = useSelector(state => selectUserInfo(state));
  const { loading, success, error } = useSelector(state => selectNewBoardState(state));
  const dispatch = useDispatch();
  const topicRef = useRef();

  const handleSubmit = useCallback(
    e => {
      if (e) e.preventDefault();
      dispatch(actions.addBoard(userInfo, { topic: topicRef.current.value }));
      setIsValid(false);
      topicRef.current.value = '';
    },
    [topicRef, actions, userInfo, setIsValid, topicRef]
  );

  const handleChange = useCallback(
    e => {
      // e.target.setCustomValidity('custom message');
      const isValidTopic = topicRegEx.test(e.target.value);
      if (isValidTopic !== isValid) setIsValid(isValidTopic);
    },
    [isValid, setIsValid, topicRegEx]
  );

  useEffect(() => {
    if (success) {
      dispatch(actions.addBoardReset());
      Router.push('/board/[slug]', `/board/${success}`);
    }
  }, [success, Router, dispatch]);

  return (
    <section className="add-board-form-container" data-testid="add-board-form-container">
      <div className="add-board-form-header mb-5 mt-3">
        <h3 className="add-form-heading">{formTopic}</h3>
        <p>{formSubtopic}</p>
      </div>
      <Form onSubmit={handleSubmit} data-testid="add-board-form">
        <FormGroup className="mb-5">
          <Label for="new-board-topic-input">{topicLabel}</Label>
          <Input
            onChange={handleChange}
            type="text"
            pattern={topicPattern}
            id="new-board-topic-input"
            placeholder={topicPlaceholder}
            innerRef={topicRef}
            required
            data-testid="new-board-topic-input"
            valid={isValid}
          />
        </FormGroup>
        <AsyncButton
          block
          loading={loading}
          disabled={!isValid}
          data-testid="add-board-btn-wrapper"
          btnTestId="add-new-topic-btn"
        >
          {addTopicButtonText}
        </AsyncButton>
      </Form>
    </section>
  );
};

export default AddBoardForm;
