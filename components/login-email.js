import { useCallback, useRef, useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import { Form, FormGroup, FormText, Label, Input } from 'reactstrap';
import PropTypes from 'prop-types';
import AsyncButton from './async-button';
import askTicketAction from '../actions/ask-ticket';
import makeTicketRequestInfoSelector from '../selectors/get-ticket-request-info';
import './login-email.css';

const guideText = 'Sign in with email';
const moreInfoGuide = 'An email will be sent to this address for verification.';
const signInButtonText = 'Sign in';
const emailPlaceholder = 'e.g. john@mymail.com';

const emailRegEx = /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i;
const doNothing = () => ({});

const LoginEmail = ({ data = {}, onRedirect = doNothing }) => {
  const [isValid, setIsValid] = useState(false);
  const [email, setEmail] = useState(null);
  const emailRef = useRef();
  const selectTicketRequestInfo = useMemo(makeTicketRequestInfoSelector, []);
  const { success, loading, error } = useSelector(state => selectTicketRequestInfo(state));
  const router = useRouter();
  const dispatch = useDispatch();

  const askTicket = useCallback(
    loginInfo => {
      dispatch(askTicketAction(loginInfo));
    },
    [dispatch]
  );

  const handleChange = useCallback(
    e => {
      const isValidEmail = emailRegEx.test(e.target.value);
      setEmail(e.target.value);
      if (isValidEmail !== isValid) {
        setIsValid(isValidEmail);
      }
    },
    [isValid, setIsValid, emailRegEx]
  );

  const handleSubmit = useCallback(
    e => {
      if (e) e.preventDefault();
      const theEmail = emailRef.current.value;
      askTicket({ email: theEmail, ticketInfo: data });
    },
    [askTicket, data, emailRef]
  );

  useEffect(() => {
    if (success) {
      onRedirect();
      router.push('/tickets/sent/[email]', `/tickets/sent/${encodeURIComponent(email)}`);
    }
  }, [success, email]);

  return (
    <Form onSubmit={handleSubmit} data-testid="login-form" className="login-form-container">
      <p className="email-login-guide text-center">{guideText}</p>
      <FormGroup className="mb-10">
        <Label for="email">Email:</Label>
        <Input
          onChange={handleChange}
          type="email"
          name="email"
          id="login-email"
          placeholder={emailPlaceholder}
          innerRef={emailRef}
          data-testid="email-input"
          required
          valid={isValid}
        />
        <FormText>{moreInfoGuide}</FormText>
      </FormGroup>
      <AsyncButton
        loading={loading}
        disabled={!isValid}
        size="lg"
        block
        btnTestId="login-btn"
        data-testid="login-btn-wrapper"
      >
        {signInButtonText}
      </AsyncButton>
    </Form>
  );
};

LoginEmail.propTypes = {
  data: PropTypes.shape({}),
  onRedirect: PropTypes.func,
};

LoginEmail.defaultProps = {
  data: {},
  onRedirect: doNothing,
};

export default LoginEmail;
