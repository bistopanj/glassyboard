import './horizontal-divider.css';
import PropTypes from 'prop-types';

const HorizontalDivider = ({ children }) => (
  <section className="lined-surrounded">
    <div className="content-container">{children}</div>
  </section>
);

HorizontalDivider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
};

HorizontalDivider.defaultProps = {
  children: null,
};

export default HorizontalDivider;
