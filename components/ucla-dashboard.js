import CompanyBoard from './company-board';

const UclaDashboard = ({ board, pathInfo }) => {
  return (
    <>
      <div>UCLA dashboard</div>
      <CompanyBoard board={board} pathInfo={pathInfo} />
    </>
  );
};

export default UclaDashboard;
