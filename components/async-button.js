import { Spinner, Button } from 'reactstrap';
import PropTypes from 'prop-types';

const AsyncButton = ({
  children,
  loading = false,
  error = null,
  disabled = false,
  btnTestId = 'button',
  color = 'info',
  ...rest
}) => {
  if (loading)
    return (
      <div className="d-flex justify-content-center align-items-center">
        <Spinner size="md" color={color} data-testid="loading-spinner" />
      </div>
    );
  return (
    <Button
      disabled={disabled}
      color={disabled ? 'secondary' : color}
      {...rest}
      data-testid={btnTestId}
    >
      {children}
    </Button>
  );
};
AsyncButton.propTypes = {};
AsyncButton.defaultProps = {};

export default React.memo(AsyncButton);
