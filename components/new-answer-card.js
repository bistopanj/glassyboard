import { useCallback, useMemo, useRef, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Input, Form } from 'reactstrap';
import AsyncButton from './async-button';
import { addAnswer, addAnswerReset } from '../actions';
import './answer-card.css';
import { makeUserInfoSelector, makeGetNewAnswerStatusSelector } from '../selectors';

const addButtonText = 'Add';
const answerPattern = '.{5,}';
const answerRegEx = new RegExp(answerPattern);

const NewAnswerCard = ({ slug, id }) => {
  const [isValid, setIsValid] = useState(false);
  const dispatch = useDispatch();
  const selectUserInfo = useMemo(makeUserInfoSelector, []);
  const selectNewAnswerStatus = useMemo(makeGetNewAnswerStatusSelector, []);
  const { loading, error, success } = useSelector(state => selectNewAnswerStatus(state, slug));
  const userInfo = useSelector(state => selectUserInfo(state));
  const answerRef = useRef();

  const handleSubmit = useCallback(
    e => {
      if (e) e.preventDefault();
      dispatch(addAnswer(userInfo, { answer: answerRef.current.value, slug, id }));
    },
    [answerRef, slug, id]
  );

  const handleChange = useCallback(
    e => {
      const isValidAnswer = answerRegEx.test(e.target.value);
      if (isValidAnswer !== isValid) setIsValid(isValidAnswer);
    },
    [isValid, setIsValid, answerPattern]
  );

  useEffect(() => {
    if (success) {
      answerRef.current.value = '';
      dispatch(addAnswerReset(slug));
      setIsValid(false);
    }
  }, [success]);

  return (
    <Form className="answer-box hover-off" onSubmit={handleSubmit} data-testid="new-answer-form">
      <Input
        onChange={handleChange}
        type="text"
        pattern={answerPattern}
        className="mr-3"
        placeholder="add your opinion..."
        required
        innerRef={answerRef}
        data-testid="add-answer-to-board-input"
        valid={isValid}
      />
      <AsyncButton
        loading={loading}
        disabled={!isValid}
        color="primary"
        outline
        data-testid="add-answer-btn-wrapper"
        btnTestId="add-answer-btn"
      >
        {addButtonText}
      </AsyncButton>
    </Form>
  );
};

export default NewAnswerCard;
