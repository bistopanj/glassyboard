import PropTypes from 'prop-types';
import LoginSocial from './login-social';
import LoginEmail from './login-email';
import HorizontalDivider from './horizontal-didiver';

const dividerText = 'or';

const generalLoginGuide = 'Please sign in to continue:';
const guidesForActions = {
  UPVOTE: 'Please sign in to finalize registering your vote:',
  'ADD-ANSWER': 'Please sign in to register your opinion:',
  'ADD-BOARD': 'Please sign in to register your new board:',
};

const doNothing = () => ({});

const renderSigninGuide = data => {
  const { ticketAction } = data || {};
  if (!ticketAction) return null;
  let guide = generalLoginGuide;
  if (ticketAction in guidesForActions) guide = guidesForActions[ticketAction];
  return (
    <p className="signin-guide" data-testid="signin-guide">
      {guide}
    </p>
  );
};

const SigninDialog = ({ signinData = null, onRedirect = doNothing }) => {
  return (
    <div data-testid="signin-dialog-container">
      {renderSigninGuide(signinData)}
      <LoginSocial data={signinData} onRedirect={onRedirect} />
      <HorizontalDivider>
        <h5 className="headline">{dividerText}</h5>
      </HorizontalDivider>
      <LoginEmail data={signinData} onRedirect={onRedirect} />
    </div>
  );
};

SigninDialog.propTypes = {
  signinData: PropTypes.shape({}),
  onRedirect: PropTypes.func,
};

SigninDialog.defaultProps = {
  signinData: null,
  onRedirect: doNothing,
};

const memoizedSigninDialog = React.memo(SigninDialog);
memoizedSigninDialog.displayName = 'SigninDialog';
memoizedSigninDialog.WrappedComponent = SigninDialog;

export default memoizedSigninDialog;
