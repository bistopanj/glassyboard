import Head from 'next/head';

const siteName = 'GlassyBoard';
const title = "Glassyboard - Collect people's opinions about topics important to you";
const description =
  'Easily create a board with any topic you like, share your board with the prople you care about and collect their opinions.';

const DefaultHead = () => (
  <Head>
    <title key="title">{title}</title>
    <script src="https://apis.google.com/js/platform.js" async defer />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway:300&display=swap"
      rel="stylesheet"
    />
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <meta
      name="google-signin-client_id"
      content="151505131662-tv5n3lmm03ggv06vc4n9e571mtnhasnb.apps.googleusercontent.com"
    />
    <meta key="description" name="description" content={description} />
    <meta key="og-url" property="og:url" content="https://glassyboard.com" />
    <meta key="og-type" property="og:type" content="website" />
    <meta key="og-title" property="og:title" content={title} />
    <meta key="og-description" property="og:description" content={description} />
    <meta key="og-site_name" property="og:site_name" content={siteName} />
  </Head>
);

export default DefaultHead;
