import Head from 'next/head';
import { Container } from 'reactstrap';
import Board from './board';

const metaDescription = 'How different people think about this topic';
const ogDescription = 'See what different people think about this topic';

const CompanyBoard = ({ board, pathInfo }) => {
  const { data = null, data: { slug, editable = false } = {} } = board || {};
  const { protocol, host, subdomain } = pathInfo;
  return (
    <main className="full-page" data-testid="board-page-container">
      {data && (
        <Head>
          <title key="title">{data.topic}</title>
          <meta key="description" name="description" content={metaDescription} />
          <meta key="og-title" property="og:title" content={data.topic} />
          <meta key="og-description" property="og:description" content={ogDescription} />
          <meta key="og-url" property="og:url" content={`https://${host}`} />
        </Head>
      )}
      <Container>
        <Board slug={slug} editable={editable} {...board} />
      </Container>
    </main>
  );
};

export default CompanyBoard;
