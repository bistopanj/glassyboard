import { connect } from 'react-redux';
import {
  Nav,
  Navbar,
  NavbarText,
  Button,
  Container,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import Link from 'next/link';
import PropTypes from 'prop-types';
import { makeUserInfoSelector } from '../selectors';
import { openLogin, logout } from '../actions';
import './top-navbar.css';

const logoutText = 'Sign out';
const loginText = 'Sign in';

const baseUrl = process.env.BASE_URL;

const renderLogoutDropdown = email => (
  <UncontrolledDropdown nav>
    <DropdownToggle nav caret>
      <span data-testid="user-email">{email}</span>
    </DropdownToggle>
    <DropdownMenu right>
      <DropdownItem data-testid="logout">
        <a href={`${baseUrl}/auth/logout`}>{logoutText}</a>
        {/* <Link href="/auth/logout">
          <a href="/auth/logout">{logoutText}</a>
        </Link> */}
      </DropdownItem>
    </DropdownMenu>
  </UncontrolledDropdown>
);

const renderLoginButton = loginFunc => (
  <Button
    href={`${baseUrl}/signin`}
    className="login-btn"
    color="link"
    size="md"
    data-testid="login"
  >
    <NavbarText>{loginText}</NavbarText>
  </Button>
);

const Brand = props => {
  const { 'data-testid': dataTestId } = props;
  return (
    <Link href="/">
      <a href="/" className="navbar-brand" data-testid={dataTestId}>
        Glassyboard
      </a>
    </Link>
  );
};

Brand.propTypes = {
  'data-testid': PropTypes.string,
};

Brand.defaultProps = {
  'data-testid': 'brand-name',
};

const TopNavbar = ({ isAuthenticated, user, login }) => {
  const { email = 'unknown' } = user || {};

  return (
    <Navbar className="colored-navbar" dark data-testid="navbar-container">
      <Container>
        <Brand />
        <Nav navbar>{isAuthenticated ? renderLogoutDropdown(email) : renderLoginButton(login)}</Nav>
      </Container>
    </Navbar>
  );
};

TopNavbar.propTypes = {
  isAuthenticated: PropTypes.bool,
  user: PropTypes.shape({
    id: PropTypes.string,
    email: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    createdAt: PropTypes.string,
  }),
  login: PropTypes.func,
};

TopNavbar.defaultProps = {
  isAuthenticated: false,
  user: null,
  login: () => ({}),
};

const mapDispatchToProps = dispatch => ({
  login: () => dispatch(openLogin()),
});

export default connect(makeUserInfoSelector, mapDispatchToProps)(TopNavbar);
