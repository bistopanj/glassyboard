import PropTypes from 'prop-types';

const ContentCard = ({ children }) => (
  <div className="content-card justify-content-center">{children}</div>
);

ContentCard.propTypes = {};

export default ContentCard;
