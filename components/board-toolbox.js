import { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Button, Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './board-toolbox.css';

const baseUrl = process.env.BASE_URL;
const doNothing = () => ({});
const goToBoardPageText = 'Go to board page';
const editBoardText = 'Edit topic';
const addToPanelText = 'Add to Panel';

const buildShareLink = (slug, topic) => {
  const subject = encodeURIComponent(topic);
  const body = encodeURIComponent(`See what different people say about this topic:

${baseUrl}/board/${slug}
`);
  return `mailto:?subject=${subject}&body=${body}`;
};

const renderButton = (name, requirements = {}) => {
  switch (name) {
    case 'edit-board':
      return (
        <button
          key={name}
          onClick={requirements.edit}
          data-testid="board-menu-edit"
          role="menuitem"
          className="dropdown-item"
          tabIndex={0}
          type="button"
        >
          {editBoardText}
        </button>
      );
    case 'open-board-page':
      return (
        <Link key={name} href="/board/[slug]" as={`/board/${requirements.slug}`}>
          <a
            href={`/board/${requirements.slug}`}
            role="menuitem"
            className="dropdown-item"
            tabIndex={0}
            data-testid="board-menu-open-board-page"
          >
            {goToBoardPageText}
          </a>
        </Link>
      );
    case 'add-to-panel':
      return (
        <Link key={name} href={`/panels?board=${requirements.slug}`}>
          <a
            href={`/panels?board=${requirements.slug}`}
            role="menuitem"
            className="dropdown-item"
            tabIndex={0}
            data-testid="board-menu-add-to-panel"
          >
            {addToPanelText}
          </a>
        </Link>
      );
    case 'remove-from-panel':
      return (
        <button
          role="menuitem"
          className="dropdown-item"
          tabIndex={0}
          type="button"
          key={`name-${requirements.id}`}
          onClick={requirements.removeFromPanel}
          data-testid="board-menu-remove-from-panel-button"
        >
          {`Remove from panel ${requirements.title}`}
        </button>
      );
    default:
      return null;
  }
};

const renderDropdownMenu = (menuButtons, isOpen, toggleMenu) => {
  if (menuButtons.length === 0) return null;
  return (
    <Dropdown isOpen={isOpen} toggle={toggleMenu} data-testid="board-menu-dropdown">
      <DropdownToggle data-testid="board-menu-toggle">
        <FontAwesomeIcon icon={{ prefix: 'fas', iconName: 'ellipsis-v' }} />
      </DropdownToggle>
      <DropdownMenu right data-testid="board-menu">
        {menuButtons.map(({ name, requirements }) => renderButton(name, requirements))}
      </DropdownMenu>
    </Dropdown>
  );
};

const BoardToolbox = ({ slug, showEdit = false, enableEditing = doNothing, topic }) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const router = useRouter();
  const { pathname, query: { admin } = {} } = router || {};

  const toggleMenu = useCallback(() => {
    setMenuOpen(val => !val);
  }, [setMenuOpen]);

  const edit = useCallback(() => {
    enableEditing();
    setMenuOpen(false);
  }, [enableEditing, setMenuOpen]);

  const menuButtons = [];
  if (pathname !== '/board/[slug]')
    menuButtons.push({ name: 'open-board-page', requirements: { slug } });
  if (showEdit) menuButtons.push({ name: 'edit-board', requirements: { edit } });
  if (showEdit && admin) menuButtons.push({ name: 'add-to-panel', requirements: { edit, slug } });
  return (
    <div className="board-toolbox-container" data-testid="board-toolbox-container">
      <Button
        color="link"
        data-testid="share-board-button"
        title="Share by Email"
        href={buildShareLink(slug, topic)}
      >
        <FontAwesomeIcon icon={{ prefix: 'far', iconName: 'share-square' }} />
      </Button>
      {renderDropdownMenu(menuButtons, menuOpen, toggleMenu)}
    </div>
  );
};

BoardToolbox.propTypes = {
  topic: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  showEdit: PropTypes.bool,
  enableEditing: PropTypes.func,
};
BoardToolbox.defaultProps = {
  showEdit: false,
  enableEditing: doNothing,
};

const MemoizedBoardToolbox = React.memo(BoardToolbox);
MemoizedBoardToolbox.displayName = 'BoardToolbox';

export { BoardToolbox as WrappedComponent };
export default MemoizedBoardToolbox;
