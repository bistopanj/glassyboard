import { useState, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import updateBoardAction from '../actions/update-board';
import AnswerCard from './answer-card';
import NewAnswerCard from './new-answer-card';
import BoardToolbox from './board-toolbox';
import makeEditable from '../hocs/make-editable';
import './board.css';

const EditableTopic = makeEditable('h5', 'EditableTopic');

const renderAnswerStack = (slug, boardId, answers) => (
  <ul className="answers-stack" data-testid="answers-stack">
    {answers.map(
      ({ id: answerId, answer: answerText, upvotes = 0, upvoted = false, editable = false }) => (
        <li key={answerId}>
          <AnswerCard
            boardId={boardId}
            answerId={answerId}
            answerText={answerText}
            upvotes={upvotes}
            upvoted={upvoted}
            editable={editable}
            slug={slug}
          />
        </li>
      )
    )}
    <li>
      <NewAnswerCard id={boardId} slug={slug} />
    </li>
  </ul>
);

const Board = ({
  editable = false,
  slug = undefined,
  error = undefined,
  loading = undefined,
  data = {},
  answers = [],
}) => {
  const { topic, id: boardId } = data || {};
  const [editing, setEditing] = useState(false);
  const dispatch = useDispatch();

  const enableEditing = useCallback(() => {
    if (editable) setEditing(true);
  }, [setEditing, editable]);

  const disableEditing = useCallback(() => {
    setEditing(false);
  }, [setEditing]);

  const updateBoard = useCallback(
    newTopic => {
      dispatch(updateBoardAction(boardId, slug, newTopic, topic));
    },
    [boardId, slug, dispatch]
  );

  return (
    <section className="board-container" data-testid="board-container">
      <div className="board-header">
        <EditableTopic
          text={topic}
          editing={editing}
          enableEditing={enableEditing}
          disableEditing={disableEditing}
          onConfirm={updateBoard}
          className="board-topic"
          data-testid="board-topic"
        />
        <BoardToolbox slug={slug} showEdit={editable} enableEditing={enableEditing} topic={topic} />
      </div>
      {renderAnswerStack(slug, boardId, answers)}
    </section>
  );
};

Board.propTypes = {
  editable: PropTypes.bool,
  slug: PropTypes.string.isRequired,
  error: PropTypes.shape({}),
  loading: PropTypes.bool,
  data: PropTypes.shape({
    id: PropTypes.string,
    topic: PropTypes.string,
    slug: PropTypes.string,
  }),
  answers: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      answer: PropTypes.string.isRequired,
      created_at: PropTypes.string,
      updated_at: PropTypes.string,
      upvotes: PropTypes.number,
      upvoted: PropTypes.bool,
    })
  ),
};

Board.defaultProps = {
  editable: false,
  error: undefined,
  loading: undefined,
  data: {},
  answers: [],
};

export default Board;
