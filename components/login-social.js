import { useEffect } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import './login-social.css';

const doNothing = () => ({});

const LoginSocial = ({ data = {}, onRedirect = doNothing }) => {
  const router = useRouter();

  const onSuccess = googleUser => {
    const { id_token: googleIdToken } = googleUser.getAuthResponse();
    const { ticketAction } = data || {};
    let baseUrl = `/auth/google?token=${googleIdToken}`;
    if (ticketAction)
      baseUrl = `/auth/google?token=${googleIdToken}&data=${encodeURIComponent(
        JSON.stringify(data)
      )}`;
    onRedirect();
    router.push(baseUrl);
  };

  const onFailure = error => {
    // handle the error case better!
    console.log(error);
  };

  useEffect(() => {
    if (window.gapi) {
      window.gapi.signin2.render('google-sign-in', {
        // scope: 'profile email',
        width: 300,
        height: 50,
        longtitle: true,
        theme: 'dark',
        onsuccess: onSuccess,
        onfailure: onFailure,
      });
    }
  }, []);

  return (
    <div className="login-social-container" data-testid="login-social-container">
      <div id="google-sign-in" />
    </div>
  );
};

LoginSocial.propTypes = {
  onRedirect: PropTypes.func,
  data: PropTypes.shape({}),
};

LoginSocial.defaultProps = {
  onRedirect: doNothing,
  data: {},
};

export default LoginSocial;
