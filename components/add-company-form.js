import { useCallback, useMemo, useRef, useState, useEffect } from 'react';
import { Form, Input, FormGroup, Label, FormText } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import AsyncButton from './async-button';
import makeUserInfoSelector from '../selectors/get-user-info';
import makeNewCompanyStateSelector from '../selectors/get-new-company-state';
import addCompany from '../actions/add-company';
import addCompanyReset from '../actions/add-company-reset';
import './add-company-form.css';

const addCompanyButtonText = 'Create';
const subdomainPlaceholder = 'yourteam';
const formTopic = 'Simple Internet Tools for Professionals';
const formSubtopic =
  'We build the missing links in your work so that you can focus on what your are good at.';
const companyLabel = 'Your board will hold all your tools in one place. Create it at:';

const subdomainPattern = '^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]$';
const subdomainRegEx = new RegExp(subdomainPattern);

const AddCompanyForm = () => {
  const [isValid, setIsValid] = useState(false);
  const selectUserInfo = useMemo(makeUserInfoSelector, []);
  const selectNewCompanyState = useMemo(makeNewCompanyStateSelector, []);
  const userInfo = useSelector(state => selectUserInfo(state));
  const { loading, success, error, company } = useSelector(state => selectNewCompanyState(state));
  const dispatch = useDispatch();
  const subdomainRef = useRef();

  const handleSubmit = useCallback(
    e => {
      if (e) e.preventDefault();
      dispatch(addCompany(userInfo, { subdomain: subdomainRef.current.value }));
      setIsValid(false);
      subdomainRef.current.value = '';
    },
    [dispatch, userInfo, addCompany]
  );

  const handleChange = useCallback(
    e => {
      const isValidSubdomain = subdomainRegEx.test(e.target.value);
      if (isValidSubdomain !== isValid) setIsValid(isValidSubdomain);
    },
    [isValid, setIsValid, subdomainRegEx]
  );

  useEffect(() => {
    if (success) {
      dispatch(addCompanyReset());
      window.location.replace(company.url);
    }
  }, [success, dispatch]);

  return (
    <section className="add-company-form-container" data-testid="add-company-form-container">
      <div className="add-company-form-header mb-5 mt-3">
        <h3 className="add-form-heading">{formTopic}</h3>
        <p>{formSubtopic}</p>
      </div>
      <Form onSubmit={handleSubmit} data-testid="add-company-form">
        <FormGroup className="mb-5">
          <Label for="new-company-name">{companyLabel}</Label>
          <div className="input-container">
            <Input
              required
              type="text"
              id="new-company-name"
              pattern={subdomainPattern}
              placeholder={subdomainPlaceholder}
              valid={isValid}
              onChange={handleChange}
              innerRef={subdomainRef}
              data-testid="new-company-subdomain-input"
              bsSize="lg"
            />
            <p>.glassyboard.com</p>
          </div>
        </FormGroup>
        <AsyncButton
          block
          loading={loading}
          disabled={!isValid}
          data-testid="add-company-btn-wrapper"
          btnTestId="add-new-company-btn"
        >
          {addCompanyButtonText}
        </AsyncButton>
      </Form>
    </section>
  );
};

export default AddCompanyForm;
