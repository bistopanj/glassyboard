import { useCallback } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import UpvoteButton from './upvote-button';
import removeAnswerAction from '../actions/remove-answer';
import './answer-card.css';

const AnswerCard = ({
  editable = false,
  boardId,
  answerId,
  answerText,
  upvotes,
  upvoted,
  slug,
}) => {
  const dispatch = useDispatch();

  const removeAnswer = useCallback(() => {
    dispatch(removeAnswerAction(answerId, boardId, slug));
  }, [dispatch]);

  return (
    <div className="answer-box" data-testid="answer">
      <div className="answer-text" data-testid="answer-text-container">
        {editable && (
          <Button color="link" data-testid="remove-answer-button" onClick={removeAnswer}>
            <FontAwesomeIcon icon={{ prefix: 'far', iconName: 'trash-alt' }} />
          </Button>
        )}
        <p data-testid="answer-text">{answerText}</p>
      </div>
      <UpvoteButton
        upvotes={upvotes}
        upvoted={upvoted}
        boardId={boardId}
        answerId={answerId}
        slug={slug}
      />
    </div>
  );
};

AnswerCard.propTypes = {
  editable: PropTypes.bool,
  slug: PropTypes.string.isRequired,
  boardId: PropTypes.string.isRequired,
  answerId: PropTypes.string.isRequired,
  answerText: PropTypes.string.isRequired,
  upvotes: PropTypes.number.isRequired,
  upvoted: PropTypes.bool,
};

AnswerCard.defaultProps = {
  editable: false,
  upvoted: false,
};

export default AnswerCard;
