import { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import closeLoginModalAction from '../actions/close-login-modal';
import makeLoginInfoSelector from '../selectors/get-login-info';
import SigninDialog from './signin-dialog';
import './login-modal.css';

const baseUrl = process.env.BASE_URL;

const buildRedirectAddress = data =>
  (data || {}).ticketAction
    ? `${baseUrl}/login?data=${encodeURIComponent(JSON.stringify(data))}`
    : `${baseUrl}/login`;

const isValidSubdomain = (sub = '') => sub && sub.length > 0 && sub !== 'www';
const doNothing = () => ({});

const LoginModal = ({ isOpen = false, data = {}, closeLoginModal = doNothing, subdomain }) => {
  useEffect(() => {
    if (isOpen && isValidSubdomain(subdomain)) {
      closeLoginModal();
      const redirectUrl = buildRedirectAddress(data);
      window.location.replace(redirectUrl);
    }
  }, [isOpen, subdomain]);

  if (!isOpen || isValidSubdomain(subdomain)) return null;

  return (
    <Modal
      isOpen={isOpen}
      toggle={closeLoginModal}
      unmountOnClose
      centered
      className="login-modal-container"
      data-testid="login-modal-container"
    >
      <ModalHeader toggle={closeLoginModal} className="border-0" />
      <ModalBody>
        <SigninDialog signinData={data} onRedirect={closeLoginModal} />
      </ModalBody>
    </Modal>
  );
};

LoginModal.propTypes = {
  isOpen: PropTypes.bool,
  data: PropTypes.shape({}),
  closeLoginModal: PropTypes.func,
  subdomain: PropTypes.string,
};

LoginModal.defaultProps = {
  isOpen: false,
  data: {},
  closeLoginModal: doNothing,
  subdomain: '',
};

const mapDispatchToProps = dispatch => ({
  closeLoginModal: () => dispatch(closeLoginModalAction()),
});

export default connect(makeLoginInfoSelector, mapDispatchToProps)(LoginModal);

/*

import { useCallback, useState, useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import Router from 'next/router';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody, Form, FormGroup, FormText, Label, Input } from 'reactstrap';
import * as actions from '../actions';
import './login-modal.css';
import AsyncButton from './async-button';

const emailRegEx = /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i;
const guideText = 'Please log in to register your vote.';
const moreInfoGuide = 'An email will be sent to this address for verification.';
const logInButtonText = 'Log in';

const LoginModal = ({
  isOpen = false,
  data = {},
  closeLoginModal = () => ({}),
  login,
  success,
  loading,
  error,
}) => {
  const [email, setEmail] = useState(null);
  const [isValid, setIsValid] = useState(false);
  const emailRef = useRef();

  const handleRouteChange = useCallback(() => {
    closeLoginModal();
  }, [closeLoginModal]);

  const handleChange = useCallback(
    e => {
      const theEmail = e.target.value;
      const isValidEmail = emailRegEx.test(theEmail);
      if (isValidEmail !== isValid) setIsValid(isValidEmail);
    },
    [isValid, setIsValid, emailRegEx]
  );
  const handleSubmit = useCallback(
    e => {
      if (e) e.preventDefault();
      const theEmail = emailRef.current.value;
      setEmail(theEmail);
      login({ email: theEmail, ticketInfo: { ...data } });
    },
    [data, login, setEmail]
  );

  const resetModal = useCallback(() => {
    setIsValid(false);
    setEmail(null);
  }, [setIsValid, setEmail]);PropTypes.bool

  useEffect(() => {
    if (Router.events) Router.events.on('routeChangeStart', handleRouteChange);

    return () => {
      if (Router.events) Router.events.off('routeChangeStart', handleRouteChange);
    };
  }, []);

  if (!isOpen) return null;
  if (success) Router.push('/tickets/sent/[email]', `/tickets/sent/${encodeURIComponent(email)}`);

  return (
    <Modal
      isOpen={isOpen}
      toggle={closeLoginModal}
      unmountOnClose
      centered
      data-testid="login-modal-container"
      onExit={resetModal}
    >
      <ModalHeader toggle={closeLoginModal} className="border-0" />
      <ModalBody>
        <Form onSubmit={handleSubmit} data-testid="login-form" className="login-form-container">
          <FormGroup>
            <Label for="exampleEmail">{guideText}</Label>
          </FormGroup>
          <div className="g-signin2 gsignin-btn-container" />
          <FormGroup className="mb-10">
            <Label for="email">Email:</Label>
            <Input
              onChange={handleChange}
              type="email"
              name="email"
              id="login-email"
              placeholder="e.g. john@mymail.com"
              data-testid="email-input"
              innerRef={emailRef}
              required
              valid={isValid}
            />
            <FormText>{moreInfoGuide}</FormText>
          </FormGroup>
          <AsyncButton
            loading={loading || success}
            disabled={!isValid}
            size="lg"
            block
            btnTestId="login-btn"
            data-testid="login-btn-wrapper"
          >
            {logInButtonText}
          </AsyncButton>
        </Form>
      </ModalBody>
    </Modal>
  );
};

LoginModal.propTypes = {
  isOpen: PropTypes.bool,
  data: PropTypes.shape({}),
  closeLoginModal: PropTypes.func,
  login: PropTypes.func.isRequired,
  success: PropTypes.bool,
  loading: PropTypes.bool,
  error: PropTypes.shape({}),
};

LoginModal.defaultProps = {
  isOpen: false,
  data: {},
  closeLoginModal: () => ({}),
  success: false,
  loading: false,
  error: {},
};

const mapStateToProps = state => ({
  isOpen: state.login.isOpen,
  data: state.login.data,
  loading: state.login.loading,
  error: state.login.error,
  success: state.login.success,
});
const mapDispatchToProps = dispatch => ({
  closeLoginModal: () => dispatch(actions.closeLoginModal()),
  login: loginInfo => dispatch(actions.login(loginInfo)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginModal);

*/
