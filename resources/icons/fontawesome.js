import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faEdit,
  faShareSquare,
  faCheckCircle,
  faTimesCircle,
  faTrashAlt,
} from '@fortawesome/free-regular-svg-icons';
import { faLink, faCodeBranch, faEllipsisV } from '@fortawesome/free-solid-svg-icons';

library.add(
  faEdit,
  faLink,
  faShareSquare,
  faCodeBranch,
  faEllipsisV,
  faCheckCircle,
  faTimesCircle,
  faTrashAlt
);
