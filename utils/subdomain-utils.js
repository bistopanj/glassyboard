import url from 'url';

export const buildSubdomainUrl = (baseUrl, subdomain) => {
  const q = url.parse(baseUrl, true);
  const { protocol, host, path, slashes, href } = q;
  const hostParts = host.split('.');
  if (hostParts.length > 0 && hostParts[0] === 'www') {
    return `${protocol}//${host.replace('www', subdomain)}${path || '/'}`;
  }
  if (slashes) return `${protocol}//${subdomain}.${host}${path || '/'}`;
  return `${subdomain}.${href}${path || '/'}`;
};

export const base = '';
