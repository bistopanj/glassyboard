import { shallow } from 'enzyme';
import checkPropTypes from 'check-prop-types';
import { act } from 'react-dom/test-utils';
import http from 'http';
import listen from 'test-listen';
import { apiResolver } from 'next-server/dist/server/api-utils';

export const setUpServer = async (reqHandler, params = undefined) => {
  const requestHandler = (req, res) => apiResolver(req, res, params, reqHandler);
  const server = http.createServer(requestHandler);
  const url = await listen(server);
  return [url, server];
};

export const tick = () => new Promise(resolve => setTimeout(resolve, 0));

export const buildMockResponse = (body = undefined, init = undefined) => req =>
  new Promise((resolve, reject) => {
    const finalData = {};
    if (body) finalData.body = JSON.stringify(body);
    if (init) finalData.init = init;
    resolve(finalData);
  });

export const checkProps = (component, expectedProps) =>
  checkPropTypes(component.propTypes, expectedProps, 'props', component.name);

export const findByTestAttr = (comp, attr, type = '') => {
  const wrapper = comp.find(`${type}[data-testid="${attr}"]`);
  return wrapper;
};

export const mockWindowEvents = () => {
  const map = {};
  window.addEventListener = jest.fn().mockImplementation((event, cb) => {
    map[event] = cb;
  });
  return map;
};

export const mountReactHook = hook => {
  const Comp = ({ children }) => children(hook());
  const componentHook = {};
  let componentMount;

  act(() => {
    componentMount = shallow(
      <Comp>
        {hookValues => {
          Object.assign(componentHook, hookValues);
          return null;
        }}
      </Comp>
    );
  });

  return { componentHook, componentMount };
};
