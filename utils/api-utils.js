import 'cross-fetch/polyfill';
import ApiError from '../errors/api-error';

const getResponseBody = async res => {
  if (!res.body) return null;
  const text = await res.text();
  try {
    const data = JSON.parse(text);
    return data;
  } catch (error) {
    return text;
  }
};

const buildError = (res, body) => {
  if (typeof body === 'object' && body !== null && body !== undefined) {
    const errorData = { ...body, status: res.status, message: body.message || res.statusText };
    return new ApiError(errorData);
  }
  if (typeof body === 'string') {
    return new ApiError({ status: res.status, message: body });
  }
  return new ApiError({ status: res.status, message: res.statusText });
};

const fetcher = async (path, options, pathInfo = undefined) => {
  try {
    let url = path;
    if (pathInfo) {
      const { host, protocol } = pathInfo || {};
      const proto = process.env.NODE_ENV === 'production' ? 'https:' : protocol;
      const baseUrl = host && proto ? `${proto}//${host}` : process.env.BASE_URL;
      const newURL = new URL(path, baseUrl);
      url = newURL.href;
    } else if (process.env.NODE_ENV === 'test') {
      const baseUrl = process.env.BASE_URL;
      const newURL = new URL(path, baseUrl);
      url = newURL.href;
    }
    const res = await fetch(url, options);
    const body = await getResponseBody(res);
    if (res.ok) return body;
    throw buildError(res, body);
  } catch (error) {
    if (error instanceof ApiError) throw error;
    throw new ApiError(error.message || '');
  }
};

const fetchData = (path, options, pathInfo) => fetcher(path, options, pathInfo);

export { fetcher, fetchData };
