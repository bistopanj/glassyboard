export const extractSubdomain = host => {
  const pattern = /(.+)(\.glassyboard\.com|\.localhost.*)/g;
  const matches = pattern.exec(host);
  return matches ? matches[1] : '';
};

export const getFullPathInfo = ctx => {
  let host;
  let protocol;
  if (ctx.isServer) {
    host = ctx.req.headers.host;
    protocol = ctx.req.socket.encrypted ? 'https:' : 'http:';
  } else {
    host = window.location.host;
    protocol = window.location.protocol;
  }
  const subdomain = extractSubdomain(host);
  return { host, protocol, subdomain };
};
