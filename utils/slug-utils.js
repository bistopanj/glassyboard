import slugify from 'slugify';

const buildSimpleSlug = string => {
  let output = string;
  output = output.replace(/đ/gi, 'd');
  // Trim the last whitespace
  output = output.replace(/\s*$/g, '');
  // Change whitespace to "-"
  output = output.replace(/\s+/g, '-');

  return output;
};

const buildSlug = string => {
  const slug = slugify(string, { lower: true });
  if (slug.length > 0) return slug;
  return buildSimpleSlug(string);
};

export default buildSlug;
