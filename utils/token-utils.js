import jwt from 'jsonwebtoken';

const checkValidity = () => true;

const extractUser = token => {
  try {
    const payload = jwt.decode(token);
    const { user = null } = payload || {};

    return user;
  } catch (error) {
    return null;
  }
};

export { extractUser, checkValidity };
