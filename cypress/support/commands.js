// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

// import { checkingInbox, baseMailSacUrl } from '../constants';
// import findLinks from '../utils';

// Cypress.Commands.add('lookForEmailAfter', sentAfter => {
//   cy.request({
//     method: 'GET',
//     url: `${baseMailSacUrl}/${checkingInbox}/messages`,
//   }).then(res => findLinks(res.body, sentAfter));
// });

import 'cypress-wait-until';
import { lookForEmailAfter } from '../utils';

Cypress.Commands.add('waitForLink', afterTime =>
  cy
    .waitUntil(
      () =>
        cy
          .wrap(afterTime)
          .pipe(lookForEmailAfter)
          .then(links => (links.length > 0 ? links : false)),
      {
        errorMsg: 'No links received',
        timeout: 5000,
        interval: 1000,
      }
    )
    .then(links => links[0])
);
