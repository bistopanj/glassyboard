import getUrls from 'get-urls';
import {
  testEmailAddress,
  checkingInbox,
  baseMailSacUrl,
  token,
  mailSacApiKey,
} from '../constants';

export const login = () => {
  cy.setCookie('token', token);
};

export const addBoardWithAnswer = ref => {
  let boardSlug;
  const topic = `new topic ${Math.floor(1000 * Math.random())}`;
  cy.request({
    method: 'POST',
    url: 'api/boards',
    body: { topic },
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
    .then(res => {
      const answer = `new answer ${Math.floor(10000 * Math.random())}`;
      const { id, slug } = res.body;
      boardSlug = slug;
      return cy.request({
        method: 'POST',
        url: 'api/answers',
        body: { answer, boardId: id },
        headers: {
          Authorization: `Bearer ${token}`,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      });
    })
    .then(() => boardSlug)
    .as(ref);
};

export const addBoard = (topic, ref) => {
  cy.request({
    method: 'POST',
    url: 'api/boards',
    body: { topic },
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
    .its('body')
    .as(ref);
};

export const findLinks = (emails, receivedAfter = new Date('2000-01-01')) => {
  const candidates = emails
    .filter(e => e.links && e.subject.includes('Glassy Board'))
    .map(e => ({ links: e.links, time: new Date(e.received) }))
    .filter(e => e.time >= receivedAfter);
  candidates.sort((a, b) => b.time - a.time);
  const candLinks = candidates
    .map(d => d.links)
    .reduce((acc, ls) => [...acc, ...ls], [])
    .filter(l => l.includes('/auth/ticket?ticketId='));
  return candLinks;
};

const filterGlassyBoardEmails = (emails, receivedAfter = new Date('2000-01-01')) => {
  const gbEmails = emails.filter(
    e =>
      e.from[0].address.includes('glassyboard.com') &&
      e.to[0].address === testEmailAddress &&
      new Date(e.received) >= receivedAfter
  );
  const sorted = gbEmails.sort((a, b) => b.time - a.time);
  return sorted;
};

const fetchEmailBodies = async emails => {
  return Promise.all(
    emails.map(e =>
      fetch(`${baseMailSacUrl}/text/${checkingInbox}/${e._id}?_mailsacKey=${mailSacApiKey}`)
        .then(res => res.text())
        .then(body => {
          e.body = body;
          return e;
        })
    )
  );
};

export const lookForEmailAfter = time => {
  return cy
    .request({
      method: 'GET',
      url: `${baseMailSacUrl}/addresses/${checkingInbox}/messages?_mailsacKey=${mailSacApiKey}`,
    })
    .then(res => {
      const gbEmails = filterGlassyBoardEmails(res.body, time);
      const fullEmails = fetchEmailBodies(gbEmails);
      return fullEmails;
    })
    .then(fullEmails => {
      const links = fullEmails
        .map(e => getUrls(e.body))
        .reduce((acc, ls) => [...acc, ...ls], [])
        .filter(l => l.includes('/auth/ticket?ticketId='));
      return links;
    });
};
