import { useEffect, useCallback } from 'react';

const useOnClickOutside = (ref, handler, disable = false) => {
  const listener = useCallback(
    e => {
      if (disable || !ref.current || ref.current.contains(e.target)) return;

      handler(e);
    },
    [ref, disable]
  );

  useEffect(() => {
    document.addEventListener('click', listener);
    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);
    return () => {
      document.removeEventListener('click', listener);
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, handler]);
};

export default useOnClickOutside;
