const withCSS = require('@zeit/next-css');
require('dotenv').config({
  path: process.env.NODE_ENV === 'production' ? './.env.production' : './.env',
});

module.exports = withCSS({
  env: {
    MAILJET_APIKEY_PUBLIC: process.env.MAILJET_APIKEY_PUBLIC,
    MAILJET_APIKEY_PRIVATE: process.env.MAILJET_APIKEY_PRIVATE,
    REDIS_URI: process.env.REDIS_URI,
    BASE_URL: process.env.BASE_URL,
    POSTGRES_DATABASE: process.env.POSTGRES_DATABASE,
    JWT_SECRET: process.env.JWT_SECRET,
    GOOGLE_SIGN_IN_CLIENT_ID: process.env.GOOGLE_SIGN_IN_CLIENT_ID,
    GOOGLE_APPLICATION_CREDENTIALS: process.env.GOOGLE_APPLICATION_CREDENTIALS,
    OWNER_EMAIL: process.env.OWNER_EMAIL,
    COOKIE_DOMAIN: process.env.COOKIE_DOMAIN,
  },
  cssLoaderOptions: {
    url: false,
  },
  // cssModules: true,
  webpack(cfg) {
    const originalEntry = cfg.entry;
    cfg.entry = async () => {
      const entries = await originalEntry();

      if (entries['main.js'] && !entries['main.js'].includes('./client/polyfills.js')) {
        entries['main.js'].unshift('./client/polyfills.js');
      }

      return entries;
    };

    return cfg;
  },
});

// module.exports = withCSS({
//   cssLoaderOptions: {
//     url: false,
//   },
//   cssModules: true,
// webpack: function(cfg) {
//   const originalEntry = cfg.entry;
//   cfg.entry = async () => {
//     const entries = await originalEntry();

//     if (entries['main.js'] && !entries['main.js'].includes('./client/polyfills.js')) {
//       entries['main.js'].unshift('./client/polyfills.js');
//     }

//     return entries;
//   };

//   return cfg;
// },
// });
