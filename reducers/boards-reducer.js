import actionTypes from '../actions/types';

export const initialState = {
  new: { loading: false, success: null, error: null },
  primary: null,
  bySlug: {
    /* 
    [slug]: {
      answers: [], // list of ids
      answerById: {}, // map from id to full answer
      newAnswer: {
        loading: true | false,
        error: {},
        success: true | false,
      },
      data: {
        created_at: '',
        updated_at: '',
        id: '',
        slug: '',
        topic: '',
      },
      ,
    }
    */
  },
};

export const buildBoardBySlugState = boardData => {
  const { answers = [], ...rest } = boardData;
  return {
    data: rest,
    answers: answers.map(a => a.id),
    answerById: answers.reduce((acc, a) => ({ ...acc, [a.id]: a }), {}),
  };
};

const calculatedNewUpvoteStates = (state, answerId, slug) => {
  const prevUpvoted = state.bySlug[slug].answerById[answerId].upvoted || false;
  const prevUpvotes = state.bySlug[slug].answerById[answerId].upvotes || 0;
  return {
    upvoted: !prevUpvoted,
    upvotes: prevUpvoted ? prevUpvotes - 1 : prevUpvotes + 1,
  };
};

const extractBoardsFromPanels = panels =>
  panels.reduce((acc, { boards }) => [...acc, ...boards], []);

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.removeAnswerSuccess:
      return {
        ...state,
        bySlug: {
          ...state.bySlug,
          [action.payload.boardSlug]: {
            ...state.bySlug[action.payload.boardSlug],
            answers: state.bySlug[action.payload.boardSlug].answers.filter(
              a => a !== action.payload.answerId
            ),
          },
        },
      };
    case actionTypes.updateBoardLoading:
      return {
        ...state,
        bySlug: {
          ...state.bySlug,
          [action.payload.slug]: {
            ...state.bySlug[action.payload.slug],
            data: {
              ...state.bySlug[action.payload.slug].data,
              topic: action.payload.topic,
            },
          },
        },
      };
    case actionTypes.updateBoardSuccess:
      return {
        ...state,
        bySlug: {
          ...state.bySlug,
          [action.payload.slug]: {
            ...state.bySlug[action.payload.slug],
            data: {
              ...state.bySlug[action.payload.slug].data,
              updated_at: action.payload.updatedBoard.updated_at,
            },
          },
        },
      };
    case actionTypes.updateBoardFailed:
      return {
        ...state,
        bySlug: {
          ...state.bySlug,
          [action.payload.slug]: {
            ...state.bySlug[action.payload.slug],
            data: {
              ...state.bySlug[action.payload.slug].data,
              topic: action.payload.oldTopic,
            },
          },
        },
      };
    case actionTypes.getPanelsSuccess:
      return {
        ...state,
        bySlug: {
          ...state.bySlug,
          ...extractBoardsFromPanels(action.payload).reduce(
            (acc, b) => ({ ...acc, [b.slug]: buildBoardBySlugState(b) }),
            {}
          ),
        },
      };
    case actionTypes.upvoteLoading:
      return {
        ...state,
        bySlug: {
          ...state.bySlug,
          [action.payload.slug]: {
            ...state.bySlug[action.payload.slug],
            answerById: {
              ...state.bySlug[action.payload.slug].answerById,
              [action.payload.answerId]: {
                ...state.bySlug[action.payload.slug].answerById[action.payload.answerId],
                ...calculatedNewUpvoteStates(state, action.payload.answerId, action.payload.slug),
              },
            },
          },
        },
      };
    case actionTypes.addAnswerSuccess:
      return {
        ...state,
        bySlug: {
          ...state.bySlug,
          [action.payload.slug]: {
            ...state.bySlug[action.payload.slug],
            answers: [action.payload.answer.id, ...state.bySlug[action.payload.slug].answers],
            answerById: {
              ...state.bySlug[action.payload.slug].answerById,
              [action.payload.answer.id]: { ...action.payload.answer },
            },
            newAnswer: { loading: false, success: true, error: null },
          },
        },
      };
    case actionTypes.addAnswerLoading:
      return {
        ...state,
        bySlug: {
          ...state.bySlug,
          [action.payload]: {
            ...state.bySlug[action.payload],
            newAnswer: { loading: true, success: null, error: null },
          },
        },
      };
    case actionTypes.addAnswerReset:
      return {
        ...state,
        bySlug: {
          ...state.bySlug,
          [action.payload]: {
            ...state.bySlug[action.payload],
            newAnswer: { loading: false, success: null, error: null },
          },
        },
      };
    case actionTypes.addBoardReset:
      return { ...state, new: { ...initialState.new } };
    case actionTypes.addBoardLoading:
      return { ...state, new: { ...initialState.new, loading: true } };
    case actionTypes.addBoardSuccess:
      return {
        ...state,
        bySlug: { ...state.bySlug, [action.payload.slug]: buildBoardBySlugState(action.payload) },
        new: { ...initialState.new, loading: false, error: null, success: action.payload.slug },
      };
    case actionTypes.getBoardLoading:
      return { ...state, bySlug: { ...state.bySlug, [action.payload]: { loading: true } } };
    case actionTypes.getBoardSuccess:
      return {
        ...state,
        bySlug: {
          ...state.bySlug,
          [action.payload.slug]: buildBoardBySlugState(action.payload.data),
        },
      };
    case actionTypes.getBoardFailed:
      return {
        ...state,
        bySlug: { ...state.bySlug, [action.payload.slug]: { error: action.payload.error } },
      };
    case actionTypes.getCompanyBoardSuccess:
      return {
        ...state,
        primary: action.payload.slug,
        bySlug: {
          ...state.bySlug,
          [action.payload.slug]: buildBoardBySlugState(action.payload),
        },
      };
    default:
      return state;
  }
};
