import { combineReducers } from 'redux';
import boardsReducer, { initialState as boardsInitialState } from './boards-reducer';
import connectionInfoReducer, {
  initialState as connectionInfoInitialState,
} from './connection-info-reducer';
import loginReducer, { initialState as loginInitialState } from './login-reducer';
import companyReducer, { initialState as companyInitialState } from './company-reducer';

export const initialState = {
  boards: boardsInitialState,
  connectionInfo: connectionInfoInitialState,
  login: loginInitialState,
  company: companyInitialState,
};

export default combineReducers({
  boards: boardsReducer,
  connectionInfo: connectionInfoReducer,
  login: loginReducer,
  company: companyReducer,
});
