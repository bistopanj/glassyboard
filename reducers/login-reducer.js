import actionTypes from '../actions/types';

export const initialState = {
  isOpen: false,
  data: null,
  google: { loading: false, error: null, success: false, authResult: null },
  ticketRequest: { loading: false, error: null, success: false },
  ticket: { loading: false, error: null, success: false, authResult: null },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.openLogin:
      return { ...state, isOpen: true, data: { ...action.payload } };
    case actionTypes.closeLoginModal:
      return { ...initialState, isOpen: false };
    case actionTypes.authWithGoogleLoading:
      return { ...state, google: { loading: true, error: null, success: false, authResult: null } };
    case actionTypes.authWithGoogleSuccess:
      return {
        ...state,
        google: { loading: false, error: null, success: true, authResult: { ...action.payload } },
      };
    case actionTypes.authWithGoogleFailed:
      return {
        ...state,
        google: { loading: false, error: { ...action.payload }, success: false, authResult: null },
      };
    case actionTypes.askTicketLoading:
      return { ...state, ticketRequest: { loading: true, error: null, success: false } };
    case actionTypes.askTicketFailed:
      return {
        ...state,
        ticketRequest: { loading: false, success: false, error: { ...action.payload } },
      };
    case actionTypes.askTicketSuccess:
      return { ...state, ticketRequest: { loading: false, error: null, success: true } };
    case actionTypes.authWithTicketLoading:
      return { ...state, ticket: { loading: true, error: null, success: false, authResult: null } };
    case actionTypes.authWithTicketSuccess:
      return {
        ...state,
        ticket: { loading: false, error: null, success: true, authResult: { ...action.payload } },
      };
    case actionTypes.authWithTicketFailed:
      return {
        ...state,
        ticket: { loading: false, error: { ...action.payload }, success: false, authResult: null },
      };
    default:
      return state;
  }
};
