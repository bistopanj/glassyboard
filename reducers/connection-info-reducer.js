import { actionTypes } from '../actions';

export const initialState = {
  token: null,
  user: null,
  host: null,
  protocol: null,
  subdomain: null,
};

const connectionInfoReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.updateConnectionInfo:
      return { ...state, token: null, user: null, ...action.payload };
    case actionTypes.authLogout:
      return { ...initialState };
    default:
      return state;
  }
};

export default connectionInfoReducer;
