const loadingStatus = {
  notLoaded: 'NOT_LOADED',
  loaded: 'LOADED',
  loading: 'LOADING',
  error: 'ERROR',
};

export default loadingStatus;
