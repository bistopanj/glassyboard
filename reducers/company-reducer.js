import { actionTypes } from '../actions';

export const initialState = {
  new: { loading: false, success: null, error: null },
  data: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.addCompanyLoading:
      return { ...initialState, new: { ...initialState.new, loading: true } };
    case actionTypes.addCompanySuccess:
      return {
        data: { ...action.payload },
        new: { loading: false, error: null, success: true },
      };
    case actionTypes.addCompanyFailed:
      return {
        data: null,
        new: { loading: false, success: false, error: { ...action.payload } },
      };
    case actionTypes.addCompanyReset:
      return { ...initialState };
    default:
      return state;
  }
};
