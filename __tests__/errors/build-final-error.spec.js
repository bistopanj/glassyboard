import { buildFinalError } from '../../errors/build-final-error';
import ApiError from '../../errors/api-error';
import UnknownError from '../../errors/unknown-error';
import errorCodes from '../../errors/error-codes';
import errorTypes from '../../errors/error-types';

describe('errors.buildFinalError', () => {
  it('replaces the unknown error code (1) with the given code based on status', () => {
    const errors = [
      new ApiError({ code: 1001, message: 'sample error', moreInfo: 'more information' }),
      new ApiError({ status: 404, message: 'Not Found' }),
      new UnknownError('something went wrong'),
      new ApiError({ code: 2, status: 404 }),
    ];

    const statusMap = { 404: 1002 };
    const replaceunknownWith = 3;

    const expectedErrorsData = [
      new ApiError({ code: 1001, message: 'sample error', moreInfo: 'more information' }).toJSON(),
      new ApiError({ status: 404, message: 'Not Found', code: 1002 }).toJSON(),
      new UnknownError({ message: 'something went wrong', code: 3 }).toJSON(),
      new ApiError({ code: 2, status: 404 }).toJSON(),
    ];
    const finalErrors = errors.map(e => buildFinalError(e, replaceunknownWith, statusMap));
    expect(finalErrors.map(e => e.toJSON())).toEqual(expectedErrorsData);
  });
});
