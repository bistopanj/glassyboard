import { shallow, mount } from 'enzyme';
import makeEditable from '../../hocs/make-editable';
import { findByTestAttr, checkProps } from '../../utils/test-utils';

describe('makeEditable HOC', () => {
  const sampleText = `some text ${Math.floor(10000 * Math.random())}`;
  const SampleComponent = () => {
    return <div>{sampleText}</div>;
  };

  describe('PropTypes', () => {
    it('returns a component that renders without propTypes warning', () => {
      const expectedProps = {
        onConfirm: () => {},
        disableEditing: () => {},
        enableEditing: () => {},
        editing: false,
        text: 'some text',
        className: 'editable-text',
      };
      const EditableComp = makeEditable(SampleComponent);
      const propsErrors = checkProps(EditableComp, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appearance', () => {
    it('renders the wrappedComponent given to it as argument', () => {
      const EditableComp = makeEditable(SampleComponent);
      const wrapper = mount(<EditableComp />);
      const sampleComponent = wrapper.find('SampleComponent');
      expect(sampleComponent).toHaveLength(1);
    });

    it('adds two buttons for confirming or discarding the edited verion when editing is true', () => {
      const EditableComp = makeEditable('h1', 'EditableComp');
      const wrapper = mount(<EditableComp editing />);
      const confirmButton = findByTestAttr(wrapper, 'confirm-edit', 'button');
      const discardButton = findByTestAttr(wrapper, 'discard-edit', 'button');
      expect(confirmButton.getDOMNode()).toBeVisible();
      expect(discardButton.getDOMNode()).toBeVisible();
    });

    it('does not show the edit buttons when the editing is disabled', () => {
      const EditableComp = makeEditable('h1', 'EditableComp');
      const wrapper = mount(<EditableComp editing={false} />);
      const confirmButton = findByTestAttr(wrapper, 'confirm-edit', 'button');
      const discardButton = findByTestAttr(wrapper, 'discard-edit', 'button');
      expect(confirmButton.getDOMNode()).not.toBeVisible();
      expect(discardButton.getDOMNode()).not.toBeVisible();
    });
  });

  describe('behavior', () => {
    it('sets the displayName of the resulting component correctly', () => {
      const randomName = `newComponent${Math.floor(1000 * Math.random())}`;
      const newCompWithName = makeEditable(SampleComponent, randomName);
      const newCompWithoutName = makeEditable(SampleComponent);
      expect(newCompWithName.displayName).toBe(randomName);
      expect(newCompWithoutName.displayName).toBe('Editable (SampleComponent)');
    });

    it('makes the wrapped component contentEditable when the editing prop is sent to it', () => {
      const EditableComp = makeEditable(SampleComponent, 'EditableComp');
      const editableComp = shallow(<EditableComp editing />);
      const sampleComponent = editableComp.find('SampleComponent');
      const expectedProps = {
        contentEditable: true,
        suppressContentEditableWarning: true,
      };
      expect(sampleComponent.props()).toEqual(expect.objectContaining(expectedProps));
    });

    it('disables editing when the discard button is pressed', () => {
      const EditableComp = makeEditable('h1', 'EditableComp');
      const disableEditing = jest.fn();
      const wrapper = mount(<EditableComp editing disableEditing={disableEditing} />);
      const discardButton = findByTestAttr(wrapper, 'discard-edit', 'button');
      discardButton.simulate('click');
      expect(disableEditing).toHaveBeenCalledTimes(1);
    });

    it('discards any changes made to the text when the discard button is clicked', () => {
      const EditableComp = makeEditable('h1', 'EditableComp');
      const wrapper = mount(<EditableComp editing text={sampleText} />);
      const discardButton = findByTestAttr(wrapper, 'discard-edit', 'button');
      const h1 = wrapper.find('h1');
      h1.instance().innerHTML = '12';
      discardButton.simulate('click');

      expect(h1.instance().innerHTML).toBe(sampleText);
    });

    it('sends the new text to to its onEdit Prop to be used in parent component', () => {
      const newTopic = `new Topic ${Math.floor(10000 * Math.random())}`;
      const EditableComp = makeEditable('h1', 'EditableComp');
      const onConfirm = jest.fn();
      const wrapper = mount(<EditableComp editing onConfirm={onConfirm} />);
      const confirmButton = findByTestAttr(wrapper, 'confirm-edit', 'button');
      const h1 = wrapper.find('h1');
      h1.instance().innerHTML = newTopic;
      confirmButton.simulate('click');
      expect(onConfirm).toHaveBeenNthCalledWith(1, newTopic);
    });

    it('disables editing when the confirm button is pressed', () => {
      const newTopic = `new Topic ${Math.floor(10000 * Math.random())}`;
      const EditableComp = makeEditable('h1', 'EditableComp');
      const onConfirm = jest.fn();
      const disableEditing = jest.fn();
      const wrapper = mount(
        <EditableComp editing onConfirm={onConfirm} disableEditing={disableEditing} />
      );
      const confirmButton = findByTestAttr(wrapper, 'confirm-edit', 'button');
      const h1 = wrapper.find('h1');
      h1.instance().innerHTML = newTopic;
      confirmButton.simulate('click');
      expect(disableEditing).toHaveBeenCalledTimes(1);
    });

    it('confirms the changes when the "Enter" key is pressed', () => {
      const newTopic = `new Topic ${Math.floor(10000 * Math.random())}`;
      const EditableComp = makeEditable('h1', 'EditableComp');
      const onConfirm = jest.fn();
      const wrapper = mount(<EditableComp editing onConfirm={onConfirm} />);
      const h1 = wrapper.find('h1');
      h1.instance().innerHTML = newTopic;
      h1.simulate('keyDown', { key: 'Enter', keyCode: 13, which: 13 });
      expect(onConfirm).toHaveBeenNthCalledWith(1, newTopic);
    });

    it('discards the changes when the "Escape" key is pressed', () => {
      const newTopic = `new Topic ${Math.floor(10000 * Math.random())}`;
      const EditableComp = makeEditable('h1', 'EditableComp');
      const onConfirm = jest.fn();
      const disableEditing = jest.fn();
      const wrapper = mount(
        <EditableComp
          editing
          onConfirm={onConfirm}
          disableEditing={disableEditing}
          text={sampleText}
        />
      );
      const h1 = wrapper.find('h1');
      h1.instance().innerHTML = newTopic;

      h1.simulate('keyDown', { key: 'Escape', keyCode: 27, which: 27 });
      expect(disableEditing).toHaveBeenCalledTimes(1);
      expect(h1.instance().innerHTML).toBe(sampleText);
    });

    it('discards the changes when clicked outside of it', () => {
      const newTopic = `new Topic ${Math.floor(10000 * Math.random())}`;
      const EditableComp = makeEditable('h1', 'EditableComp');
      const onConfirm = jest.fn();
      const disableEditing = jest.fn();
      const wrapper = mount(
        <div id="root">
          <EditableComp
            editing
            onConfirm={onConfirm}
            disableEditing={disableEditing}
            text={sampleText}
          />
          <div id="another-elem"> some content</div>
        </div>
      );
      const h1 = wrapper.find('h1');
      h1.instance().innerHTML = newTopic;

      const evt = document.createEvent('HTMLEvents');
      evt.initEvent('click', false, true);
      document.dispatchEvent(evt);

      expect(disableEditing).toHaveBeenCalledTimes(1);
      expect(h1.instance().innerHTML).toBe(sampleText);
    });

    it('calls its enableEditing prop when the user clicked on it and it is not active', () => {
      const EditableComp = makeEditable('h1', 'EditableComp');
      const enableEditing = jest.fn();
      const wrapper = mount(
        <EditableComp editing={false} enableEditing={enableEditing} text={sampleText} />
      );
      const h1 = wrapper.find('h1');

      h1.simulate('click');
      expect(enableEditing).toHaveBeenCalledTimes(1);
    });
  });
});
