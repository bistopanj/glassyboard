import { generateEmail } from '../../services/template-services';

describe('services.template', () => {
  describe('generateEmail', () => {
    it('generates the email data in the appropriate form', () => {
      const expectedResult = {
        Subject: 'sample 1',
        TextPart: 'sample text part 1',
        HTMLPart: '<p>sample html part 1</p>',
      };
      const emailData = generateEmail({ template: 'sample', data: { test: '1' } });
      expect(emailData).toEqual(expectedResult);
    });
  });
});
