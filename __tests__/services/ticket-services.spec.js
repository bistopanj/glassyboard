import { generateTicket, getRedisKey } from '../../services/ticket-services';
import redisClient, { closeRedisInstance } from '../../dbs/redis-cache';

describe('services.ticket', () => {
  describe('generateTicket', () => {
    const email = 'sample-email@someEmailServer.com';
    const ticketAction = 'SOME_ACTION';
    let ticketPayload;
    let ticketInfo;
    let loginInfo;

    beforeEach(() => {
      ticketPayload = {
        name: 'random ticket info',
        fixedData: 'this is fixed',
        randomData: Math.random(),
      };
      ticketInfo = { ticketAction, payload: ticketPayload };
      loginInfo = { email, ticketInfo };
    });

    afterAll(async () => {
      await closeRedisInstance();
    });

    it('generates a valid ticket id in uuid v4 format', async () => {
      expect.assertions(1);
      const ticketId = await generateTicket(loginInfo);
      const toUUID = Buffer.from(ticketId, 'base64').toString('ascii');
      expect(toUUID).toMatch(
        /^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i
      );
    });

    it('saves the generated ticket to redis', async () => {
      expect.assertions(1);
      const ticketId = await generateTicket(loginInfo);
      const fromRedisStr = await redisClient.getAsync(getRedisKey(ticketId));
      const fromRedis = JSON.parse(fromRedisStr);

      expect(fromRedis).toEqual(loginInfo);
      await redisClient.delAsync(getRedisKey(ticketId));
    });
  });
});
