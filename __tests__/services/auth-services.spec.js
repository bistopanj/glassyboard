import fetch from 'cross-fetch';
import {
  authMiddleware,
  generateToken,
  loginUser,
  verifyToken,
} from '../../services/auth-services';
import { setUpServer } from '../../utils/test-utils';
import { token as testToken } from '../../cypress/constants';
import { closeDbConnections, query } from '../../dbs/pg-db';

describe('services.auth', () => {
  afterAll(async () => {
    await closeDbConnections();
  });

  describe('loginUser', () => {
    const sampleEmail = 'sample@email.com';
    const userInfo = {
      email: sampleEmail,
      firstName: 'some first name',
      lastName: 'some last name',
    };

    it('returns a valid token containing the userInfo given to it', async () => {
      expect.assertions(2);
      const { token, user } = await loginUser(userInfo);
      const payload = await verifyToken(token);
      expect(payload.user).toEqual(user);
      expect(user.email).toBe(userInfo.email);
    });

    it('returns the user id in resolved value', async () => {
      expect.assertions(1);
      const { user } = await loginUser(userInfo);
      expect(user.id).toBeDefined();
    });

    it('updates the user info with the new values if the old values are null', async () => {
      expect.assertions(1);
      await query('UPDATE users SET first_name=null, last_name=null WHERE email=($1);', [
        sampleEmail,
      ]);

      const { user } = await loginUser(userInfo);
      expect(user).toEqual(expect.objectContaining(userInfo));
    });

    it('does not update the user info if the old values are not null', async () => {
      expect.assertions(2);
      const anotherFirstName = 'another first name';
      await query(`UPDATE users SET first_name=($2), last_name=null WHERE email=($1);`, [
        sampleEmail,
        anotherFirstName,
      ]);
      const { user } = await loginUser(userInfo);
      expect(user.firstName).toBe(anotherFirstName);
      expect(user.lastName).toBeNull();
    });
  });

  describe('authMiddleware', () => {
    const exUser = { id: '1', email: 'sample@email.com', ex_id: '5678' };
    const invalidToken = `${testToken}randomchars`;

    let server;
    let url;

    const simpleHandler = jest.fn();
    simpleHandler.mockImplementation((req, res) => res.json({ result: 'ok' }));

    beforeEach(async () => {
      jest.clearAllMocks();
      const combinedHandler = authMiddleware(simpleHandler);
      const [theUrl, theServer] = await setUpServer(combinedHandler);
      url = theUrl;
      server = theServer;
    });

    afterEach(async () => {
      await server.close();
    });

    it('rejects the request if no authentication token is provided', async () => {
      expect.assertions(3);
      const res = await fetch(url);
      const response = await res.json();

      expect(simpleHandler).not.toHaveBeenCalled();
      expect(res.status).toBe(401);
      expect(response.error.code).toBe(2004);
    });

    it('rejects the request if an invalid authentication token is provided', async () => {
      expect.assertions(3);
      const res = await fetch(url, {
        method: 'post',
        credentials: 'include',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${invalidToken}`,
          // Cookie: `token=${invalidToken}`,
        },
        body: JSON.stringify({ data: 'some data' }),
      });
      const response = await res.json();
      expect(simpleHandler).not.toHaveBeenCalled();
      expect(res.status).toBe(401);
      expect(response.error.code).toBe(2005);
    });

    it('rejects the request if an expired token is provided', async () => {
      expect.assertions(3);
      const { token: expiredToken } = await generateToken(exUser, {
        expiresIn: '-60 days',
        issuer: 'glassyboard.com',
      });

      const res = await fetch(url, {
        method: 'post',
        credentials: 'include',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${expiredToken}`,
          // Cookie: `token=${invalidToken}`,
        },
        body: JSON.stringify({ data: 'some data' }),
      });
      const response = await res.json();
      expect(simpleHandler).not.toHaveBeenCalled();
      expect(res.status).toBe(401);
      expect(response.error.code).toBe(2005);
    });

    it('passes the request to the next middleware if a valid token is provided', async () => {
      expect.assertions(3);
      const { token: validToken } = await generateToken(exUser, {
        expiresIn: '60 days',
        issuer: 'glassyboard.com',
      });
      const res = await fetch(url, {
        method: 'post',
        credentials: 'include',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${validToken}`,
        },
        body: JSON.stringify({ data: 'some data' }),
      });
      const response = await res.json();
      expect(simpleHandler).toHaveBeenCalled();
      expect(res.status).toBe(200);
      expect(response).toEqual({ result: 'ok' });
    });

    it('adds the user data as req.user and passes it to the next middleware if a valid token is provided', async () => {
      expect.assertions(1);
      const { token: validToken, user } = await generateToken(exUser);
      await fetch(url, {
        method: 'post',
        credentials: 'include',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${validToken}`,
        },
        body: JSON.stringify({ data: 'some data' }),
      });

      const passedReq = simpleHandler.mock.calls[0][0];
      expect(passedReq.user).toEqual(user);
    });
  });
});
