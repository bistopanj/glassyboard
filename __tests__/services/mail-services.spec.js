import { sendEmail } from '../../services/mail-services';

describe('services.mail', () => {
  describe('sendEmail', () => {
    const sampleEmail = 'sample@somemail.com';
    it('sends a sample email to a sample address', async () => {
      expect.assertions(1);
      const expectedResult = { status: 'success', email: sampleEmail, errors: undefined };
      const sendResult = await sendEmail({ to: sampleEmail, template: 'sample', data: {} });
      const { email, status, errors } = sendResult;
      const shortSendResult = { email, status, errors };
      expect(shortSendResult).toEqual(expectedResult);
    });
  });
});
