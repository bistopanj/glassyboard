import { raiseJob } from '../../services/job-services';
import * as jobs from '../../jobs';
import ServerError from '../../errors/server-error';

const { default: _, ...rest } = jobs;
const allJobs = [...Object.keys(rest)];

jest.mock('../../jobs', () => ({
  upvote: jest.fn(),
}));

const pickJob = () => allJobs[Math.floor(Math.random() * allJobs.length)];

describe('services.job', () => {
  let validJob;
  let payload;
  const userInfo = { token: 'someToken', ticketId: 'ticket-id' };
  beforeEach(() => {
    jest.resetAllMocks();
    validJob = pickJob();
    payload = { name: 'test name', randomData: 100 * Math.floor(Math.random()) };
  });

  afterAll(() => {
    jest.clearAllMocks();
  });
  describe('valid job', () => {
    it('calls the job handler for the job given to it and passes the payload and userInfo to the job', async () => {
      expect.assertions(1);

      const result = await raiseJob({ job: validJob, payload }, userInfo);
      expect(jobs[validJob]).toHaveBeenNthCalledWith(1, payload, userInfo);
    });

    it('is resolved with value resolved by the job handler', async () => {
      expect.assertions(1);
      const jobResult = { fixedData: 'some data', randomData: Math.floor(1000 * Math.random()) };
      const redirectAddress = `/some-address${Math.floor(100 * Math.random())}`;
      const jobReturns = [jobResult, redirectAddress];
      jobs[validJob].mockResolvedValue(jobReturns);
      const result = await raiseJob({ job: validJob, payload }, userInfo);

      expect(result).toEqual(jobReturns);
    });

    it('always throws a ServerError', async () => {
      expect.assertions(1);
      jobs[validJob].mockImplementation(() => {
        throw new Error('Some Error');
      });
      let error;
      try {
        await raiseJob({ job: validJob, payload }, userInfo);
      } catch (err) {
        error = err;
      }
      expect(error).toBeInstanceOf(ServerError);
    });
  });

  describe('invalid job', () => {
    it('resolves with default values when the job does not exist', async () => {
      expect.assertions(2);
      const invalidJob = `JOB_INVALID_${Math.floor(1000 * Math.random())}`;
      expect(allJobs).not.toContain(invalidJob);
      const result = await raiseJob({ job: invalidJob, payload }, userInfo);

      expect(result).toEqual([null, '/']);
    });
  });
});
