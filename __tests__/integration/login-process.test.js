import fetch from 'cross-fetch';
import { sendEmail } from '../../services/mail-services';
import { raiseJob } from '../../services/job-services';
import { setUpServer } from '../../utils/test-utils';
import ticketsApiHandler from '../../pages/api/tickets';
import authApiHandler from '../../pages/api/auth/ticket';
import { closeRedisInstance } from '../../dbs/redis-cache';
import { closeDbConnections } from '../../dbs/pg-db';

jest.mock('../../services/mail-services', () => ({
  sendEmail: jest.fn(),
}));

jest.mock('../../services/job-services', () => ({
  raiseJob: jest.fn(),
}));

const authenticate = (ticketId, authUrl) =>
  fetch(authUrl, {
    method: 'POST',
    body: JSON.stringify({ ticketId }),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });

describe('login process', () => {
  let ticketServer;
  let authServer;
  let ticketUrl;
  let authUrl;

  beforeAll(async () => {
    const [u1, s1] = await setUpServer(ticketsApiHandler);
    ticketServer = s1;
    ticketUrl = u1;
    const [u2, s2] = await setUpServer(authApiHandler);
    authServer = s2;
    authUrl = u2;
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  afterAll(async () => {
    await closeRedisInstance();
    await closeDbConnections();
    await ticketServer.close();
    await authServer.close();
    jest.clearAllMocks();
  });

  it('raises the given job specified by ticketInfo when the authentication process is complete', async () => {
    expect.assertions(1);

    const ticketAction = 'SOME_ACTION';
    const ticketPayload = {
      name: 'random ticket info',
      fixedData: 'this is fixed',
      randomData: Math.random(),
    };

    const ticketInfo = {
      ticketAction: 'SOME_ACTION',
      payload: ticketPayload,
    };

    const sampleEmail = 'sample@email.com';
    const loginInfo = { email: sampleEmail, ticketInfo };
    await fetch(ticketUrl, {
      method: 'POST',
      body: JSON.stringify(loginInfo),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    const generatedTicket = sendEmail.mock.calls[0][0].data.ticketId;
    await authenticate(generatedTicket, authUrl);
    const args = raiseJob.mock.calls[0];
    expect(args[0]).toEqual({ job: ticketAction, payload: ticketPayload });
  });
});
