import fetch from 'cross-fetch';
import { upsertUser } from '../../services/user-services';
import { generateToken } from '../../services/auth-services';
import { deleteBoardById } from '../../services/board-services';
import { deleteAnswerById } from '../../services/answer-services';
import { closeDbConnections } from '../../dbs/pg-db';
import { setUpServer } from '../../utils/test-utils';
import boardsApiHandler from '../../pages/api/boards';
import answersApiHandler from '../../pages/api/answers';
import boardsBySlugApiHandler from '../../pages/api/boards/[slug]';

const postBoard = async (apiUrl, board, token) => {
  const res = await fetch(apiUrl, {
    method: 'POST',
    body: JSON.stringify(board),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
  return res.json();
};

const postAnswer = async (apiUrl, answer, token) => {
  const res = await fetch(apiUrl, {
    method: 'POST',
    body: JSON.stringify(answer),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
  return res.json();
};

const getBoardBySlug = async (url, slug) => {
  const res = await fetch(`${url}?slug=${slug}`);
  return res.json();
};

describe('Adding answer to topic (api)', () => {
  const sampleEmail = 'sample@email.com';
  let token;
  let boardsApiUrl;
  let boardsApiServer;
  let boardsBySlugApiUrl;
  let boardsBySlugApiServer;
  let answersApiUrl;
  let answersApiServer;
  let newBoard;

  beforeAll(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    const { token: t } = await generateToken(intUser);
    const [u1, s1] = await setUpServer(boardsApiHandler);
    const [u2, s2] = await setUpServer(answersApiHandler);
    const [u3, s3] = await setUpServer(boardsBySlugApiHandler);
    token = t;
    boardsApiUrl = u1;
    boardsApiServer = s1;
    answersApiUrl = u2;
    answersApiServer = s2;
    boardsBySlugApiUrl = u3;
    boardsBySlugApiServer = s3;
  });

  beforeEach(async () => {
    const sampleTopic = `new topic ${Math.floor(1000 * Math.random())}`;
    const theBoard = await postBoard(boardsApiUrl, { topic: sampleTopic }, token);
    newBoard = theBoard;
  });

  afterEach(async () => {
    await deleteBoardById(newBoard.id);
  });

  afterAll(async () => {
    await boardsApiServer.close();
    await answersApiServer.close();
    await boardsBySlugApiServer.close();
    await closeDbConnections();
  });

  it('returns the new answers added to an empty board', async () => {
    expect.assertions(1);
    const answerText = `some answer ${Math.floor(10000 * Math.random())}`;
    const newAnswer = await postAnswer(
      answersApiUrl,
      { answer: answerText, boardId: newBoard.id },
      token
    );
    const boardData = await getBoardBySlug(boardsBySlugApiUrl, newBoard.slug);
    const boardAnswers = boardData.answers || [];
    const answerTexts = boardAnswers.map(a => a.answer);

    await deleteAnswerById(newAnswer.id);
    expect(answerTexts).toContain(answerText);
  });
});
