import { buildSubdomainUrl } from '../../utils/subdomain-utils';

describe('utils.subdomain', () => {
  it('adds the subdomain to the given url', () => {
    const subdomain = `sub-domain-${Math.floor(10000 * Math.random())}`;
    const samples = {
      'http://localhost:3000': `http://${subdomain}.localhost:3000/`,
      'http://localhost:3000/': `http://${subdomain}.localhost:3000/`,
      'localhost:3000': `${subdomain}.localhost:3000/`,
      'http://admin.localhost:3000': `http://${subdomain}.admin.localhost:3000/`,
      'https://glassyboard.com': `https://${subdomain}.glassyboard.com/`,
      'https://beta.glassyboard.com': `https://${subdomain}.beta.glassyboard.com/`,
      'https://admin.glassyboard.com': `https://${subdomain}.admin.glassyboard.com/`,
      'https://admin.beta.glassyboard.com': `https://${subdomain}.admin.beta.glassyboard.com/`,
      'https://www.glassyboard.com': `https://${subdomain}.glassyboard.com/`,
    };
    const results = Object.keys(samples).reduce(
      (acc, url) => ({ ...acc, [url]: buildSubdomainUrl(url, subdomain) }),
      {}
    );
    expect(results).toEqual(samples);
  });
});
