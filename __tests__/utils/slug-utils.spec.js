import buildSlug from '../../utils/slug-utils';

describe('utils.slug', () => {
  describe('buildSlug', () => {
    it('fails', () => {
      const string = 'سلام به روی ماهت';
      const slug = buildSlug(string);
      expect(slug.length).toBeGreaterThan(0);
      expect(slug).toMatch(/^\S*$/gi);
    });
  });
});
