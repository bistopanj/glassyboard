import { extractSubdomain } from '../../utils/next-utils';

describe('utils.next.extractSubdomain', () => {
  const subdomainSamplesMap = {
    'admin.localhost:3000': 'admin',
    'somedomain.glassyboard.com': 'somedomain',
    'admin.glassyboard.com': 'admin',
    'glassyboard.com': '',
    'localhost:3000': '',
  };

  it('parses the given url string and returns the correct data for the cases we might need', () => {
    const responses = Object.keys(subdomainSamplesMap).map(host => ({
      host,
      subdomain: extractSubdomain(host),
    }));

    const expectedResponses = Object.keys(subdomainSamplesMap).map(host => ({
      host,
      subdomain: subdomainSamplesMap[host],
    }));
    expect(expectedResponses).toEqual(responses);
  });
});
