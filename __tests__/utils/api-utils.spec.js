import { fetcher } from '../../utils/api-utils';
import { buildMockResponse } from '../../utils/test-utils';
import ApiError from '../../errors/api-error';

const baseUrl = process.env.BASE_URL;

const range = (start, end) => new Array(end - start + 1).fill(undefined).map((_, i) => i + start);

const checkStatusForPromises = async promises => {
  const allResults = await Promise.all(
    promises.map(p =>
      p.then(res => ({ resolved: true, response: res })).catch(e => ({ rejected: true, error: e }))
    )
  );

  return allResults;
};

describe('utils.api.fetcher', () => {
  describe('successful response', () => {
    beforeEach(() => {
      fetch.resetMocks();
    });

    it('resolves with response body when status is 200', async () => {
      expect.assertions(1);
      const responseBody = { name: 'tester', age: 34 };
      fetch.mockResponse(buildMockResponse(responseBody, { status: 200 }));
      const response = await fetcher(baseUrl);
      // console.log(response);
      expect(response).toEqual(responseBody);
    });

    it('resolves with null when status is 200 and body is empty', async () => {
      expect.assertions(1);
      fetch.mockResponse(buildMockResponse(undefined, { status: 200 }));
      const response = await fetcher(baseUrl);
      expect(response).toBeNull();
    });

    it('resolves with text when the status is 200 and body is text', async () => {
      expect.assertions(1);
      const sampleBody = 'sample body';
      fetch.mockResponse(buildMockResponse(sampleBody, { status: 200 }));
      const response = await fetcher(baseUrl);
      expect(response).toBe(sampleBody);
    });

    it('resolves with the response body when any success status is received', async () => {
      expect.assertions(1);
      const statusCodes = [...range(200, 208), 226];
      const expectedResponses = statusCodes.map(sc => ({ statusCode: sc }));

      statusCodes.reduce(
        (acc, sc) => acc.mockResponseOnce(buildMockResponse({ statusCode: sc }, { status: sc })),
        fetch
      );
      const responses = await Promise.all(statusCodes.map(() => fetcher(baseUrl)));

      expect(responses).toEqual(expectedResponses);
    });
  });

  describe('unsuccessful response', () => {
    beforeEach(() => {
      fetch.resetMocks();
    });

    it('is rejected with an appropriate ApiError when status is not in 2** range', async () => {
      expect.assertions(1);
      const statusCodes = [
        ...range(100, 103),
        ...range(300, 308),
        ...range(400, 431),
        451,
        ...range(500, 511),
      ];
      const expectedResults = statusCodes.map(sc => ({
        rejected: true,
        error: new ApiError({
          status: sc,
          message: `status is ${sc}`,
          code: sc,
          moreInfo: `visit docs for status ${sc}`,
        }),
      }));

      statusCodes.reduce(
        (acc, sc) =>
          acc.mockResponseOnce(
            buildMockResponse(
              { code: sc, message: `status is ${sc}`, moreInfo: `visit docs for status ${sc}` },
              { status: sc }
            )
          ),
        fetch
      );
      const fetchResults = await checkStatusForPromises(statusCodes.map(() => fetcher(baseUrl)));
      expect(fetchResults).toEqual(expectedResults);
    });

    it('throws an ApiError containing the error message when the fetch is rejected for some reason', async () => {
      expect.assertions(1);
      const errorMessage = 'something went wrong';
      const expectedResult = { rejected: true, error: new ApiError(errorMessage) };
      fetch.mockReject(new Error(errorMessage));
      const results = await checkStatusForPromises([fetcher(baseUrl)]);

      expect(results[0]).toEqual(expectedResult);
    });
  });
});
