import fetch from 'cross-fetch';
import { setUpServer } from '../../../../utils/test-utils';
import handleReqs from '../../../../pages/api/upvotes';
import { generateToken } from '../../../../services/auth-services';
import { raiseJob } from '../../../../services/job-services';

jest.mock('../../../../services/job-services', () => ({
  raiseJob: jest.fn(),
}));

const intUser = { id: '1', email: 'sample@email.com', ex_id: '5678' };

const upvoteAnswer = async (url, token, upvoteData) => {
  const res = await fetch(url, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(upvoteData),
  });
  const response = await res.json();
  return response;
};

describe('/api/upvotes', () => {
  let server;
  let url;
  let token;
  let user;

  beforeAll(async () => {
    const [serverUrl, theServer] = await setUpServer(handleReqs);
    const { token: validToken, user: theUser } = await generateToken(intUser);
    token = validToken;
    server = theServer;
    url = serverUrl;
    user = theUser;
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  afterAll(async () => {
    await server.close();
  });

  describe('POST', () => {
    it('raises the UPVOTE job and passes the received payload to it', async () => {
      expect.assertions(2);
      const upvoteData = {
        answerId: '1235-12312asdas-asda',
        boardId: '81237-asdas1-12312csxc',
        slug: 'some-sample-slug',
      };
      const jobResult = {
        id: '1234',
        active: true,
        created_at: 'some-date',
        updated_at: 'some-date',
        user_id: intUser.ex_id,
        answer_id: upvoteData.answerId,
        board_id: upvoteData.boardId,
      };
      raiseJob.mockResolvedValue([jobResult, '/some-address']);
      const response = await upvoteAnswer(url, token, upvoteData);

      expect(raiseJob).toHaveBeenNthCalledWith(1, { job: 'upvote', payload: upvoteData }, { user });
      expect(response).toEqual(jobResult);
    });
  });
});
