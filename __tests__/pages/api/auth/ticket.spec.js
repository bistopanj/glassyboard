import fetch from 'cross-fetch';
import { setUpServer } from '../../../../utils/test-utils';
import authApiHandler from '../../../../pages/api/auth/ticket';
import redisClient, { closeRedisInstance } from '../../../../dbs/redis-cache';
import { closeDbConnections } from '../../../../dbs/pg-db';
import { getRedisKey } from '../../../../services/ticket-services';
import { raiseJob } from '../../../../services/job-services';

const jwtPattern = /^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/g;

jest.mock('../../../../services/job-services', () => ({
  raiseJob: jest.fn(),
}));

const authenticate = (ticketId, authUrl) =>
  fetch(authUrl, {
    method: 'POST',
    body: JSON.stringify({ ticketId }),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });

describe('/api/auth/ticket', () => {
  let server;
  let url;

  beforeAll(async () => {
    const [authUrl, authServer] = await setUpServer(authApiHandler);
    server = authServer;
    url = authUrl;
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  afterAll(async () => {
    await closeRedisInstance();
    await closeDbConnections();
    await server.close();
    jest.clearAllMocks();
  });

  describe('invalid ticket', () => {
    it('responds with appropriate error object when an invalid ticketId is given', async () => {
      expect.assertions(1);
      const wrongTicketId = 'invalid';
      const redisKey = getRedisKey(wrongTicketId);
      await redisClient.delAsync(redisKey);
      const expectedResult = {
        status: 403,
        body: {
          error: {
            code: 1001,
            type: 'ServerError',
            message: 'authentication failed',
            moreInfo:
              'the given ticket is expired or wrong. please try to login again and get a new ticket.',
          },
        },
      };
      const res = await authenticate(wrongTicketId, url);
      const responseBody = await res.json();
      const authResult = { status: res.status, body: responseBody };

      expect(authResult).toEqual(expectedResult);
    });
  });

  describe('valid ticket', () => {
    const validTicket = '8aea2f18-d953-4a42-bc7d-29b4c1af8bc5';
    const ticketAction = 'SOME_ACTION';
    const ticketPayload = {
      name: 'random ticket info',
      fixedData: 'this is fixed',
      randomData: Math.random(),
    };
    const ticketInfo = {
      ticketAction,
      payload: ticketPayload,
    };
    const sampleEmail = 'sample@email.com';
    const loginInfo = { email: sampleEmail, ticketInfo };
    const redisKey = getRedisKey(validTicket);
    const redirectAddress = `/some-address-${Math.floor(1000 * Math.random())}`;

    beforeEach(async () => {
      jest.resetAllMocks();
      await redisClient.setAsync(redisKey, JSON.stringify(loginInfo));
      raiseJob.mockResolvedValue(['ok', redirectAddress]);
    });

    afterEach(async () => {
      await redisClient.delAsync(redisKey);
    });

    it('returns a valid token in response to a valid ticket', async () => {
      expect.assertions(2);
      const res = await authenticate(validTicket, url);
      const responseBody = await res.json();
      expect(res.status).toBe(200);
      expect(responseBody.token).toMatch(jwtPattern);
    });

    it('returns user email in user field of the response to a valid ticket', async () => {
      expect.assertions(1);
      const res = await authenticate(validTicket, url);
      const responseBody = await res.json();
      expect(responseBody.user.email).toBe(sampleEmail);
    });

    it('raises the job specified in ticket', async () => {
      expect.assertions(2);
      await authenticate(validTicket, url);
      expect(raiseJob).toHaveBeenCalledTimes(1);
      const args = raiseJob.mock.calls[0];

      expect(args[0]).toEqual({ job: ticketAction, payload: ticketPayload });
    });

    it('returns the redirect address received from raising the job', async () => {
      expect.assertions(1);
      const res = await authenticate(validTicket, url);
      const responseBody = await res.json();
      expect(responseBody.redirectTo).toBe(redirectAddress);
    });
  });
});
