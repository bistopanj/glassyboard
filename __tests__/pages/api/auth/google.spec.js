import fetch from 'cross-fetch';
import { setUpServer } from '../../../../utils/test-utils';
import authWithGoogleApiHandler from '../../../../pages/api/auth/google';
import { validateGoogleIdToken } from '../../../../services/google-auth-services';
import { closeDbConnections } from '../../../../dbs/pg-db';
import { raiseJob } from '../../../../services/job-services';

jest.mock('../../../../services/google-auth-services', () => ({
  validateGoogleIdToken: jest.fn(),
}));

jest.mock('../../../../services/job-services', () => ({
  raiseJob: jest.fn(),
}));

// jest.autoMockOff();

const jwtPattern = /^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/g;

const postToAuthGoogle = async (body, url) =>
  fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });

describe('/api/auth/google', () => {
  let server;
  let url;
  const googleToken = 'acbe.123.efghi';
  const ticketAction = 'SOME-ACTION';
  const ticketPayload = { someData: 'value' };
  const data = { ticketAction, payload: ticketPayload };
  const sampleEmail = 'sample@gmail.com';
  const firstName = 'first name';
  const lastName = 'last name';

  const sampleUserInfo = { email: sampleEmail, firstName, lastName };
  const redirectAddress = `/some/address/${Math.floor(1000 * Math.random())}`;
  validateGoogleIdToken.mockResolvedValue(sampleUserInfo);

  beforeAll(async () => {
    const [serverUrl, theServer] = await setUpServer(authWithGoogleApiHandler);
    server = theServer;
    url = serverUrl;
  });

  beforeEach(() => {
    jest.clearAllMocks();
    raiseJob.mockResolvedValue(['ok', redirectAddress]);
  });

  afterAll(async () => {
    await server.close();
    await closeDbConnections();
  });

  it('sends a valid token in response to a valid google token', async () => {
    expect.assertions(2);
    const res = await postToAuthGoogle({ googleToken, data }, url);
    const responseBody = await res.json();
    expect(res.status).toBe(200);
    expect(responseBody.token).toMatch(jwtPattern);
  });

  it('raises the job specified in request body', async () => {
    expect.assertions(2);
    await postToAuthGoogle({ googleToken, data }, url);
    expect(raiseJob).toHaveBeenCalledTimes(1);
    const raiseJobArgsReceived = raiseJob.mock.calls[0];
    expect(raiseJobArgsReceived[0]).toEqual({ job: ticketAction, payload: ticketPayload });
  });

  it('returns the redirect address received from raising the job', async () => {
    expect.assertions(1);
    const res = await postToAuthGoogle({ googleToken, data }, url);
    const responseBody = await res.json();
    expect(responseBody.redirectTo).toBe(redirectAddress);
  });
});
