import fetch from 'cross-fetch';
import handleReqs from '../../../../pages/api/tickets';
import { setUpServer } from '../../../../utils/test-utils';
import { sendEmail } from '../../../../services/mail-services';
import { generateTicket } from '../../../../services/ticket-services';

jest.mock('../../../../services/mail-services', () => ({
  sendEmail: jest.fn(),
}));

jest.mock('../../../../services/ticket-services', () => ({
  generateTicket: jest.fn(),
}));

describe('/api/tickets', () => {
  describe('a correct ticket request', () => {
    const ticketId = 'fd8415aa-bbb0-4777-a91b-7cf712c5f390';
    const loginInfo = {
      email: 'test@email.com',
      ticketInfo: { answerId: 'abcd', boardId: '1234', ticketAction: 'UPVOTE' },
    };
    const fetchOptions = {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(loginInfo),
    };
    let url;
    let server;

    beforeEach(async () => {
      const [serverUrl, theServer] = await setUpServer(handleReqs);
      url = serverUrl;
      server = theServer;
      jest.resetAllMocks();
    });

    afterEach(async () => {
      await server.close();
    });

    afterAll(() => {
      jest.clearAllMocks();
    });

    it('responds 200 to POST requests containing necessary data for creating a ticket', async () => {
      expect.assertions(1);
      const expectedResponse = { done: true };
      generateTicket.mockResolvedValue(ticketId);
      const response = await fetch(url, fetchOptions);
      const resBody = await response.json();
      expect(resBody).toEqual(expectedResponse);
    });

    it('sends correct arguments to mail service in order to send the ticket to the customer', async () => {
      expect.assertions(1);
      generateTicket.mockResolvedValue(ticketId);
      const expectedArgs = { to: loginInfo.email, template: 'ticket', data: { ticketId } };
      await fetch(url, fetchOptions);
      expect(sendEmail).toHaveBeenNthCalledWith(1, expectedArgs);
    });

    it('sends the correct arguments to the ticket service in order to generate a new ticket', async () => {
      expect.assertions(1);
      generateTicket.mockResolvedValue(ticketId);
      const expectedArgs = { ...loginInfo };
      await fetch(url, fetchOptions);
      expect(generateTicket).toHaveBeenNthCalledWith(1, expectedArgs);
    });
  });
});
