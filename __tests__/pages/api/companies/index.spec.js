import fetch from 'cross-fetch';
import { generateToken } from '../../../../services/auth-services';
import { setUpServer } from '../../../../utils/test-utils';
import handleReqs from '../../../../pages/api/companies';
import { raiseJob } from '../../../../services/job-services';

jest.mock('../../../../services/job-services');

const intUser = { id: '1', email: 'sample@email.com', ex_id: '5678' };

const postCompany = async (url, token, company) => {
  const res = await fetch(url, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(company),
  });
  const response = await res.json();
  return response;
};

describe('/api/companies', () => {
  let server;

  beforeEach(() => {
    jest.resetAllMocks();
  });

  afterEach(async () => {
    await server.close();
  });

  describe('POST', () => {
    it('raises the ADD-COMPANY jon and passes the received payload to it', async () => {
      expect.assertions(2);
      const { token: validToken, user } = await generateToken(intUser);
      const [serverUrl, theServer] = await setUpServer(handleReqs);
      server = theServer;
      const jobResult = { someData: `some-data-${Math.floor(1000 * Math.random())}` };
      raiseJob.mockResolvedValue([jobResult, '/']);

      const companyData = { subdomain: `some-sub-domain-${Math.floor(10000 * Math.random())}` };
      const response = await postCompany(serverUrl, validToken, companyData);

      expect(raiseJob).toHaveBeenNthCalledWith(
        1,
        { job: 'add-company', payload: companyData },
        { user }
      );
      expect(response).toEqual(jobResult);
    });
  });
});
