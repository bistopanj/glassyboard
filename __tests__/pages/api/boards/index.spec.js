import fetch from 'cross-fetch';
import querystring from 'querystring';
import { query } from '../../../../dbs/pg-db';
import { setUpServer } from '../../../../utils/test-utils';
import handleReqs from '../../../../pages/api/boards';
import { generateToken } from '../../../../services/auth-services';
import { raiseJob } from '../../../../services/job-services';
import { findBoardBySubdomainQuery } from '../../../../dbs/queries';

jest.mock('../../../../dbs/pg-db');

jest.mock('../../../../services/job-services');

const intUser = { id: '1', email: 'sample@email.com', ex_id: '5678' };
let boardData = {
  id: 'c568eba5-e806-4d59-afde-583ae219d48e',
  slug: 'Why-are-you-so-dumb',
  topic: 'Why are you so dumb?',
  created_at: '2020-03-11T13:21:32.743Z',
  updated_at: '2020-03-11T13:21:32.743Z',
  answers: [
    {
      id: '667086e7-7fa0-407e-b587-428ab0997212',
      answer: 'new answer',
      created_at: '2020-03-17T15:26:58.230169+03:30',
      updated_at: '2020-03-17T15:26:58.230169+03:30',
    },
    {
      id: '53dc4074-4bb6-458b-b1b1-549e7c774372',
      answer: 'another answer',
      created_at: '2020-03-17T16:17:55.328538+03:30',
      updated_at: '2020-03-17T16:17:55.328538+03:30',
    },
  ],
};

const postBoard = async (url, token, board) => {
  const res = await fetch(url, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(board),
  });
  const response = await res.json();
  return response;
};

const getBoard = async (url, token, queryParams) => {
  const res = await fetch(`${url}?${querystring.encode(queryParams)}`, {
    method: 'get',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
  const response = await res.json();
  return [response, res];
};

describe('/api/boards', () => {
  let server;
  let url;
  let token;
  let user;

  beforeAll(async () => {
    const [serverUrl, theServer] = await setUpServer(handleReqs);
    const { token: validToken, user: theUser } = await generateToken(intUser);
    server = theServer;
    url = serverUrl;
    token = validToken;
    user = theUser;
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  afterAll(async () => {
    await server.close();
  });

  describe('GET', () => {
    it('returns the board in correct form for correct query params', async () => {
      expect.assertions(1);
      const queryParams = { subdomain: 'company' };
      query.mockResolvedValue({ rows: [boardData] });
      const [response] = await getBoard(url, token, queryParams);
      expect(response).toEqual(boardData);
    });

    it('returns a 404 status with appropriate information for a wrong subdomain', async () => {
      expect.assertions(3);

      const queryParams = { subdomain: 'some-invalid-subdomain' };
      query.mockResolvedValue({ rows: [] });
      const [response, res] = await getBoard(url, token, queryParams);

      expect(query).toHaveBeenNthCalledWith(1, findBoardBySubdomainQuery, [
        queryParams.subdomain,
        user.id,
      ]);
      expect(res.status).toBe(404);
      expect(response.error.code).toBe(2051);
    });

    it('returns a 400 status when the query parameters are not correct', async () => {
      expect.assertions(2);
      const queryParams = { anotherParam: 'some-invalid-subdomain' };
      const [response, res] = await getBoard(url, token, queryParams);
      expect(res.status).toBe(400);
      expect(response.error.code).toBe(2053);
    });
  });

  describe('POST', () => {
    it('raises the ADD-BOARD job and passes the received payload to it', async () => {
      expect.assertions(2);
      const jobResult = { someData: `some-data-${Math.floor(1000 * Math.random())}` };
      raiseJob.mockResolvedValue([jobResult, '/']);
      boardData = { topic: `some-topic-${Math.floor(1000 * Math.random())}` };
      const response = await postBoard(url, token, boardData);

      expect(raiseJob).toHaveBeenNthCalledWith(
        1,
        { job: 'add-board', payload: boardData },
        { user }
      );
      expect(response).toEqual(jobResult);
    });
  });
});
