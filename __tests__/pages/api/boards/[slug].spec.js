import fetch from 'cross-fetch';
import handleReqs from '../../../../pages/api/boards/[slug]';
import { setUpServer } from '../../../../utils/test-utils';
import { query } from '../../../../dbs/pg-db';
import { findBoardBySlugQuery } from '../../../../dbs/queries';
import { generateToken } from '../../../../services/auth-services';
import { raiseJob } from '../../../../services/job-services';

jest.mock('../../../../dbs/pg-db', () => ({
  query: jest.fn(),
}));

jest.mock('../../../../services/job-services', () => ({
  raiseJob: jest.fn(),
}));

const putBoard = async (url, token, newData) => {
  const res = await fetch(url, {
    method: 'put',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(newData),
  });
  const response = await res.json();
  return response;
};

describe('/api/board/[slug]', () => {
  let server;

  beforeEach(async () => {
    jest.resetAllMocks();
  });

  afterEach(async () => {
    await server.close();
  });

  afterAll(async () => {
    jest.clearAllMocks();
  });

  describe('GET', () => {
    it('returns a 404 error with appropriate information for a wrong slug', async () => {
      expect.assertions(3);
      const invalidSlug = `some-random-invalid-slug-${Math.floor(2000 * Math.random())}`;
      const [serverUrl, theServer] = await setUpServer(handleReqs, { slug: invalidSlug });
      server = theServer;
      query.mockResolvedValue({ rows: [] });

      const res = await fetch(serverUrl);
      const response = await res.json();

      expect(query).toHaveBeenNthCalledWith(1, findBoardBySlugQuery, [invalidSlug, null]);
      expect(res.status).toBe(404);
      expect(response.error.code).toBe(2051);
    });

    it('returns the board in correct exposed format with status 200 for correct slug', async () => {
      expect.assertions(2);
      const validSlug = 'Why-are-you-so-dumb';
      const [serverUrl, theServer] = await setUpServer(handleReqs, { slug: validSlug });
      server = theServer;
      const boardData = {
        id: 'c568eba5-e806-4d59-afde-583ae219d48e',
        slug: 'Why-are-you-so-dumb',
        topic: 'Why are you so dumb?',
        created_at: '2020-03-11T13:21:32.743Z',
        updated_at: '2020-03-11T13:21:32.743Z',
        answers: [
          {
            id: '667086e7-7fa0-407e-b587-428ab0997212',
            answer: 'new answer',
            created_at: '2020-03-17T15:26:58.230169+03:30',
            updated_at: '2020-03-17T15:26:58.230169+03:30',
          },
          {
            id: '53dc4074-4bb6-458b-b1b1-549e7c774372',
            answer: 'another answer',
            created_at: '2020-03-17T16:17:55.328538+03:30',
            updated_at: '2020-03-17T16:17:55.328538+03:30',
          },
        ],
      };
      query.mockResolvedValue({
        rows: [boardData],
      });
      const expectedResponse = { ...boardData };
      const res = await fetch(serverUrl);
      const response = await res.json();
      expect(res.status).toBe(200);
      expect(response).toEqual(expectedResponse);
    });
  });

  describe('PUT', () => {
    const intUser = { id: '1', email: 'sample@email.com', ex_id: '5678' };
    const slug = `some-valid-slug-${Math.floor(1000 * Math.random())}`;
    let token;
    let user;
    let url;

    beforeEach(async () => {
      const { token: validToken, user: validUser } = await generateToken(intUser);
      token = validToken;
      user = validUser;
      const [serverUrl, theServer] = await setUpServer(handleReqs, { slug });
      server = theServer;
      url = serverUrl;
    });

    it('raises the UPDATE-BOARD job and passes the received payload to it', async () => {
      expect.assertions(2);
      const jobResult = { someData: `some-data-${Math.floor(1000 * Math.random())}` };
      const newTopic = `some very interesting new topic ${Math.floor(10000 * Math.random())}`;
      const newData = { topic: newTopic };
      raiseJob.mockResolvedValue([jobResult, '/']);
      const response = await putBoard(url, token, newData);
      expect(raiseJob).toHaveBeenNthCalledWith(
        1,
        { job: 'update-board', payload: { ...newData, slug } },
        { user }
      );
      expect(response).toEqual(jobResult);
    });
  });
});
