import fetch from 'cross-fetch';
import handleReqs from '../../../../pages/api/panels';
import { setUpServer } from '../../../../utils/test-utils';
import { query } from '../../../../dbs/pg-db';
import samplePanels from '../../../../data/sample-panels.json';
import { getFeaturedPanelsQuery } from '../../../../dbs/queries';
import { generateToken } from '../../../../services/auth-services';
import { raiseJob } from '../../../../services/job-services';

jest.mock('../../../../dbs/pg-db', () => ({
  query: jest.fn(),
}));

jest.mock('../../../../services/job-services', () => ({
  raiseJob: jest.fn(),
}));

const intUser = { id: '1', email: 'sample@email.com', ex_id: '5678' };

const postPanel = async (url, token, panelData) => {
  const res = await fetch(url, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(panelData),
  });
  const response = await res.json();
  return response;
};

describe('/api/panels', () => {
  let server;
  let url;
  let token;
  let user;

  beforeAll(async () => {
    const [serverUrl, theServer] = await setUpServer(handleReqs);
    const { token: validToken, user: theUser } = await generateToken(intUser);
    token = validToken;
    url = serverUrl;
    server = theServer;
    user = theUser;
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  afterAll(async () => {
    await server.close();
  });

  describe('POST', () => {
    it('raises the ADD-PANEL job and passes the received payload to it, then returns the job result', async () => {
      expect.assertions(2);
      const title = `sample panel ${Math.floor(1000 * Math.random())}`;
      const panelData = { title };

      const jobResult = samplePanels[0];

      raiseJob.mockResolvedValue([jobResult, '/']);
      const response = await postPanel(url, token, panelData);
      expect(raiseJob).toHaveBeenNthCalledWith(
        1,
        { job: 'add-panel', payload: panelData },
        { user }
      );
      expect(response).toEqual(jobResult);
    });
  });

  describe('GET', () => {
    it('sends the required database query to get featured panels then returns the panels', async () => {
      expect.assertions(2);

      query.mockResolvedValue({ rows: samplePanels });

      const res = await fetch(url, {
        method: 'get',
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
      const response = await res.json();
      expect(query).toHaveBeenNthCalledWith(1, getFeaturedPanelsQuery, [intUser.ex_id]);
      expect(response).toEqual(samplePanels);
    });
  });
});
