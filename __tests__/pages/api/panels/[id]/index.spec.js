import fetch from 'cross-fetch';
import { raiseJob } from '../../../../../services/job-services';
import { generateToken } from '../../../../../services/auth-services';
import { setUpServer } from '../../../../../utils/test-utils';
import handleReqs from '../../../../../pages/api/panels/[id]';

jest.mock('../../../../../services/job-services');

const putPanel = async (url, token, newData) => {
  const res = await fetch(url, {
    method: 'put',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(newData),
  });
  const response = await res.json();
  return response;
};

describe('/api/panels/[id]', () => {
  let server;

  beforeEach(async () => {
    jest.resetAllMocks();
  });

  afterAll(async () => {
    jest.clearAllMocks();
  });

  describe('PUT', () => {
    const intUser = { id: '1', email: 'sample@email.com', ex_id: '5678' };
    const panelId = 'avuasd.qweqwe123.123cz';
    let token;
    let user;
    let url;

    beforeEach(async () => {
      const { token: validToken, user: validUser } = await generateToken(intUser);
      token = validToken;
      user = validUser;
      const [serverUrl, theServer] = await setUpServer(handleReqs, { id: panelId });
      server = theServer;
      url = serverUrl;
    });

    afterEach(async () => {
      await server.close();
    });

    it('raises the UPDATE-PANEL job and passes the received payload to it', async () => {
      expect.assertions(2);
      const jobResult = { someData: `some-data-${Math.floor(1000 * Math.random())}` };
      const newTitle = `some very interesting new title ${Math.floor(10000 * Math.random())}`;
      const newData = { title: newTitle };
      raiseJob.mockResolvedValue([jobResult, '/']);
      const response = await putPanel(url, token, newData);
      expect(raiseJob).toHaveBeenNthCalledWith(
        1,
        {
          job: 'update-panel',
          payload: { ...newData, id: panelId },
        },
        { user }
      );
      expect(response).toEqual(jobResult);
    });
  });
});
