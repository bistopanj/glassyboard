import fetch from 'cross-fetch';
import handleReqs from '../../../../../../pages/api/panels/[id]/boards';
import { raiseJob } from '../../../../../../services/job-services';
import { setUpServer } from '../../../../../../utils/test-utils';
import { generateToken } from '../../../../../../services/auth-services';

jest.mock('../../../../../../services/job-services', () => ({
  raiseJob: jest.fn(),
}));

const intUser = { id: '1', email: 'sample@email.com', ex_id: '5678' };

const addBoardToPanel = async (url, token, reqBody, panelId) => {
  const res = await fetch(`${url}?id=${panelId}`, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(reqBody),
  });
  return res.json();
};

const removeBoardFromPanel = async (url, token, reqBody, panelId) => {
  const res = await fetch(`${url}?id=${panelId}`, {
    method: 'delete',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(reqBody),
  });
  return res.json();
};

describe('/api/panels/[id]/boards', () => {
  const boardSlug = `some-slug-${Math.floor(10000 * Math.random())}`;
  const panelId = `someId-${Math.floor(1000 * Math.random())}`;
  const reqBody = { boardSlug: boardSlug };
  let server;
  let url;
  let token;
  let user;

  beforeAll(async () => {
    const [serverUrl, theServer] = await setUpServer(handleReqs);
    const { token: validToken, user: theUser } = await generateToken(intUser);
    token = validToken;
    url = serverUrl;
    server = theServer;
    user = theUser;
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  afterAll(async () => {
    await server.close();
  });

  describe('DELETE', () => {
    it('raises the REMOVE-BOARD-FROM-PANEL job and passes the payload to it, then returns the job result', async () => {
      expect.assertions(2);
      const jobResult = {
        board_id: '1234-5678',
        panel_id: 'abcd-1234',
        active: false,
        created_by: user.id,
        created_at: 'some time',
        updated_at: 'another time',
      };
      raiseJob.mockResolvedValue([jobResult, '/']);
      const response = await removeBoardFromPanel(url, token, reqBody, panelId);
      expect(raiseJob).toHaveBeenNthCalledWith(
        1,
        { job: 'remove-board-from-panel', payload: { ...reqBody, panelId } },
        { user }
      );
      expect(response).toEqual(jobResult);
    });
  });

  describe('POST', () => {
    it('raises the ADD-BOARD-TO-PANEL job and passes the payload to it, then returns the job result', async () => {
      expect.assertions(2);
      const jobResult = {
        board_id: '1234-5678',
        panel_id: 'abcd-1234',
        active: true,
        created_by: user.id,
        created_at: 'some time',
        updated_at: 'another time',
      };
      raiseJob.mockResolvedValue([jobResult, '/']);
      const response = await addBoardToPanel(url, token, reqBody, panelId);

      expect(raiseJob).toHaveBeenNthCalledWith(
        1,
        { job: 'add-board-to-panel', payload: { ...reqBody, panelId } },
        { user }
      );
      expect(response).toEqual(jobResult);
    });
  });
});
