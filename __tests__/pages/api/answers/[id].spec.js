import fetch from 'cross-fetch';
import { raiseJob } from '../../../../services/job-services';
import { generateToken } from '../../../../services/auth-services';
import { setUpServer } from '../../../../utils/test-utils';
import handleReqs from '../../../../pages/api/answers/[id]';

jest.mock('../../../../services/job-services');

const delAnswer = async (url, token) => {
  const res = await fetch(url, {
    method: 'delete',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${token}`,
    },
  });
  const response = await res.json();
  return response;
};

describe('/api/answers/[id]', () => {
  let server;

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('DELETE', () => {
    const intUser = { id: '1', email: 'sample@email.com', ex_id: '5678' };
    const answerId = 'adb.def.123.ghi';
    let token;
    let user;
    let url;

    beforeEach(async () => {
      const { token: validToken, user: validUser } = await generateToken(intUser);
      user = validUser;
      token = validToken;
      const [serverUrl, theServer] = await setUpServer(handleReqs, { id: answerId });
      server = theServer;
      url = serverUrl;
    });

    afterEach(async () => {
      await server.close();
    });

    it('raises the DEACTIVATE-ANSWER job and passes the received payload to it', async () => {
      expect.assertions(2);
      const jobResult = { someData: `some data ${Math.floor(10000 * Math.random())}` };
      raiseJob.mockResolvedValue([jobResult, '/']);
      const response = await delAnswer(url, token);
      expect(raiseJob).toHaveBeenNthCalledWith(
        1,
        { job: 'deactivate-answer', payload: { id: answerId } },
        { user }
      );
      expect(response).toEqual(jobResult);
    });
  });
});
