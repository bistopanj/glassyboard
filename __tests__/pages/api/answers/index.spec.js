import fetch from 'cross-fetch';
import { raiseJob } from '../../../../services/job-services';
import { generateToken } from '../../../../services/auth-services';
import { setUpServer } from '../../../../utils/test-utils';
import handleReqs from '../../../../pages/api/answers';

jest.mock('../../../../services/job-services', () => ({
  raiseJob: jest.fn(),
}));

const intUser = { id: '1', email: 'sample@email.com', ex_id: '5678' };

const postAnswer = async (url, token, answer) => {
  const res = await fetch(url, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(answer),
  });
  // const response = await res.json();
  return res.json();
};

describe('/api/answers', () => {
  let server;

  afterEach(async () => {
    await server.close();
  });

  describe('POST', () => {
    it('raises the ADD-ANSWER job and passes the received payload to it', async () => {
      expect.assertions(2);
      const { token: validToken, user } = await generateToken(intUser);
      const [serverUrl, theServer] = await setUpServer(handleReqs);
      server = theServer;
      const jobResult = { someData: `some-data-${Math.floor(1000 * Math.random())}` };

      raiseJob.mockResolvedValue([jobResult, '/']);
      const answerData = {
        answer: `some-answer-${Math.floor(1000 * Math.random())}`,
        boardId: '1234-1234',
      };
      const response = await postAnswer(serverUrl, validToken, answerData);

      expect(raiseJob).toHaveBeenNthCalledWith(
        1,
        { job: 'add-answer', payload: answerData },
        { user }
      );
      expect(response).toEqual(jobResult);
    });
  });
});
