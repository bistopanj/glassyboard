import configureMockStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import IndexPage from '../../pages/index';
import { checkProps, tick } from '../../utils/test-utils';
import { initialState as defaultState } from '../../reducers';
import { middlewares } from '../../build-store';
import getCompanyBoard from '../../actions/get-company-board';

jest.mock('../../actions/get-company-board');

const mockStore = configureMockStore(middlewares);

const primaryBoardData = {
  id: 'e4e24eb9-583d-47c9-93b3-f2e64e231d3f',
  editable: false,
  slug: 'some-slug',
  topic: 'the topic',
  created_at: '2020-03-16T05:04:07.111Z',
  updated_at: '2020-03-16T05:04:07.111Z',
  answers: [
    {
      id: '667086e7-7fa0-407e-b587-428ab0997212',
      editable: false,
      answer: 'first answer',
      created_at: '2020-03-17T15:26:58.230169+03:30',
      updated_at: '2020-03-17T15:26:58.230169+03:30',
    },
    {
      id: '53dc4074-4bb6-458b-b1b1-549e7c774372',
      answer: 'another answer',
      editable: true,
      created_at: '2020-03-17T16:17:55.328538+03:30',
      updated_at: '2020-03-17T16:17:55.328538+03:30',
    },
  ],
};

const defaultPathInfo = {
  subdomain: '',
  host: 'glassyboard.com',
  protocol: 'https',
};

const defaultProps = { pathInfo: defaultPathInfo };

const setUp = ({ initialState = defaultState, props = defaultProps }) => {
  const store = mockStore(initialState);
  const wrapper = shallow(<IndexPage {...props} store={store} />)
    .childAt(0)
    .dive();
  return [wrapper, store];
};

describe('/ (home page)', () => {
  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        pathInfo: {
          subdomain: 'company',
          host: 'www.glassyboard.com',
          protocol: 'https',
        },
        board: {},
      };
      const propsErrors = checkProps(IndexPage.WrappedComponent, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('no subdomain', () => {
    it('renders one <AddCompanyForm /> and no <CompanyBoard />', () => {
      const props = { ...defaultProps, pathInfo: { ...defaultProps.pathInfo, subdomain: '' } };
      const [wrapper] = setUp({ props });
      const addCompanyForm = wrapper.find('AddCompanyForm');
      const companyBoard = wrapper.find('CompanyBoard');
      expect(addCompanyForm).toHaveLength(1);
      expect(companyBoard).toHaveLength(0);
    });
  });

  describe('with subdomain', () => {
    it('renders one <CompanyBoard /> and no <AddCompanyForm />', () => {
      const props = {
        ...defaultProps,
        pathInfo: { ...defaultProps.pathInfo, subdomain: 'company' },
      };
      const [wrapper] = setUp({ props });
      const addCompanyForm = wrapper.find('AddCompanyForm');
      const companyBoard = wrapper.find('CompanyBoard');
      expect(companyBoard).toHaveLength(1);
      expect(addCompanyForm).toHaveLength(0);
    });
  });

  describe('behavior', () => {
    it('passes down the primary board to its AddCompanyForm child', () => {
      const { answers, ...rest } = primaryBoardData;
      const answerIds = answers.map(a => a.id);
      const answerById = answers.reduce((acc, a) => ({ ...acc, [a.id]: a }), {});
      const state = {
        ...defaultState,
        boards: {
          ...defaultState.boards,
          primary: primaryBoardData.slug,
          bySlug: {
            ...defaultState.boards.bySlug,
            [primaryBoardData.slug]: { data: rest, answers: answerIds, answerById },
          },
        },
      };
      const [wrapper] = setUp({ initialState: state });
      const addCompanyForm = wrapper.find('AddCompanyForm');
      expect(addCompanyForm.prop('board')).toEqual({ answers, data: rest });
    });

    it('passes down the primary board to its CompanyBoard child', () => {
      const { answers, ...rest } = primaryBoardData;
      const answerIds = answers.map(a => a.id);
      const answerById = answers.reduce((acc, a) => ({ ...acc, [a.id]: a }), {});
      const state = {
        ...defaultState,
        boards: {
          ...defaultState.boards,
          primary: primaryBoardData.slug,
          bySlug: {
            ...defaultState.boards.bySlug,
            [primaryBoardData.slug]: { data: rest, answers: answerIds, answerById },
          },
        },
      };
      const [wrapper] = setUp({
        initialState: state,
        props: { pathInfo: { ...defaultPathInfo, subdomain: 'company' } },
      });
      const companyBoard = wrapper.find('CompanyBoard');
      expect(companyBoard.prop('board')).toEqual({ answers, data: rest });
    });

    it('dispatches the necessary action to get company primary board at initiation', async () => {
      expect.assertions(2);
      const subdomain = `some-sub-domain-${Math.floor(10000 * Math.random())}`;
      const cookie = `someCookie${Math.floor(1000 * Math.random())}`;
      const action1 = { type: 'SOME_ACTION', payload: { someData: 'some-value' } };
      const action2 = { type: 'ANOTHER_ACTION', payload: { someData: 'another-value' } };
      const action3 = { type: 'YET_ANOTHER_ACTION', payload: { someData: 'yet-another-value' } };
      getCompanyBoard.mockReturnValue(async dispatch => {
        dispatch(action1);
        await tick();
        dispatch(action2);
        await tick();
        dispatch(action3);
      });
      const expectedActions = [action1, action2, action3];
      const store = mockStore(defaultState);
      await IndexPage.getInitialProps({
        store,
        isServer: true,
        req: { headers: { cookie } },
        pathInfo: { ...defaultPathInfo, subdomain },
      });
      const dispatchedActions = store.getActions();
      expect(getCompanyBoard).toHaveBeenNthCalledWith(1, subdomain, {
        cookie,
        pathInfo: { ...defaultPathInfo, subdomain },
      });
      expect(dispatchedActions).toEqual(expectedActions);
    });
  });
});
