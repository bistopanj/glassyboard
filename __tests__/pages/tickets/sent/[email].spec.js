import { shallow } from 'enzyme';
import { useRouter } from 'next/router';
import TicketSent from '../../../../pages/tickets/sent/[email]';
import { findByTestAttr } from '../../../../utils/test-utils';

jest.mock('next/router');

describe('/tickets/sent', () => {
  const sampleEmail = 'sample@mail.com';
  let wrapper;

  beforeEach(() => {
    jest.resetAllMocks();
    useRouter.mockReturnValue({ query: { email: sampleEmail } });
    wrapper = shallow(<TicketSent />);
  });

  it('renders without error', () => {
    const container = findByTestAttr(wrapper, 'ticket-sent-confirmation');
    expect(container).toHaveLength(1);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('displays the email receiving the ticket in the page', () => {
    const userEmail = findByTestAttr(wrapper, 'user-email');
    expect(userEmail.text()).toContain(sampleEmail);
  });
});
