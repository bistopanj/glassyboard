import { mount, shallow } from 'enzyme';
import cookies from 'next-cookies';
import MyAppConnected, { MyApp } from '../../pages/_app';
import { findByTestAttr } from '../../utils/test-utils';
import updateConnectionInfo from '../../actions/update-connection-info';
import { getFullPathInfo } from '../../utils/next-utils';

jest.mock('next-cookies');
jest.mock('../../actions/update-connection-info');
jest.mock('../../utils/next-utils');

describe('_app root component', () => {
  const SampleComponent = () => <div data-testid="sample-component">Sample Text</div>;
  let wrapper;

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('common components it renders', () => {
    it('renders any child component without error', async () => {
      wrapper = mount(<MyAppConnected Component={SampleComponent} />);
      const sampleComp = findByTestAttr(wrapper, 'sample-component');
      expect(sampleComp).toHaveLength(1);
    });

    it('renders a <LoginModal /> to be shared across all pages', () => {
      wrapper = shallow(<MyAppConnected Component={SampleComponent} />).dive();
      const loginModalComp = findByTestAttr(wrapper, 'login-modal-comp');
      expect(loginModalComp).toHaveLength(1);
    });

    it('renders a <TopNavbar/> to be shared across all pages', () => {
      wrapper = shallow(<MyAppConnected Component={SampleComponent} />).dive();
      const Layout = wrapper.find('Layout');
      expect(Layout.contains(<SampleComponent />)).toBeTruthy();
    });
  });

  describe('behavior', () => {
    it('dispatches the appropriate action to update the store with connection info', async () => {
      expect.assertions(3);
      const token = `some-token-${Math.floor(10000 * Math.random())}`;
      const cookiesObj = { token };
      cookies.mockImplementation(() => cookiesObj);
      const host = 'www.some-host.com';
      const protocol = 'http';
      const subdomain = `somesubdomain${Math.floor(10000 * Math.random())}`;
      const action = { type: 'SOME_ACTION', payload: { someData: 'value' } };
      const store = { dispatch: jest.fn() };
      getFullPathInfo.mockReturnValue({ host, protocol, subdomain });
      updateConnectionInfo.mockReturnValue(action);
      const output = await MyApp.getInitialProps({
        Component: SampleComponent,
        ctx: { store },
      });
      expect(updateConnectionInfo).toHaveBeenNthCalledWith(1, token, { host, subdomain, protocol });
      expect(store.dispatch).toHaveBeenNthCalledWith(1, action);
      expect(output.pageProps).toEqual(
        expect.objectContaining({ pathInfo: { host, protocol, subdomain } })
      );
    });
  });
});
