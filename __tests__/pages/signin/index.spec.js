import { shallow } from 'enzyme';
import { useRouter } from 'next/router';
import SigninPage from '../../../pages/signin';
import { findByTestAttr } from '../../../utils/test-utils';

jest.mock('next/router');

const defaultRouter = { query: {}, back: () => ({}) };

const setUp = ({ router = defaultRouter } = {}) => {
  useRouter.mockReturnValue(router);
  const wrapper = shallow(<SigninPage />);
  return wrapper;
};
describe('/signin', () => {
  let wrapper;
  describe('appearance', () => {
    beforeEach(() => {
      wrapper = setUp();
    });

    it('renders without error', () => {
      const container = findByTestAttr(wrapper, 'login-page-container');
      expect(container).toHaveLength(1);
    });

    it('renders a <SigninDialog />', () => {
      const signinDialog = wrapper.find('SigninDialog');
      expect(signinDialog).toHaveLength(1);
    });

    it('renders a link to go back', () => {
      const goBackButton = findByTestAttr(wrapper, 'go-back-button');
      expect(goBackButton).toHaveLength(1);
    });

    it('renders with correct style', () => {
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('behavior', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('fetches the login data from query string and passes it down to the SignInDialog', () => {
      const signinData = { ticketAction: 'SOME-ACTION', payload: { someData: 'some-value' } };
      const dataStr = JSON.stringify(signinData);
      const router = { query: { data: dataStr } };
      wrapper = setUp({ router });
      const signinDialog = wrapper.find('SigninDialog');
      expect(signinDialog.prop('signinData')).toEqual(signinData);
    });

    it('calls the method to go back when the go back button is clicked', () => {
      const router = { query: {}, back: jest.fn() };
      wrapper = setUp({ router });
      const goBackButton = findByTestAttr(wrapper, 'go-back-button');
      goBackButton.simulate('click');
      expect(router.back).toHaveBeenCalledTimes(1);
    });
  });
});
