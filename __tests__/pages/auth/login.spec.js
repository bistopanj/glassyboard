import { mount, shallow } from 'enzyme';
import { useRouter } from 'next/router';
import Cookies from 'js-cookie';
import LoginPage from '../../../pages/auth/login';
import { findByTestAttr } from '../../../utils/test-utils';

jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}));

jest.mock('js-cookie');
const cookieDomain = process.env.COOKIE_DOMAIN;

describe('/auth/login', () => {
  const token = `some.token.${Math.floor(1000 * Math.random())}`;
  const redirectTo = `/some/path/${Math.floor(10000 * Math.random())}`;
  let routerPush;
  const { location } = window;

  beforeAll(() => {
    delete window.location;
    window.location = { replace: jest.fn() };
  });

  beforeEach(() => {
    jest.resetAllMocks();
    routerPush = jest.fn();
    useRouter.mockReturnValue({ query: { token, redirectTo }, push: routerPush });
  });

  afterAll(() => {
    window.location = location;
  });

  it('saves the given token in cookies', () => {
    mount(<LoginPage />);
    expect(Cookies.set).toHaveBeenNthCalledWith(1, 'token', token, {
      path: '/',
      domain: cookieDomain,
    });
  });

  it('renders without error', () => {
    const wrapper = shallow(<LoginPage />);
    const container = findByTestAttr(wrapper, 'login-initial-message');
    expect(container).toHaveLength(1);
  });

  it('renders with correct style', () => {
    const wrapper = shallow(<LoginPage />);
    expect(wrapper).toMatchSnapshot();
  });

  it('redirects to the address specified in query.redirectTo', () => {
    mount(<LoginPage />);
    expect(window.location.replace).toHaveBeenNthCalledWith(1, redirectTo);
  });
});
