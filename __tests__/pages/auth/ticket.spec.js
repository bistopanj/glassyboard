import configureMockStore from 'redux-mock-store';
import Router from 'next/router';
import { mount, shallow } from 'enzyme';
import { middlewares } from '../../../build-store';
import { initialState as defaultState } from '../../../reducers';
import { actionTypes } from '../../../actions';
import AuthWithTicketPage from '../../../pages/auth/ticket';
import { buildMockResponse, findByTestAttr, checkProps } from '../../../utils/test-utils';

const mockStore = configureMockStore(middlewares);
const baseUrl = process.env.BASE_URL;

jest.mock('next/router');

const defaultProps = {
  ticketId: 'some.ticket.id',
};

const setUp = ({ initialState = defaultState, props = defaultProps, doMount = false } = {}) => {
  const store = mockStore(initialState);
  let wrapper;
  if (doMount) wrapper = mount(<AuthWithTicketPage store={store} {...props} />);
  else
    wrapper = shallow(<AuthWithTicketPage store={store} {...props} />)
      .childAt(0)
      .dive();
  return [wrapper, store];
};

describe('/auth/ticket', () => {
  const token = `some.token.${Math.floor(10000 * Math.random())}`;
  const redirectPath = `/some/path/${Math.floor(1000 * Math.random())}`;
  const ticketId = `abSmms123Hxksdlwpw${Math.floor(5000 * Math.random)}`;
  const query = { ticketId };

  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        ticketId,
        error: {},
        loading: false,
        success: true,
        authResult: {
          token: 'some.token.from.glasyboard',
          redirectTo: '/some/path',
          user: {
            id: 'user-id',
            first_name: 'first name of user',
            last_name: 'last name of user',
            created_at: 'some date',
            email: 'some@email.com',
          },
        },
      };
      const propsErrors = checkProps(AuthWithTicketPage.WrappedComponent, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appearance', () => {
    it('displays the initialText at start', () => {
      const [wrapper] = setUp();
      const container = findByTestAttr(wrapper, 'auth-ticket-initial-message');
      expect(container).toHaveLength(1);
    });

    it('shows the loading message when waiting for server response', () => {
      const state = {
        ...defaultState,
        login: { ticket: { loading: true, error: null, success: false } },
      };
      const [wrapper] = setUp({ initialState: state });
      const loadingMessageContainer = findByTestAttr(wrapper, 'auth-ticket-loading-message');
      expect(loadingMessageContainer).toHaveLength(1);
    });

    it('shows the error dialog when the authentication fails', () => {
      const state = {
        ...defaultState,
        login: {
          ticket: {
            loading: false,
            error: {
              code: 1002,
              message: 'authentication service unavailable',
              moreInfo: 'some info',
              type: 'ApiError',
              status: 404,
            },
            success: false,
            authResult: null,
          },
        },
      };
      const [wrapper] = setUp({ initialState: state });
      const errorMessageContainer = findByTestAttr(wrapper, 'auth-ticket-error-card');
      expect(errorMessageContainer).toHaveLength(1);
    });
  });

  describe('behavior', () => {
    Router.push = jest.fn();

    beforeEach(() => {
      fetch.resetMocks();
      jest.resetAllMocks();
      fetch.doMockIf(
        `${baseUrl}/api/auth/ticket`,
        buildMockResponse({ token, redirectTo: redirectPath })
      );
    });

    it('dispatches the appropriate actions to authenticate with server on page initiation', async () => {
      expect.assertions(1);
      const store = mockStore(defaultState);
      const expectedActions = [
        { type: actionTypes.authWithTicketLoading },
        { type: actionTypes.authWithTicketSuccess, payload: { token, redirectTo: redirectPath } },
      ];
      await AuthWithTicketPage.getInitialProps({ store, query });
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });

    it('redirects to the page specified in authResult when the process is successful', () => {
      const state = {
        ...defaultState,
        login: {
          ticket: {
            loading: false,
            error: null,
            success: true,
            authResult: { token, redirectTo: redirectPath },
          },
        },
      };
      const expectedRedirectUrl = `/auth/login?token=${token}&redirectTo=${encodeURIComponent(
        redirectPath
      )}`;
      setUp({ initialState: state, doMount: true });
      expect(Router.push).toHaveBeenNthCalledWith(1, expectedRedirectUrl, '/auth/login');
    });
  });
});
