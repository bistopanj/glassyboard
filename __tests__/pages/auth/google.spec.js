import configureMockStore from 'redux-mock-store';
import Router from 'next/router';
import { shallow, mount } from 'enzyme';
import { middlewares } from '../../../build-store';
import { initialState as defaultState } from '../../../reducers';
import { buildMockResponse, findByTestAttr, checkProps } from '../../../utils/test-utils';
import AuthWithGooglePage from '../../../pages/auth/google';
import { actionTypes } from '../../../actions';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);

jest.mock('next/router');

const defaultProps = {
  // data: undefined,
  // token: '123',
};

const setUp = ({ initialState = defaultState, props = defaultProps, doMount = false } = {}) => {
  const store = mockStore(initialState);
  let wrapper;
  if (doMount) wrapper = mount(<AuthWithGooglePage store={store} {...props} />);
  else
    wrapper = shallow(<AuthWithGooglePage store={store} {...props} />)
      .childAt(0)
      .dive();
  return [wrapper, store];
};

describe('/auth/google', () => {
  const token = `some.token.${Math.floor(1000 * Math.random())}`;
  const googleToken = `some.other_token.${Math.floor(1000 * Math.random())}`;
  const data = { ticketAction: 'SOME-ACTION', payload: { someData: 'data' } };
  const redirectPath = `/some/path/${Math.floor(1000 * Math.random())}`;
  const query = { token: googleToken, data: JSON.stringify(data) };

  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        // token: 'some.google.token',
        // data: { ticketAction: 'SOME-ACTION', payload: { someData: 'its value' } },
        loading: false,
        success: false,
        authResult: {
          token: 'some.token.from.glasyboard',
          redirectTo: '/some/path',
          user: {
            id: 'user-id',
            first_name: 'first name of user',
            last_name: 'last name of user',
            created_at: 'some date',
            email: 'some@email.com',
          },
        },
      };
      const propsErrors = checkProps(AuthWithGooglePage.WrappedComponent, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appearance', () => {
    it('displays the initialText at start', () => {
      const [wrapper] = setUp();
      const container = findByTestAttr(wrapper, 'auth-google-initial-message');
      expect(container).toHaveLength(1);
    });

    it('shows the loading messsage when waiting for server response', () => {
      const state = {
        ...defaultState,
        login: { google: { loading: true, error: null, success: false, authResult: null } },
      };
      const [wrapper] = setUp({ initialState: state });
      const loadingMessageContainer = findByTestAttr(wrapper, 'auth-google-loading-message');
      expect(loadingMessageContainer).toHaveLength(1);
    });

    it('shows the error dialog when the authentication fails', () => {
      const state = {
        ...defaultState,
        login: {
          google: {
            loading: false,
            error: {
              code: 1002,
              message: 'authentication service unavailable',
              moreInfo: 'some info',
              type: 'ApiError',
              status: 404,
            },
            success: false,
            authResult: null,
          },
        },
      };
      const [wrapper] = setUp({ initialState: state });
      const errorMessageContainer = findByTestAttr(wrapper, 'auth-google-error-card');
      expect(errorMessageContainer).toHaveLength(1);
    });
  });

  describe('behavior', () => {
    Router.push = jest.fn();

    beforeEach(() => {
      fetch.resetMocks();
      jest.resetAllMocks();
      fetch.doMockIf(
        `${baseUrl}/api/auth/google`,
        buildMockResponse({ token, redirectTo: redirectPath })
      );
    });

    it('dispatches the appropriate actions to authenticate with google on page initiation', async () => {
      expect.assertions(1);
      const store = mockStore(defaultState);
      const expectedActions = [
        { type: actionTypes.authWithGoogleLoading },
        { type: actionTypes.authWithGoogleSuccess, payload: { token, redirectTo: redirectPath } },
      ];
      await AuthWithGooglePage.getInitialProps({ store, query });
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });

    it('redirects to the login page when the authentication is successful', () => {
      const state = {
        ...defaultState,
        login: {
          google: {
            loading: false,
            error: null,
            success: true,
            authResult: { token, redirectTo: redirectPath },
          },
        },
      };
      const expectedRedirectUrl = `/auth/login?token=${token}&redirectTo=${encodeURIComponent(
        redirectPath
      )}`;
      setUp({ initialState: state, doMount: true });
      expect(Router.push).toHaveBeenNthCalledWith(1, expectedRedirectUrl, '/auth/login');
    });
  });
});
