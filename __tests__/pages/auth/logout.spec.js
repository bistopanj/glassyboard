import Cookies from 'js-cookie';
import { mount, shallow } from 'enzyme';
import Router from 'next/router';
import LogoutPage from '../../../pages/auth/logout';
import { findByTestAttr, tick } from '../../../utils/test-utils';

const cookieDomain = process.env.COOKIE_DOMAIN;

jest.mock('js-cookie');
jest.mock('next/router');

describe('/auth/logout', () => {
  const { gapi } = window;
  const disconnectGoogle = jest.fn();
  Router.push = jest.fn();

  beforeAll(() => {
    delete window.gapi;
    window.gapi = {
      load: (val, func) => {
        func();
      },
      auth2: {
        init: () => new Promise((resolve, reject) => resolve({ disconnect: disconnectGoogle })),
      },
    };
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  afterAll(() => {
    window.gapi = gapi;
    jest.resetAllMocks();
  });

  it('removes the token cookie', async () => {
    expect.assertions(1);
    mount(<LogoutPage />);
    await tick();
    expect(Cookies.remove).toHaveBeenNthCalledWith(1, 'token', { path: '/', domain: cookieDomain });
  });

  it('signs out of google', async () => {
    expect.assertions(1);
    mount(<LogoutPage />);
    await tick();
    expect(disconnectGoogle).toHaveBeenCalledTimes(1);
  });

  it('redirects to the home page', async () => {
    expect.assertions(1);
    mount(<LogoutPage />);
    await tick();
    expect(Router.push).toHaveBeenNthCalledWith(1, '/');
  });

  it('renders without error', () => {
    const wrapper = shallow(<LogoutPage />);
    const container = findByTestAttr(wrapper, 'logout-initial-message');
    expect(container).toHaveLength(1);
  });

  it('renders with correct style', () => {
    const wrapper = shallow(<LogoutPage />);
    expect(wrapper).toMatchSnapshot();
  });
});
