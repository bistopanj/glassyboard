import addPanel from '../../jobs/add-panel';
import { upsertUser } from '../../services/user-services';
import { closeDbConnections } from '../../dbs/pg-db';
import { findPanelById, deletePanelById } from '../../services/panel-services';

describe('jobs.addPanel', () => {
  const sampleEmail = 'sample@email.com';
  let userInfo;
  let payload;
  let sampleTitle;

  beforeAll(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    userInfo = { user: { id: intUser.ex_id } };
  });

  afterAll(async () => {
    await closeDbConnections();
  });

  describe('correct input', () => {
    let id1;
    beforeEach(() => {
      id1 = null;
      sampleTitle = `some title ${Math.floor(1000 * Math.random())}`;
      payload = { title: sampleTitle };
    });

    afterEach(async () => {
      if (id1) await deletePanelById(id1);
    });

    it('adds a new panel to the panels table', async () => {
      expect.assertions(1);
      const [result] = await addPanel(payload, userInfo);
      const { id } = result;
      id1 = id;
      const thePanel = await findPanelById(id);
      expect(thePanel.title).toBe(sampleTitle);
    });

    it('sets the featured value to false by default if it is not present in the payload', async () => {
      expect.assertions(1);
      const [result] = await addPanel(payload, userInfo);
      id1 = result.id;
      expect(result.featured).toBe(false);
    });

    it('sets the featured value to true if given so in the payload', async () => {
      expect.assertions(1);
      payload.featured = true;
      const [result] = await addPanel(payload, userInfo);
      id1 = result.id;
      expect(result.featured).toBe(true);
    });
  });
});
