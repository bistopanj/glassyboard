import { updatePanel } from '../../jobs';
import { findPanelById, createPanel, deletePanelById } from '../../services/panel-services';
import { upsertUser } from '../../services/user-services';
import { closeDbConnections } from '../../dbs/pg-db';

const ownerEmail = process.env.OWNER_EMAIL;

describe('jobs.updatePanel', () => {
  const sampleEmail = 'sample@email.com';
  const title = `some title ${Math.floor(1000 * Math.random())}`;
  let panel;
  let userInfo;
  let payload;
  let newTitle;

  beforeAll(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    panel = await createPanel({ title, createdBy: intUser.ex_id });
    userInfo = { user: { id: intUser.ex_id, email: intUser.email } };
  });

  afterAll(async () => {
    await deletePanelById(panel.id);
    await closeDbConnections();
  });

  beforeEach(() => {
    newTitle = `some new title ${Math.floor(1000 * Math.random())}`;
    payload = { title: newTitle, id: panel.id };
  });

  it('updates the panel', async () => {
    expect.assertions(1);
    await updatePanel(payload, userInfo);
    const theUpdatedPanel = await findPanelById(panel.id);
    expect(theUpdatedPanel).toEqual(expect.objectContaining(payload));
  });

  it('returns the updated panel in the first returned value', async () => {
    expect.assertions(1);
    const [result] = await updatePanel(payload, userInfo);
    expect(result).toEqual(expect.objectContaining(payload));
  });

  it('does not allow another (non-admin) user to update the panel created by a user', async () => {
    expect.assertions(2);
    const anotherEmail = 'someother@email.com';
    const intUser2 = await upsertUser({ email: anotherEmail });
    const user2Info = { user: { id: intUser2.ex_id, email: intUser2.email } };
    try {
      await updatePanel(payload, user2Info);
    } catch (error) {
      expect(error.status).toBe(400);
      expect(error.code).toBe(2062);
    }
  });

  it('allows the app owner to update any panel', async () => {
    expect.assertions(1);
    const ownerIntUser = await upsertUser({ email: ownerEmail });
    const ownerUserInfo = { user: { id: ownerIntUser.ex_id, email: ownerEmail } };
    const [result] = await updatePanel(payload, ownerUserInfo);
    expect(result).toEqual(expect.objectContaining(payload));
  });
});
