import { upsertUser } from '../../services/user-services';
import { closeDbConnections } from '../../dbs/pg-db';
import addCompany from '../../jobs/add-company';
import { findCompanyById, deleteCompanyById } from '../../services/company-services';
import { findBoardById, deleteBoardById } from '../../services/board-services';

describe('jobs.addCompany', () => {
  const sampleEmail = 'sample@email.com';
  let userInfo;
  let payload;
  let sampleSubdomain;
  let id1;
  let boardId;

  beforeEach(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    userInfo = { user: { id: intUser.ex_id } };
  });

  afterAll(async () => {
    await closeDbConnections();
  });

  describe('correct input', () => {
    beforeEach(() => {
      sampleSubdomain = `some-subdomain-${Math.floor(1000 * Math.random())}`;
      payload = { subdomain: sampleSubdomain };
    });

    afterEach(async () => {
      if (id1) await deleteCompanyById(id1);
      if (boardId) await deleteBoardById(boardId);
    });

    it('adds a new company to the companies table', async () => {
      expect.assertions(1);
      const [result] = await addCompany(payload, userInfo);
      const { id } = result;
      id1 = id;
      const company = await findCompanyById(id);
      expect(company).toBeDefined();
    });

    it('returns the subdomain url in the second returned value', async () => {
      expect.assertions(1);
      const [result, subdomainUrl] = await addCompany(payload, userInfo);
      const { id } = result;
      id1 = id;
      expect(subdomainUrl).toContain(sampleSubdomain);
    });

    it('adds a primary board to the created company', async () => {
      expect.assertions(2);
      const [result] = await addCompany(payload, userInfo);
      const { id, primary_board: reportedPrimaryBoardId } = result;
      id1 = id;
      boardId = reportedPrimaryBoardId;
      const company = await findCompanyById(id);
      const { primary_board: primaryBoardId } = company;
      expect(primaryBoardId).toEqual(expect.anything());
      const theBoard = await findBoardById(reportedPrimaryBoardId);
      expect(theBoard).toBeDefined();
    });
  });
});
