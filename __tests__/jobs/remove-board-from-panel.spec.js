import { upsertUser } from '../../services/user-services';
import { addBoard, addPanel, addBoardToPanel, removeBoardFromPanel } from '../../jobs';
import { closeDbConnections } from '../../dbs/pg-db';
import { deleteBoardById } from '../../services/board-services';
import {
  deletePanelById,
  deletePanelBoardRelById,
  findPanelBoardRelById,
} from '../../services/panel-services';

const ownerEmail = process.env.OWNER_EMAIL;

describe('jobs.removeBoardFromPanel', () => {
  const sampleEmail = 'sample@email.com';
  let userInfo;
  let ownerUserInfo;
  let theBoard;
  let thePanel;
  let payload;

  beforeAll(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    const intUserOwner = await upsertUser({ email: ownerEmail });
    userInfo = { user: { id: intUser.ex_id, email: sampleEmail } };
    ownerUserInfo = { user: { id: intUserOwner.ex_id, email: ownerEmail } };
    const sampleTopic = `some topic ${Math.floor(1000 * Math.random())}`;
    const sampleTitle = `some title ${Math.floor(1000 * Math.random())}`;
    const [newBoard] = await addBoard({ topic: sampleTopic }, userInfo);
    const [newPanel] = await addPanel({ title: sampleTitle }, userInfo);
    theBoard = newBoard;
    thePanel = newPanel;
  });

  afterAll(async () => {
    await deleteBoardById(theBoard.id);
    await deletePanelById(thePanel.id);
    await closeDbConnections();
  });

  describe('correct input', () => {
    let relId;
    let rel;
    beforeEach(async () => {
      payload = { boardSlug: theBoard.slug, panelId: thePanel.id };
      const [boardPanelRel] = await addBoardToPanel(payload, ownerUserInfo);
      rel = boardPanelRel;
      relId = boardPanelRel.id;
    });

    afterEach(async () => {
      if (relId) await deletePanelBoardRelById(relId);
    });

    it('deactivates the board-panel relationship', async () => {
      expect.assertions(2);
      expect(rel.active).toBe(true);
      await removeBoardFromPanel(payload, ownerUserInfo);
      const fromDB = await findPanelBoardRelById(relId);
      expect(fromDB.active).toBe(false);
    });

    it('is limited to the owner of the app', async () => {
      expect.assertions(2);
      try {
        await removeBoardFromPanel(payload, userInfo);
      } catch (error) {
        expect(error.status).toBe(403);
        expect(error.code).toBe(2006);
      }
    });
  });
});
