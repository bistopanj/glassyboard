import { upsertUser } from '../../services/user-services';
import { closeDbConnections } from '../../dbs/pg-db';
import { addBoard, addBoardToPanel, addPanel } from '../../jobs';
import { deleteBoardById } from '../../services/board-services';
import {
  deletePanelById,
  findPanelBoardRelById,
  deletePanelBoardRelById,
} from '../../services/panel-services';

const ownerEmail = process.env.OWNER_EMAIL;

describe('jobs.addBoardToPanel', () => {
  const sampleEmail = 'sample@email.com';
  let ownerUserInfo;
  let userInfo;
  let payload;
  let theBoard;
  let thePanel;

  beforeAll(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    const intUserOwner = await upsertUser({ email: ownerEmail });
    userInfo = { user: { id: intUser.ex_id, email: sampleEmail } };
    ownerUserInfo = { user: { id: intUserOwner.ex_id, email: ownerEmail } };
    const sampleTopic = `some topic ${Math.floor(1000 * Math.random())}`;
    const sampleTitle = `some title ${Math.floor(1000 * Math.random())}`;
    const [newBoard] = await addBoard({ topic: sampleTopic }, userInfo);
    theBoard = newBoard;
    const [newPanel] = await addPanel({ title: sampleTitle }, userInfo);
    thePanel = newPanel;
  });

  afterAll(async () => {
    await deleteBoardById(theBoard.id);
    await deletePanelById(thePanel.id);
    await closeDbConnections();
  });

  describe('correct input', () => {
    let id1;
    let id2;

    beforeEach(async () => {
      id1 = null;
      payload = { boardSlug: theBoard.slug, panelId: thePanel.id };
    });

    afterEach(async () => {
      if (id1) await deletePanelBoardRelById(id1);
      if (id2) await deletePanelBoardRelById(id2);
    });

    it('adds the board-panel relationship to the panel_board table', async () => {
      expect.assertions(1);
      const [result] = await addBoardToPanel(payload, ownerUserInfo);
      id1 = result.id;
      const fromDb = await findPanelBoardRelById(id1);
      expect(fromDb.board_id).toBe(theBoard.id);
    });

    it('does not add the board-panel relationship if it already exist in database', async () => {
      expect.assertions(1);
      const [result1] = await addBoardToPanel(payload, ownerUserInfo);
      id1 = result1.id;
      const [result2] = await addBoardToPanel(payload, ownerUserInfo);
      id2 = result2.id;
      expect(id1).toBe(id2);
    });

    it('is limited to the owner of the app', async () => {
      expect.assertions(2);
      try {
        const [result] = await addBoardToPanel(payload, userInfo);
        id1 = result.id;
      } catch (error) {
        expect(error.status).toBe(403);
        expect(error.code).toBe(2006);
      }
    });
  });
});
