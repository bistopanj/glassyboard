import { upsertUser } from '../../services/user-services';
import { createBoard, deleteBoardById } from '../../services/board-services';
import { closeDbConnections } from '../../dbs/pg-db';
import { addAnswer, deactivateAnswer } from '../../jobs';
import { deleteAnswerById, findAnswerById } from '../../services/answer-services';

const ownerEmail = process.env.OWNER_EMAIL;

describe('jobs.deactivateAnswer', () => {
  const sampleEmail = 'sample@email.com';
  const topic = `some topic ${Math.floor(10000 * Math.random())}`;
  const answerText = `some answer ${Math.floor(10000 * Math.random())}`;
  let userInfo;
  let board;
  let answer;
  let payload;

  beforeAll(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    board = await createBoard({ topic, createdBy: intUser.ex_id });
    userInfo = { user: { id: intUser.ex_id, email: intUser.email } };
  });

  afterAll(async () => {
    await deleteBoardById(board.id);
    await closeDbConnections();
  });

  beforeEach(async () => {
    const [newAnswer] = await addAnswer(
      { answer: answerText, slug: board.slug, boardId: board.id },
      userInfo
    );
    answer = newAnswer;
    payload = { id: answer.id };
  });

  afterEach(async () => {
    await deleteAnswerById(answer.id);
  });

  it('deactivates the answer', async () => {
    expect.assertions(1);
    await deactivateAnswer(payload, userInfo);
    const theUpdatedAnswer = await findAnswerById(answer.id, true);
    expect(theUpdatedAnswer.active).toBe(false);
  });

  it('returns the updatd answer in the first returned value', async () => {
    expect.assertions(1);
    const [result] = await deactivateAnswer(payload, userInfo);
    expect(result.active).toBe(false);
  });

  it('does not allow another (non-admin) user to deactivate the answer created by a user', async () => {
    expect.assertions(2);
    const anotherEmail = 'someother@email.com';
    const intUser2 = await upsertUser({ email: anotherEmail });
    const user2Info = { user: { id: intUser2.ex_id, email: intUser2.email } };
    try {
      await deactivateAnswer(payload, user2Info);
    } catch (error) {
      expect(error.status).toBe(400);
      expect(error.code).toBe(2061);
    }
  });

  it('allows the app owner to deactivate any answer', async () => {
    expect.assertions(1);
    const ownerIntUser = await upsertUser({ email: ownerEmail });
    const ownerUserInfo = { user: { id: ownerIntUser.ex_id, email: ownerEmail } };
    const [result] = await deactivateAnswer(payload, ownerUserInfo);
    expect(result.active).toBe(false);
  });
});
