import addBoard from '../../jobs/add-board';
import { closeDbConnections } from '../../dbs/pg-db';
import { findBoardById, deleteBoardById } from '../../services/board-services';
import { upsertUser } from '../../services/user-services';

describe('jobs.addBoard', () => {
  const sampleEmail = 'sample@email.com';
  let userInfo;
  let payload;
  let sampleTopic;

  beforeAll(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    userInfo = { user: { id: intUser.ex_id } };
  });

  afterAll(async () => {
    await closeDbConnections();
  });

  describe('correct input', () => {
    beforeEach(() => {
      sampleTopic = `sample topic ${Math.floor(10000 * Math.random())}`;
      payload = { topic: sampleTopic };
    });

    it('adds a new board to the boards table', async () => {
      expect.assertions(1);
      const [result] = await addBoard(payload, userInfo);
      const { id } = result;
      const board = await findBoardById(id);

      await deleteBoardById(id);
      expect(board).toBeDefined();
    });

    it('returns the path to the created board as the second returned value', async () => {
      expect.assertions(1);
      const [result, redirectAddress] = await addBoard(payload, userInfo);
      const { slug, id } = result;
      await deleteBoardById(id);
      expect(redirectAddress).toBe(`/board/${slug}`);
    });

    it('generates a unique slug for every board', async () => {
      expect.assertions(1);
      const [result1] = await addBoard(payload, userInfo);
      const { slug: slug1, id: id1 } = result1;

      const [result2] = await addBoard(payload, userInfo);
      const { slug: slug2, id: id2 } = result2;

      await deleteBoardById(id1);
      await deleteBoardById(id2);
      expect(slug1).not.toEqual(slug2);
    });
  });
});
