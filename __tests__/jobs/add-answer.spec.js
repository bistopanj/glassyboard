import addAnswer from '../../jobs/add-answer';
import { closeDbConnections } from '../../dbs/pg-db';
import { upsertUser } from '../../services/user-services';
import { createBoard, deleteBoardById } from '../../services/board-services';
import { findAnswerById, deleteAnswerById } from '../../services/answer-services';

describe('jobs.addAnswer', () => {
  const sampleEmail = 'sample@email.com';
  const boardTopic = `some-topic-${Math.floor(1000 * Math.random())}`;
  let board;
  let userInfo;
  let payload;
  let sampleAnswer;

  beforeAll(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    userInfo = { user: { id: intUser.ex_id } };
    const newBoard = await createBoard({ topic: boardTopic, createdBy: userInfo.user.id });
    board = newBoard;
  });

  afterAll(async () => {
    await deleteBoardById(board.id);
    await closeDbConnections();
  });

  describe('correct input', () => {
    let newAnswerId;
    beforeEach(() => {
      sampleAnswer = `some answer ${Math.floor(10000 * Math.random())}`;
      payload = { answer: sampleAnswer, boardId: board.id };
    });

    afterEach(async () => {
      await deleteAnswerById(newAnswerId);
    });

    it('adds a new answer to the given board', async () => {
      expect.assertions(1);
      const [newAnswer] = await addAnswer(payload, userInfo);
      const { id } = newAnswer;
      newAnswerId = id;
      const theAnswer = await findAnswerById(id);
      expect(theAnswer).toBeDefined();
    });
  });
});
