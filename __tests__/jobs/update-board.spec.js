import { upsertUser } from '../../services/user-services';
import { closeDbConnections } from '../../dbs/pg-db';
import { createBoard, deleteBoardById, findBoardById } from '../../services/board-services';
import { updateBoard } from '../../jobs';

const ownerEmail = process.env.OWNER_EMAIL;

describe('jobs.updateBoard', () => {
  const sampleEmail = 'sample@email.com';
  const topic = `some topic ${Math.floor(10000 * Math.random())}`;
  let board;
  let userInfo;
  let payload;
  let newTopic;

  beforeAll(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    board = await createBoard({ topic, createdBy: intUser.ex_id });
    userInfo = { user: { id: intUser.ex_id, email: intUser.email } };
  });

  afterAll(async () => {
    await deleteBoardById(board.id);
    await closeDbConnections();
  });

  beforeEach(() => {
    newTopic = `some new topic ${Math.floor(10000 * Math.random())}`;
    payload = { topic: newTopic, slug: board.slug };
  });

  it('updates the board', async () => {
    expect.assertions(1);
    await updateBoard(payload, userInfo);
    const theUpdatedBoard = await findBoardById(board.id);
    expect(theUpdatedBoard).toEqual(expect.objectContaining(payload));
  });

  it('returns the updated board in the first returned value', async () => {
    expect.assertions(1);
    const [result] = await updateBoard(payload, userInfo);
    expect(result).toEqual(expect.objectContaining(payload));
  });

  it('returns the board url in the second returned value', async () => {
    expect.assertions(1);
    const [, boardUrl] = await updateBoard(payload, userInfo);
    expect(boardUrl).toBe(`/board/${board.slug}`);
  });

  it('does not allow another (non-admin) user to update the board created by a user', async () => {
    expect.assertions(2);
    const anotherEmail = 'someother@email.com';
    const intUser2 = await upsertUser({ email: anotherEmail });
    const user2Info = { user: { id: intUser2.ex_id, email: intUser2.email } };
    try {
      await updateBoard(payload, user2Info);
    } catch (error) {
      expect(error.status).toBe(400);
      expect(error.code).toBe(2060);
    }
  });

  it('allows the app owner to update any board', async () => {
    expect.assertions(1);
    const ownerIntUser = await upsertUser({ email: ownerEmail });
    const ownerUserInfo = { user: { id: ownerIntUser.ex_id, email: ownerEmail } };
    const [result] = await updateBoard(payload, ownerUserInfo);
    expect(result).toEqual(expect.objectContaining(payload));
  });
});
