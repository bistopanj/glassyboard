import upvote from '../../jobs/upvote';
import { closeDbConnections } from '../../dbs/pg-db';
import { upsertUser } from '../../services/user-services';
import { createBoard, deleteBoardById } from '../../services/board-services';
import { insertAnswer, deleteAnswerById } from '../../services/answer-services';
import { findUpvoteById, deleteUpvoteById } from '../../services/upvote-services';

describe('jobs.upvote', () => {
  const sampleEmail = 'sample@email.com';
  const sampleEmail2 = 'sample2@email.com';
  const boardTopic = `some topic ${Math.floor(1000 * Math.random())}`;
  const answerText = `new answer ${Math.floor(2000 * Math.random())}`;
  let userInfo;
  let userInfo2;
  let board;
  let answer;

  beforeAll(async () => {
    const intUser = await upsertUser({ email: sampleEmail });
    const intUser2 = await upsertUser({ email: sampleEmail2 });
    userInfo = { user: { id: intUser.ex_id } };
    userInfo2 = { user: { id: intUser2.ex_id } };
    const newBoard = await createBoard({ topic: boardTopic, createdBy: userInfo.user.id });
    board = newBoard;
  });

  beforeEach(async () => {
    const newAnswer = await insertAnswer({
      answer: answerText,
      createdBy: userInfo.user.id,
      boardId: board.id,
    });
    answer = newAnswer;
  });

  afterEach(async () => {
    await deleteAnswerById(answer.id);
  });

  afterAll(async () => {
    await deleteBoardById(board.id);
    await closeDbConnections();
  });

  describe('correct input', () => {
    let newUpvoteId1;
    let newUpvoteId2;
    let payload;

    beforeEach(() => {
      payload = { answerId: answer.id, boardId: board.id, slug: board.slug };
      newUpvoteId1 = null;
      newUpvoteId2 = null;
    });

    afterEach(async () => {
      if (newUpvoteId1) await deleteUpvoteById(newUpvoteId1);
      if (newUpvoteId2) await deleteUpvoteById(newUpvoteId2);
    });

    it('adds an upvote for the given answer', async () => {
      expect.assertions(1);
      const [upvoteResult] = await upvote(payload, userInfo);
      const { id } = upvoteResult;
      newUpvoteId1 = id;
      const theUpvote = await findUpvoteById(id);
      expect(theUpvote).toBeDefined();
    });

    it('does not add a new upvote when the same answer is upvoted by the same user', async () => {
      expect.assertions(3);
      const [firstUpvoteResult] = await upvote(payload, userInfo);
      newUpvoteId1 = firstUpvoteResult.id;
      const [secondUpvoteResult] = await upvote(payload, userInfo);
      newUpvoteId2 = secondUpvoteResult.id;
      expect(newUpvoteId1).toBe(newUpvoteId2);
      expect(firstUpvoteResult.active).toBe(true);
      expect(secondUpvoteResult.active).toBe(false);
    });

    it('adds new upvotes to an answer when added by another user', async () => {
      expect.assertions(1);
      const [firstUpvoteResult] = await upvote(payload, userInfo);
      newUpvoteId1 = firstUpvoteResult.id;
      const [secondUpvoteResult] = await upvote(payload, userInfo2);
      newUpvoteId2 = secondUpvoteResult.id;
      expect(newUpvoteId1).not.toBe(newUpvoteId2);
    });
  });
});
