import loginReducer, { initialState } from '../../reducers/login-reducer';
import { actionTypes } from '../../actions';

describe('Login Reducer', () => {
  it('returns the default state', () => {
    const expectedState = { ...initialState };
    const newState = loginReducer(undefined, {});
    expect(newState).toEqual(expectedState);
  });

  describe('Login Modal', () => {
    it('opens the login modal when receiving an OPEN_LOGIN action', () => {
      const state1 = { ...initialState, isOpen: false, data: null };
      const payload = {
        answerId: '123',
        boardId: 'abc',
        ticketAction: 'UPVOTE',
      };
      const expectedState = { ...state1, isOpen: true, data: payload };
      const newState = loginReducer(state1, { type: actionTypes.openLogin, payload });
      expect(newState).toEqual(expectedState);
    });

    it('closes the login modal when receiving an CLOSE_LOGIN_MODA action', () => {
      const state1 = {
        ...initialState,
        isOpen: true,
        data: { answerId: '123', boardId: 'abc', ticketAction: 'UPVOTE' },
      };
      const expectedState = {
        ...state1,
        isOpen: false,
        data: null,
      };
      const newState = loginReducer(state1, { type: actionTypes.closeLoginModal });
      expect(newState).toEqual(expectedState);
    });
  });

  describe('Asking for a Ticket to Sign in', () => {
    it('sets the loading state correctly when receives an ASK_TICKET_LOADING action', () => {
      const state1 = { ...initialState };
      const expectedState = {
        ...state1,
        ticketRequest: { loading: true, error: null, success: false },
      };
      const newState = loginReducer(state1, { type: actionTypes.askTicketLoading });
      expect(newState).toEqual(expectedState);
    });

    it('sets the success state to true when receives an ASK_TICKET_SUCCESS action', () => {
      const state1 = { ...initialState };
      const expectedState = {
        ...state1,
        ticketRequest: { loading: false, error: null, success: true },
      };
      const newState = loginReducer(state1, { type: actionTypes.askTicketSuccess });
      expect(newState).toEqual(expectedState);
    });

    it('saves the error received in the state when receives an ASK_TICKET_FAILED action', () => {
      const errorData = {
        code: 1002,
        message: 'authentication service unavailable',
        moreInfo: 'some info',
        type: 'ApiError',
        status: 404,
      };
      const state1 = { ...initialState };
      const expectedState = {
        ...state1,
        ticketRequest: { loading: false, success: false, error: errorData },
      };
      const newState = loginReducer(state1, {
        type: actionTypes.askTicketFailed,
        payload: errorData,
      });
      expect(newState).toEqual(expectedState);
    });
  });

  describe('Sign in with Google', () => {
    it('sets the loading state correctly when receives an AUTH_WITH_GOOGLE_LOADING action', () => {
      const state1 = { ...initialState };
      const expectedState = {
        ...state1,
        google: { loading: true, error: null, success: false, authResult: null },
      };
      const newState = loginReducer(state1, { type: actionTypes.authWithGoogleLoading });
      expect(newState).toEqual(expectedState);
    });

    it('saves the authentication result in the state when an AUTH_WITH_GOOGLE_SUCCESS action is received', () => {
      const state1 = {
        ...initialState,
        google: { loading: true, error: null, success: false, authResult: null },
      };

      const authResult = {
        token: `some.token.${Math.floor(1000 * Math.random())}`,
        redirectTo: 'some.path',
        user: {
          id: '123-abc',
          first_name: 'some first name',
          last_name: 'some last name',
          created_at: 'some date',
          email: 'some@email.com',
        },
      };

      const newState = loginReducer(state1, {
        type: actionTypes.authWithGoogleSuccess,
        payload: authResult,
      });

      const expectedState = {
        ...state1,
        google: { loading: false, error: null, success: true, authResult },
      };
      expect(newState).toEqual(expectedState);
    });

    it('saves the error in the state when received an AUTH_WITH_GOOGLE_FAILED action', () => {
      const state1 = {
        ...initialState,
        google: { loading: true, error: null, success: false, authResult: null },
      };

      const error = {
        code: 1002,
        message: 'authentication service unavailable',
        moreInfo: 'some info',
        type: 'ApiError',
        status: 404,
      };

      const newState = loginReducer(state1, {
        type: actionTypes.authWithGoogleFailed,
        payload: error,
      });

      const expectedState = {
        ...state1,
        google: { loading: false, error, success: false, authResult: null },
      };
      expect(newState).toEqual(expectedState);
    });
  });

  describe('Sign in with Ticket', () => {
    it('sets the loading state correctly when receives an AUTH_WITH_TICKET_LOADING action', () => {
      const state1 = { ...initialState };
      const expectedState = {
        ...state1,
        ticket: { loading: true, error: null, success: false, authResult: null },
      };
      const newState = loginReducer(state1, { type: actionTypes.authWithTicketLoading });
      expect(newState).toEqual(expectedState);
    });

    it('saves the authentication result in the state when an AUTH_WITH_TICKET_SUCCESS action is received', () => {
      const state1 = {
        ...initialState,
        ticket: { loading: true, error: null, success: false, authResult: null },
      };

      const authResult = {
        token: `some.token.${Math.floor(1000 * Math.random())}`,
        redirectTo: 'some.path',
        user: {
          id: '123-abc',
          first_name: 'some first name',
          last_name: 'some last name',
          created_at: 'some date',
          email: 'some@email.com',
        },
      };

      const newState = loginReducer(state1, {
        type: actionTypes.authWithTicketSuccess,
        payload: authResult,
      });

      const expectedState = {
        ...state1,
        ticket: { loading: false, error: null, success: true, authResult },
      };
      expect(newState).toEqual(expectedState);
    });

    it('saves the error in the state when received an AUTH_WITH_TICKET_FAILED action', () => {
      const state1 = {
        ...initialState,
        ticket: { loading: true, error: null, success: false, authResult: null },
      };

      const error = {
        code: 1002,
        message: 'authentication service unavailable',
        moreInfo: 'some info',
        type: 'ApiError',
        status: 404,
      };

      const newState = loginReducer(state1, {
        type: actionTypes.authWithTicketFailed,
        payload: error,
      });

      const expectedState = {
        ...state1,
        ticket: { loading: false, error, success: false, authResult: null },
      };
      expect(newState).toEqual(expectedState);
    });
  });
});
