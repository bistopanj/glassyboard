import actionTypes from '../../actions/types';
import sampleBoards from '../../data/boards-3.json';
import samplePanels from '../../data/sample-panels.json';
import boardsReducer, { initialState } from '../../reducers/boards-reducer';

describe('Boards Reducer', () => {
  const board1 = sampleBoards[0];
  const board2 = sampleBoards[1];

  it('returns default state', () => {
    const expectedState = { ...initialState };
    const newState = boardsReducer(undefined, {});
    expect(newState).toEqual(expectedState);
  });

  describe('removing an answer', () => {
    it('removes the answer from the board when the REMOVE_ANSWER_SUCCESS action is received', () => {
      const theBoard = { ...board1 };
      const { answers, ...data } = theBoard;
      const randomAnswer = answers[Math.floor(Math.random() * answers.length)];
      const state1 = {
        ...initialState,
        bySlug: {
          [theBoard.slug]: {
            data,
            newAnswer: { loading: false, error: null, success: false },
            answers: answers.map(a => a.id),
            answerById: answers.reduce((acc, a) => ({ ...acc, [a.id]: a }), {}),
          },
        },
      };
      const newState = boardsReducer(state1, {
        type: actionTypes.removeAnswerSuccess,
        payload: { answerId: randomAnswer.id, boardSlug: theBoard.slug },
      });

      const expectedState = {
        ...state1,
        bySlug: {
          ...state1.bySlug,
          [theBoard.slug]: {
            ...state1.bySlug[theBoard.slug],
            answers: state1.bySlug[theBoard.slug].answers.filter(a => a !== randomAnswer.id),
          },
        },
      };
      expect(newState).toEqual(expectedState);
    });
  });

  describe('updating a board', () => {
    const theBoard = { ...board1 };
    const newTopic = `some new topic ${Math.floor(10000 * Math.random())}`;
    it('updates the board data when the request is sent to the server (UPDATE_BOARD_LOADING)', () => {
      const { answers, ...data } = theBoard;
      const state1 = {
        ...initialState,
        bySlug: {
          [theBoard.slug]: {
            answers: [],
            answerById: {},
            newAnswer: { loading: false, error: null, success: false },
            data,
          },
        },
      };

      const newState = boardsReducer(state1, {
        type: actionTypes.updateBoardLoading,
        payload: { slug: theBoard.slug, topic: newTopic },
      });
      const expectedState = {
        ...state1,
        bySlug: {
          ...state1.bySlug,
          [theBoard.slug]: {
            ...state1.bySlug[theBoard.slug],
            data: {
              ...state1.bySlug[theBoard.slug].data,
              topic: newTopic,
            },
          },
        },
      };

      expect(newState).toEqual(expectedState);
    });

    it('updates the updated_at field of the board when the success response is received from server (UPDATE_BOARD_SUCCESS)', () => {
      const { answers, ...data } = theBoard;
      const serverResponse = { ...theBoard, topic: newTopic, updated_at: 'some new date' };
      const state1 = {
        ...initialState,
        bySlug: {
          [theBoard.slug]: {
            answers: [],
            answerById: {},
            newAnswer: { loading: false, error: null, success: false },
            data: { ...data, topic: newTopic },
          },
        },
      };
      const newState = boardsReducer(state1, {
        type: actionTypes.updateBoardSuccess,
        payload: { slug: theBoard.slug, updatedBoard: serverResponse },
      });
      const expectedState = {
        ...state1,
        bySlug: {
          ...state1.bySlug,
          [theBoard.slug]: {
            ...state1.bySlug[theBoard.slug],
            data: {
              ...state1.bySlug[theBoard.slug].data,
              updated_at: serverResponse.updated_at,
            },
          },
        },
      };
      expect(newState).toEqual(expectedState);
    });

    it('reverts the board topic to the old topic when the update request fails (UPDATE_BOARD_FAILED)', () => {
      const { answers, ...data } = theBoard;
      const errorData = {
        errorCode: 1001,
        errorMessage: 'something went wrong',
      };
      const state1 = {
        ...initialState,
        bySlug: {
          [theBoard.slug]: {
            answers: [],
            answerById: {},
            newAnswer: { loading: false, error: null, success: false },
            data: { ...data, topic: newTopic },
          },
        },
      };
      const newState = boardsReducer(state1, {
        type: actionTypes.updateBoardFailed,
        payload: { slug: theBoard.slug, error: errorData, oldTopic: theBoard.topic },
      });

      const expectedState = {
        ...state1,
        bySlug: {
          ...state1.bySlug,
          [theBoard.slug]: {
            ...state1.bySlug[theBoard.slug],
            data: {
              ...state1.bySlug[theBoard.slug].data,
              topic: theBoard.topic,
            },
          },
        },
      };

      expect(newState).toEqual(expectedState);
    });
  });

  describe('fetching featured panels', () => {
    it('adds the featured boards to bySlug map', () => {
      const state1 = { ...initialState };
      const boardsInPanels = samplePanels.reduce((acc, { boards }) => [...acc, ...boards], []);
      const boardsInPanelsMap = boardsInPanels.reduce(
        (acc, { slug, answers = [], ...rest }) => ({
          ...acc,
          [slug]: {
            data: { slug, ...rest },
            answers: answers.map(a => a.id),
            answerById: answers.reduce((accc, a) => ({ ...accc, [a.id]: a }), {}),
          },
        }),
        {}
      );
      const expectedState = { ...state1, bySlug: boardsInPanelsMap };
      const newState = boardsReducer(state1, {
        type: actionTypes.getPanelsSuccess,
        payload: samplePanels,
      });
      expect(newState).toEqual(expectedState);
    });
  });

  describe('adding answer to board', () => {
    it('adds the answer to the board', () => {
      const slug = `some-slug-${Math.floor(1000 * Math.random())}`;
      const previousAnswers = [
        {
          id: '1234',
          answer: 'some answer',
          createdBy: '12345-6789-012345',
          upvotes: 0,
        },
        {
          id: '1235',
          answer: 'some answer 2',
          createdBy: '123455-6789-012345',
          upvotes: 12,
        },
        {
          id: '1236',
          answer: 'some answer 4',
          createdBy: '122345-6789-012345',
          upvotes: 2,
        },
      ];
      const newAnswer = {
        id: '1237',
        answer: 'some new answer',
        createdBy: '12345-6789-01234512',
        upvotes: 0,
      };
      const state1 = {
        ...initialState,
        bySlug: {
          [slug]: {
            answers: previousAnswers.map(a => a.id),
            answerById: previousAnswers.reduce((acc, a) => ({ ...acc, [a.id]: a }), {}),
            newAnswer: { loading: true },
          },
        },
      };
      const expectedState = {
        ...state1,
        bySlug: {
          ...state1.bySlyg,
          [slug]: {
            ...state1.bySlug[slug],
            answers: [newAnswer.id, ...state1.bySlug[slug].answers],
            answerById: { ...state1.bySlug[slug].answerById, [newAnswer.id]: { ...newAnswer } },
            newAnswer: { loading: false, success: true, error: null },
          },
        },
      };
      const newState = boardsReducer(state1, {
        type: actionTypes.addAnswerSuccess,
        payload: { slug, answer: newAnswer },
      });
      expect(newState).toEqual(expectedState);
    });

    it('sets loading state for the new answer correctly', () => {
      const state1 = { ...initialState };
      const slug = `some-slug-${Math.floor(1000 * Math.random())}`;
      const expectedState = {
        ...initialState,
        bySlug: {
          [slug]: {
            ...state1.bySlug[slug],
            newAnswer: { loading: true, success: null, error: null },
          },
        },
      };
      const newState = boardsReducer(state1, { type: actionTypes.addAnswerLoading, payload: slug });
      expect(newState).toEqual(expectedState);
    });
  });

  describe('adding new board', () => {
    it('sets loading state for the new board correctly', () => {
      const state1 = { ...initialState };
      const expectedState = { ...initialState, new: { ...initialState.new, loading: true } };

      const newState = boardsReducer(state1, { type: actionTypes.addBoardLoading });
      expect(newState).toEqual(expectedState);
    });

    it('updates the success state with the slug of the new board and adds the new board to the state when it is added successfully', () => {
      const state1 = { ...initialState };
      const newBoard = board1;
      const { answers = [], ...rest } = newBoard;
      const expectedState = {
        ...initialState,
        bySlug: {
          ...initialState.bySlug,
          [newBoard.slug]: {
            data: rest,
            answers: answers.map(a => a.id),
            answerById: answers.reduce((acc, a) => ({ ...acc, [a.id]: a }), {}),
          },
        },
        new: { ...initialState.new, loading: false, success: newBoard.slug },
      };
      const newState = boardsReducer(state1, {
        type: actionTypes.addBoardSuccess,
        payload: newBoard,
      });
      expect(newState).toEqual(expectedState);
    });
  });

  describe('getiing primary board', () => {
    it('saves the boardData in the store state correctly', () => {
      const state1 = { ...initialState };
      const { answers, ...rest } = board1;
      const expectedState = {
        ...state1,
        primary: board1.slug,
        bySlug: {
          [board1.slug]: {
            data: rest,
            answers: answers.map(a => a.id),
            answerById: answers.reduce((acc, a) => ({ ...acc, [a.id]: a }), {}),
          },
        },
      };
      const newState = boardsReducer(state1, {
        type: actionTypes.getCompanyBoardSuccess,
        payload: board1,
      });
      expect(newState).toEqual(expectedState);
    });
  });

  describe('getting boards by slug', () => {
    let slug;

    beforeEach(() => {
      slug = `some-sample-slug-${Math.floor(1000 * Math.random())}`;
    });

    it('sets the loading status for the given board correctly', () => {
      const state1 = { ...initialState, bySlug: { slug1: { data: { name: 'some name' } } } };
      const expectedState = {
        ...initialState,
        bySlug: {
          slug1: { data: { name: 'some name' } },
          [slug]: { loading: true },
        },
      };
      const newState = boardsReducer(state1, {
        type: actionTypes.getBoardLoading,
        payload: slug,
      });
      expect(newState).toEqual(expectedState);
    });

    it('sets the boardData for the specified board correctly in the state', () => {
      const state1 = {
        ...initialState,
        bySlug: {
          [slug]: { loading: true },
        },
      };
      const { answers, ...rest } = board2;
      const expectedState = {
        ...initialState,
        bySlug: {
          [slug]: {
            data: rest,
            answers: answers.map(a => a.id),
            answerById: answers.reduce((acc, a) => ({ ...acc, [a.id]: a }), {}),
          },
        },
      };

      const newState = boardsReducer(state1, {
        type: actionTypes.getBoardSuccess,
        payload: { slug, data: { ...board2 } },
      });

      expect(newState).toEqual(expectedState);
    });

    it('sets the error data for the board specified by the given slug in the state', () => {
      const state1 = {
        ...initialState,
        bySlug: {
          board1Slug: { data: { ...board1 } },
          [slug]: { loading: true },
        },
      };
      const errorData = {
        errorCode: 1001,
        errorMessage: 'something went wrong',
      };

      const expectedState = {
        ...initialState,
        bySlug: {
          board1Slug: { data: { ...board1 } },
          [slug]: { error: { ...errorData } },
        },
      };
      const newState = boardsReducer(state1, {
        type: actionTypes.getBoardFailed,
        payload: { slug, error: errorData },
      });
      expect(newState).toEqual(expectedState);
    });
  });

  describe('upvoting', () => {
    const boardTopic = 'some topic';
    const answerId = '1234-abcd-efgh-5678';
    const answerText = 'answer one';
    const boardId = 'bgdhc-123ax-1ssf313';
    const slug = `some-slug-${Math.floor(1000 * Math.random())}`;

    it('updates the upvote state correctly when the answer has not been upvoted before and UPVOTE_LOADING is dispatched', () => {
      const initialAnswerData = {
        id: answerId,
        answer: answerText,
        created_at: 'some date',
        updated_at: 'some date',
        upvotes: 12,
        upvoted: false,
      };
      const state1 = {
        ...initialState,
        bySlug: {
          [slug]: {
            answers: [answerId],
            answerById: {
              [answerId]: { ...initialAnswerData },
            },
            newAnswer: { loading: false, error: null, success: null },
            data: {
              created_at: 'some date',
              updated_at: 'some date',
              id: boardId,
              slug,
              topic: boardTopic,
            },
          },
        },
      };
      const expectedState = {
        ...state1,
        bySlug: {
          ...state1.bySlug,
          [slug]: {
            ...state1.bySlug[slug],
            answerById: {
              [answerId]: {
                ...initialAnswerData,
                upvotes: initialAnswerData.upvotes + 1,
                upvoted: true,
              },
            },
          },
        },
      };
      const newState = boardsReducer(state1, {
        type: actionTypes.upvoteLoading,
        payload: { answerId, slug },
      });
      expect(newState).toEqual(expectedState);
    });

    it('updates the upvote state correctly when the answer has been upvoted before and UPVOTE_LOADING is dispatched', () => {
      const initialAnswerData = {
        id: answerId,
        answer: answerText,
        created_at: 'some date',
        updated_at: 'some date',
        upvotes: 12,
        upvoted: true,
      };
      const state1 = {
        ...initialState,
        bySlug: {
          [slug]: {
            answers: [answerId],
            answerById: {
              [answerId]: { ...initialAnswerData },
            },
            newAnswer: { loading: false, error: null, success: null },
            data: {
              created_at: 'some date',
              updated_at: 'some date',
              id: boardId,
              slug,
              topic: boardTopic,
            },
          },
        },
      };
      const expectedState = {
        ...state1,
        bySlug: {
          ...state1.bySlug,
          [slug]: {
            ...state1.bySlug[slug],
            answerById: {
              [answerId]: {
                ...initialAnswerData,
                upvotes: initialAnswerData.upvotes - 1,
                upvoted: false,
              },
            },
          },
        },
      };
      const newState = boardsReducer(state1, {
        type: actionTypes.upvoteLoading,
        payload: { answerId, slug },
      });
      expect(newState).toEqual(expectedState);
    });
  });
});
