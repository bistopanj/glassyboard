import companyReducer, { initialState } from '../../reducers/company-reducer';
import { actionTypes } from '../../actions';

describe('Company Reducer', () => {
  it('returns the default state', () => {
    const expectedState = { ...initialState };
    const newState = companyReducer(undefined, {});
    expect(newState).toEqual(expectedState);
  });

  describe('Adding a new Company', () => {
    it('sets the loading state for the new company correctly', () => {
      const state1 = { ...initialState };
      const expectedState = { ...initialState, new: { ...initialState.new, loading: true } };
      const newState = companyReducer(state1, { type: actionTypes.addCompanyLoading });
      expect(newState).toEqual(expectedState);
    });

    it('updates the success state correctly', () => {
      const serverResponse = {
        id: '3e285d01-f0ad-4ff6-a712-df69c5b2b3cf',
        subdomain: 'subdomain',
        primary_board: 'dd268330-c1c9-48a5-841f-eba2eb67e11a',
        created_at: '2020-04-16T15:48:39.626Z',
        updated_at: '2020-04-16T15:48:39.643Z',
        url: 'http://subdomain.glassyboard.com/',
        board_slug: 'default-company-board-subdomain-123123',
      };
      const state1 = { ...initialState, new: { ...initialState.new, loading: true } };
      const expectedState = {
        new: { ...initialState.new, loading: false, error: null, success: true },
        data: serverResponse,
      };
      const newState = companyReducer(state1, {
        type: actionTypes.addCompanySuccess,
        payload: serverResponse,
      });
      expect(newState).toEqual(expectedState);
    });

    it('updates the failed state correctly', () => {
      const errorData = {
        code: 1002,
        message: 'service unavailable',
        moreInfo: 'some info',
        type: 'ApiError',
        status: 404,
      };
      const state1 = { ...initialState, new: { ...initialState.new, loading: true } };
      const expectedState = {
        ...state1,
        new: { loading: false, success: false, error: errorData },
        data: null,
      };
      const newState = companyReducer(state1, {
        type: actionTypes.addCompanyFailed,
        payload: errorData,
      });
      expect(newState).toEqual(expectedState);
    });

    it('resets the state when ADD_COMPANY_RESET action is received', () => {
      const serverResponse = {
        id: '3e285d01-f0ad-4ff6-a712-df69c5b2b3cf',
        subdomain: 'subdomain',
        primary_board: 'dd268330-c1c9-48a5-841f-eba2eb67e11a',
        created_at: '2020-04-16T15:48:39.626Z',
        updated_at: '2020-04-16T15:48:39.643Z',
        url: 'http://subdomain.glassyboard.com/',
        board_slug: 'default-company-board-subdomain-123123',
      };
      const state1 = {
        ...initialState,
        new: { ...initialState.new, success: true },
        data: serverResponse,
      };
      const expectedState = { ...initialState };
      const newState = companyReducer(state1, { type: actionTypes.addCompanyReset });
      expect(newState).toEqual(expectedState);
    });
  });
});
