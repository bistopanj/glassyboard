import connectionInfoReducer, { initialState } from '../../reducers/connection-info-reducer';
import { actionTypes } from '../../actions';

describe('User Info Reducer', () => {
  it('returns default state', () => {
    const expectedState = { ...initialState };
    const newState = connectionInfoReducer(undefined, {});
    expect(newState).toEqual(expectedState);
  });

  describe('updating userInfo from cookies', () => {
    it('updates the authorization token when received an UPDATE_CONNECTION_INFO action with the token', () => {
      const state1 = { ...initialState };
      const token = 'abcdef';
      const host = 'somehost.com';
      const protocol = 'https';
      const subdomain = 'subdomain';
      const expectedState = { ...initialState, token, user: null, host, protocol, subdomain };
      const newState = connectionInfoReducer(state1, {
        type: actionTypes.updateConnectionInfo,
        payload: { token, host, subdomain, protocol },
      });
      expect(newState).toEqual(expectedState);
    });

    it('clears the userInfo state when received an UPDATE_CONNECTION_INFO with no payload', () => {
      const state1 = { token: 'abcde' };
      const expectedState = { token: null, user: null };

      const newState = connectionInfoReducer(state1, { type: actionTypes.updateConnectionInfo });

      expect(newState).toEqual(expectedState);
    });
  });
});
