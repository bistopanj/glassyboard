import configureMockStore from 'redux-mock-store';
import { mount } from 'enzyme';
import { useRouter } from 'next/router';
import { Provider } from 'react-redux';
import { WrappedComponent as BoardToolbox } from '../../components/board-toolbox';
import { findByTestAttr, checkProps } from '../../utils/test-utils';
import { initialState as defaultState } from '../../reducers';
import { middlewares } from '../../build-store';

jest.mock('next/router');

const mockStore = configureMockStore(middlewares);

const defaultSlug = `some-slug-${Math.floor(1000 * Math.random())}`;
const defaultTopic = `some interesting topic ${Math.floor(1000 * Math.random())}`;
const defaultProps = {
  topic: defaultTopic,
  slug: defaultSlug,
};
const defaultRouter = { pathname: '/', query: {}, asPath: '/' };

const setUp = ({
  props = defaultProps,
  initialState = defaultState,
  router = defaultRouter,
} = {}) => {
  useRouter.mockReturnValue(router);
  const store = mockStore(initialState);
  const parent = mount(
    <Provider store={store}>
      <BoardToolbox {...props} />
    </Provider>
  );
  const wrapper = parent.find('BoardToolbox');
  return [wrapper, store];
};

describe('<BoardToolbox />', () => {
  let router;
  const slug = `some-slug-${Math.floor(1000 * Math.random())}`;

  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        topic: 'some topic',
        slug: 'some-slug',
        showEdit: true,
        enableEditing: () => {
          const someVar = 1;
          return someVar;
        },
      };
      const propsErrors = checkProps(BoardToolbox, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appearance', () => {
    it('renders without error', () => {
      const [wrapper] = setUp();
      const container = findByTestAttr(wrapper, 'board-toolbox-container');
      expect(container).toHaveLength(1);
    });

    it('renders a share button', () => {
      const [wrapper] = setUp();
      const shareButton = findByTestAttr(wrapper, 'share-board-button', 'a');
      expect(shareButton).toHaveLength(1);
    });

    it('always renders the board menu toggle to open the board menu when rendered on any page other than the board page', () => {
      router = { pathname: `/some/path/${Math.floor(10000 * Math.random())}` };
      const [wrapper] = setUp({ router });
      const menuButton = findByTestAttr(wrapper, 'board-menu-toggle', 'button');
      expect(menuButton).toHaveLength(1);
    });

    it('does not render the board menu toggle for an non-editable board and no admin in query', () => {
      router = { pathname: '/board/[slug]' };
      const [wrapper] = setUp({ router });
      const menuButton = findByTestAttr(wrapper, 'board-menu-toggle', 'button');
      expect(menuButton).toHaveLength(0);
    });

    it('renders an Edit Board button if the showEdit prop is true', () => {
      const [wrapper] = setUp({ props: { showEdit: true, slug, topic: defaultTopic } });
      const editButton = findByTestAttr(wrapper, 'board-menu-edit', 'button');
      expect(editButton).toHaveLength(1);
    });

    it('renders a "Go To Board Page" button in the menu when rendered on a route other than the board page', () => {
      router = { pathname: '/some/path' };
      const [wrapper] = setUp({ props: { showEdit: true, slug, topic: defaultTopic }, router });
      const goToBoardPageItem = findByTestAttr(wrapper, 'board-menu-open-board-page', 'a');
      expect(goToBoardPageItem).toHaveLength(1);
    });
  });

  describe('behavior', () => {
    it('hides the Edit Board menu button when the showEdit prop is false', () => {
      const [wrapper] = setUp({ props: { showEdit: false, slug, topic: defaultTopic } });
      const editButton = findByTestAttr(wrapper, 'board-menu-edit', 'button');
      expect(editButton).toHaveLength(0);
    });

    it('hides the Go to Board Page menu item on the board page', () => {
      router = { pathname: '/board/[slug]' };
      const [wrapper] = setUp({ props: { showEdit: true, slug, topic: defaultTopic }, router });
      const goToBoardPageItem = findByTestAttr(wrapper, 'board-menu-open-board-page', 'a');
      expect(goToBoardPageItem).toHaveLength(0);
    });
  });

  describe('admin buttons', () => {
    const props = { showEdit: true, slug, topic: defaultTopic };

    beforeAll(() => {
      router = { ...defaultRouter, query: { admin: 'admin' } };
    });

    it('shows a button to add the board to a featured panel', () => {
      const [wrapper] = setUp({ initialState: defaultState, props, router });
      const addToPanelButton = findByTestAttr(wrapper, 'board-menu-add-to-panel', 'a');
      expect(addToPanelButton).toHaveLength(1);
    });
  });
});
