import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import UpvoteButton from '../../components/upvote-button';
import { findByTestAttr, tick, buildMockResponse, checkProps } from '../../utils/test-utils';
import { middlewares } from '../../build-store';
import { initialState as boardsInitialState } from '../../reducers/boards-reducer';
import { initialState as connectionInfoInitialState } from '../../reducers/connection-info-reducer';
import { initialState as loginInitialState } from '../../reducers/login-reducer';
import { actionTypes } from '../../actions';

const mockStore = configureMockStore(middlewares);
const baseUrl = process.env.BASE_URL;

const defaultProps = {
  upvotes: 12,
  answerId: 'abcdef',
  boardId: 'ghijkl',
  slug: 'some-slug',
};

const defaultState = {
  boards: boardsInitialState,
  connectionInfo: connectionInfoInitialState,
  login: loginInitialState,
};

const sampleEmail = 'test@email.com';
const loggedInState = {
  ...defaultState,
  connectionInfo: { token: '123', user: { email: sampleEmail } },
};

const setUp = ({ props = defaultProps, initialState = defaultState } = {}) => {
  const store = mockStore(initialState);
  const parent = mount(
    <Provider store={store}>
      <UpvoteButton {...props} />
    </Provider>
  );

  const wrapper = parent.find('UpvoteButton').childAt(0);

  return [wrapper, store];
};

describe('<UpvoteButton />', () => {
  let wrapper;

  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        upvotes: 1,
        boardId: '234234',
        answerId: 'someId',
        slug: 'some-slug',
      };
      const propsErrors = checkProps(UpvoteButton.WrappedComponent, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appreance', () => {
    beforeEach(() => {
      const [w] = setUp();
      wrapper = w;
    });

    it('renders without errors', () => {
      const btnWrapper = findByTestAttr(wrapper, 'upvote-btn');
      expect(btnWrapper).toHaveLength(1);
    });

    it('renders correctly', () => {
      expect(wrapper).toMatchSnapshot();
    });

    it('shows the number of upvotes', () => {
      const btnWrapper = findByTestAttr(wrapper, 'upvote-btn');
      expect(btnWrapper.text()).toMatch(defaultProps.upvotes.toString());
    });

    it('applies a different style if the answer is upvoted by the user', () => {
      const [upvotedWrapper] = setUp({ props: { ...defaultProps, upvoted: true } });
      const btnWrapper = findByTestAttr(upvotedWrapper, 'upvote-btn');
      expect(btnWrapper.hasClass('upvoted')).toBe(true);
    });
  });

  describe('behavior', () => {
    let store;

    beforeEach(() => {});

    it('dispatches the correct action when it is pressed (not logged in)', async () => {
      expect.assertions(1);
      const [w, s] = setUp();
      wrapper = w;
      store = s;
      const expectedActions = [
        {
          type: actionTypes.openLogin,
          payload: {
            ticketAction: 'UPVOTE',
            payload: {
              answerId: defaultProps.answerId,
              slug: defaultProps.slug,
              boardId: defaultProps.boardId,
            },
          },
        },
      ];
      const theButton = findByTestAttr(wrapper, 'upvote-btn');
      theButton.simulate('click');
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });

    it('dispatches the correct action when it is pressed (logged in)', async () => {
      expect.assertions(1);
      const [w, s] = setUp({ initialState: loggedInState });
      wrapper = w;
      store = s;
      const serverResponse = {
        id: '1234',
        active: true,
        created_at: 'some-date',
        updated_at: 'some-date',
        user_id: 'abcs',
        answer_id: defaultProps.answerId,
        board_id: defaultProps.boardId,
      };
      const expectedActions = [
        {
          type: actionTypes.upvoteLoading,
          payload: {
            answerId: defaultProps.answerId,
            slug: defaultProps.slug,
          },
        },
        {
          type: actionTypes.upvoteSuccess,
          payload: {
            answerId: defaultProps.answerId,
            slug: defaultProps.slug,
            upvote: serverResponse,
          },
        },
      ];

      fetch.doMockIf(`${baseUrl}/api/upvotes`, buildMockResponse(serverResponse));

      const theButton = findByTestAttr(wrapper, 'upvote-btn');
      theButton.simulate('click');
      await tick();
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });
  });
});
