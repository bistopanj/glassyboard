import { shallow, mount } from 'enzyme';
import { useDispatch } from 'react-redux';
import updateBoardAction from '../../actions/update-board';
import Board from '../../components/board';
import { findByTestAttr, checkProps } from '../../utils/test-utils';

jest.mock('react-redux');
jest.mock('../../actions/update-board');

const defaultProps = {
  editable: false,
  slug: 'some-slug',
  error: undefined,
  loading: undefined,
  data: { topic: 'sample topic', slug: 'some-slug', id: '123' },
  answers: [],
};

const setUp = (props = defaultProps) => {
  return shallow(<Board {...props} />);
};

describe('<Board />', () => {
  const sampleSlug = `some-slug-${Math.floor(1000 * Math.random())}`;
  const topic = `some topic for our test boaord ${Math.floor(10000 * Math.random())}`;
  const boardData = { topic, slug: sampleSlug, id: '123' };
  const sampleProps = { ...defaultProps, slug: sampleSlug, data: boardData };

  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        slug: 'some-slug',
        error: { code: 213, type: 'ServerError' },
        loading: true,
        data: null,
        answers: [
          {
            id: 'a-1',
            answer: 'some answer',
            upvotes: 12,
          },
          {
            id: 'a-2',
            answer: 'another answer',
            upvotes: 1200,
            created_at: '2020-03-17T15:26:58.230169+03:30',
          },
        ],
      };
      const propsErrors = checkProps(Board, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appearance', () => {
    it('renders without error', () => {
      const wrapper = setUp(sampleProps);
      const container = findByTestAttr(wrapper, 'board-container');
      expect(container).toHaveLength(1);
    });

    it('shows board topic', () => {
      const wrapper = setUp(sampleProps);
      const topicEl = findByTestAttr(wrapper, 'board-topic').dive();
      expect(topicEl.text()).toContain(topic);
    });

    it('renders one <NewAnswerCard /> to add a new answer to the topic', () => {
      const wrapper = setUp(sampleProps);
      const newAnswerCard = wrapper.find('NewAnswerCard');
      expect(newAnswerCard).toHaveLength(1);
    });

    it('renders one <AnswerCard /> for every answer it receives in props.answers', () => {
      const answerCount = Math.floor(20 * Math.random());
      const sampleAnswers = new Array(answerCount).fill(0).map((_, i) => ({
        id: `a-${i}`,
        answer: `answer ${i}`,
      }));
      const wrapper = setUp({ ...sampleProps, answers: sampleAnswers });
      const answerCards = wrapper.find('AnswerCard');
      expect(answerCards).toHaveLength(sampleAnswers.length);
    });

    it('renders one <BoardToolbox /> in the board header', () => {
      const wrapper = setUp(sampleProps);
      const toolbox = wrapper.find('BoardToolbox');

      expect(toolbox).toHaveLength(1);
    });

    it('renders an Editable topic in the header', () => {
      const wrapper = setUp(sampleProps);
      const editableTopic = wrapper.find('EditableTopic');

      expect(editableTopic).toHaveLength(1);
    });
  });

  describe('behavior', () => {
    it('passes down a function to <BoardToolbox /> to enable editing the topic', () => {
      const wrapper = setUp({ ...sampleProps, editable: true });
      const boardToolbox = wrapper.find('BoardToolbox');
      expect(wrapper.find('EditableTopic').prop('editing')).toBe(false);
      boardToolbox.prop('enableEditing')();
      expect(wrapper.find('EditableTopic').prop('editing')).toBe(true);
    });

    it('passes down the necessary data to BoardToolbox', () => {
      const wrapper = setUp({ ...sampleProps, editable: true });
      const boardToolbox = wrapper.find('BoardToolbox');
      const expectedProps = { slug: sampleSlug, showEdit: true };
      const passedProps = boardToolbox.props();
      expect(passedProps).toEqual(expect.objectContaining(expectedProps));
    });

    it('dispatches the appropriate action when its EditableTopic child calls its onConfirm prop with new title for the board', () => {
      const newTopic = `some new topic ${Math.floor(10000 * Math.random())}`;
      const actionResult = { type: `SOME-TYPE-${Math.floor(1000 * Math.random())}` };
      const dispatch = jest.fn();
      useDispatch.mockImplementation(() => dispatch);
      updateBoardAction.mockReturnValue(actionResult);
      const wrapper = setUp({ ...sampleProps, editable: true });
      const editableTopic = findByTestAttr(wrapper, 'board-topic', 'EditableTopic');
      editableTopic.prop('onConfirm')(newTopic);
      expect(dispatch).toHaveBeenNthCalledWith(1, actionResult);
      expect(updateBoardAction).toHaveBeenNthCalledWith(
        1,
        boardData.id,
        sampleSlug,
        newTopic,
        topic
      );
    });
  });
});
