import { shallow } from 'enzyme';
import { useRouter } from 'next/router';
import SigninDialog from '../../components/signin-dialog';
import { findByTestAttr, checkProps } from '../../utils/test-utils';

jest.mock('next/router');

const setUp = (props = {}) => {
  const wrapper = shallow(<SigninDialog {...props} />);
  return wrapper;
};

describe('<SigninDialog />', () => {
  let wrapper;
  const routerPush = jest.fn();
  useRouter.mockReturnValue({ push: routerPush });

  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        signinData: {
          ticketAction: 'SOME-ACTION',
          payload: {
            someData: 'some-value',
          },
        },
        onRedirect: () => {
          const someVar = 1;
          return someVar;
        },
      };
      const propsErrors = checkProps(SigninDialog.WrappedComponent, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appearance', () => {
    it('renders without error', () => {
      wrapper = setUp();
      const container = findByTestAttr(wrapper, 'signin-dialog-container');
      expect(container).toHaveLength(1);
    });

    it('renders one <LoginSocial />', () => {
      wrapper = setUp();
      const socialLoginComp = wrapper.find('LoginSocial');
      expect(socialLoginComp).toHaveLength(1);
    });

    it('renders one <LoginEmail />', () => {
      wrapper = setUp();
      const emailLoginComp = wrapper.find('LoginEmail');
      expect(emailLoginComp).toHaveLength(1);
    });

    it('separates the social login and email login with an appropriate divider', () => {
      wrapper = setUp();
      const horizontalDivider = wrapper.find('HorizontalDivider');
      expect(horizontalDivider).toHaveLength(1);
    });

    it('renders a guide when some data in provided to the login modal (login while performing protected action)', () => {
      wrapper = setUp({
        signinData: { ticketAction: 'UPVOTE', payload: { someData: 'someValue' } },
      });
      const signinGuide = findByTestAttr(wrapper, 'signin-guide');
      expect(signinGuide).toHaveLength(1);
    });

    it('does not render the guide when no data is provided', () => {
      wrapper = setUp();
      const signinGuide = findByTestAttr(wrapper, 'signin-guide');
      expect(signinGuide).toHaveLength(0);
    });

    it('renders with correct style', () => {
      wrapper = setUp();
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('behavior', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('passes down its signinData prop to its signin children as data', () => {
      const signinData = {
        ticketAction: 'UPVOTE',
        payload: { someData: `someValue-${Math.floor(10000 * Math.random())}` },
      };
      wrapper = setUp({ signinData });
      const socialLoginComp = wrapper.find('LoginSocial');
      const emailLoginComp = wrapper.find('LoginEmail');
      expect(socialLoginComp.prop('data')).toEqual(signinData);
      expect(emailLoginComp.prop('data')).toEqual(signinData);
    });

    it('passes down its onRedirect prop to its LoginSocial child', () => {
      const onRedirect = jest.fn();
      wrapper = setUp({ onRedirect });
      const socialLoginComp = wrapper.find('LoginSocial');
      socialLoginComp.prop('onRedirect')();
      socialLoginComp.prop('onRedirect')();
      socialLoginComp.prop('onRedirect')();
      expect(onRedirect).toHaveBeenCalledTimes(3);
    });

    it('passes down its onRedirect prop to its LoginEmail child', () => {
      const onRedirect = jest.fn();
      wrapper = setUp({ onRedirect });
      const emailLoginComp = wrapper.find('LoginEmail');
      emailLoginComp.prop('onRedirect')();
      emailLoginComp.prop('onRedirect')();
      emailLoginComp.prop('onRedirect')();
      expect(onRedirect).toHaveBeenCalledTimes(3);
    });
  });
});
