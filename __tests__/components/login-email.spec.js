import configureMockStore from 'redux-mock-store';
import { useRouter } from 'next/router';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import LoginEmail from '../../components/login-email';
import { findByTestAttr, checkProps, tick } from '../../utils/test-utils';
import { initialState } from '../../reducers';
import { middlewares } from '../../build-store';
import askTicketAction from '../../actions/ask-ticket';

jest.mock('next/router');
jest.mock('../../actions/ask-ticket');

const mockStore = configureMockStore(middlewares);
const defaultProps = { data: undefined };
const emailRegEx = /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i;

const setUp = ({ props = defaultProps, state = initialState } = {}) => {
  const store = mockStore(state);
  const parent = mount(
    <Provider store={store}>
      <LoginEmail {...props} />
    </Provider>
  );
  const wrapper = parent.find('LoginEmail').childAt(0);
  return [wrapper, store];
};

const typeCharacter = (wrapper, char) => {
  const textInput = findByTestAttr(wrapper, 'email-input', 'input');
  const oldChar = textInput.instance().value;
  const newVal = `${oldChar}${char}`;
  textInput.instance().value = newVal;
  textInput.simulate('change', { target: { value: newVal } });
  wrapper.update();
};

describe('<LoginEmail />', () => {
  const routerPush = jest.fn();
  useRouter.mockReturnValue({ push: routerPush });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        data: {
          ticketAction: 'SOME-ACTION',
          payload: {
            someData: 'value',
          },
        },
      };

      const propsErrors = checkProps(LoginEmail, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appearance', () => {
    it('renders without error', () => {
      const [wrapper] = setUp();
      const formContainer = findByTestAttr(wrapper, 'login-form', 'form');
      expect(formContainer).toHaveLength(1);
    });

    it('renders a text input for entering email address', () => {
      const [wrapper] = setUp();
      const textInput = findByTestAttr(wrapper, 'email-input', 'input');
      expect(textInput).toHaveLength(1);
    });

    it('renders a submit button', () => {
      const [wrapper] = setUp();
      const button = findByTestAttr(wrapper, 'login-btn', 'button');
      expect(button).toHaveLength(1);
    });

    it('renders with correct style', () => {
      const [wrapper] = setUp();
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('behavior', () => {
    const testEmail = 'sample@email.com';
    const data = {
      payload: {
        someData: 'value',
      },
      ticketAction: 'SOME-ACTION',
    };

    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('dispatches the askTicket action with correct input when the submit button is clicked', async () => {
      expect.assertions(2);
      const action1 = { type: 'SOME_ACTION', payload: { someData: 'some-value' } };
      const action2 = { type: 'ANOTHER_ACTION', payload: { someData: 'some-other-value' } };
      askTicketAction.mockReturnValue(async dispatch => {
        dispatch(action1);
        await tick();
        dispatch(action2);
      });
      const expectedActions = [action1, action2];
      const [wrapper, store] = setUp({ props: { data } });
      const textInput = findByTestAttr(wrapper, 'email-input', 'input');
      const loginForm = findByTestAttr(wrapper, 'login-form', 'Form');
      textInput.instance().value = testEmail;
      textInput.simulate('change', { target: { value: testEmail } });
      loginForm.simulate('submit');
      await tick();
      const dispatchedActions = store.getActions();
      expect(askTicketAction).toHaveBeenNthCalledWith(1, { email: testEmail, ticketInfo: data });
      expect(dispatchedActions).toEqual(expectedActions);
    });

    it('disables the submit button until a correct email address is entered', async () => {
      const [wrapper] = setUp();
      const inputChars = testEmail.split('');
      let button;
      inputChars.forEach(async (char, index) => {
        const enteredStr = testEmail.slice(0, index + 1);
        typeCharacter(wrapper, char);
        button = findByTestAttr(wrapper, 'login-btn', 'button');
        await tick();
        expect(button.prop('disabled')).toBe(!emailRegEx.test(enteredStr));
      });
    });

    it('passes dows its loading props to <AsyncButton />', () => {
      const loadingState = {
        ...initialState,
        login: {
          ...initialState.login,
          ticketRequest: { ...initialState.login.ticketRequest, loading: true },
        },
      };
      const [wrapper] = setUp({ state: loadingState });
      const submitButton = findByTestAttr(wrapper, 'login-btn-wrapper');
      expect(submitButton.prop('loading')).toBe(true);
    });
  });
});

/*
describe('<LoginEmail />', () => {
  

  describe('behavior', () => {

    it('calls its props.redirectTo method with appropriate path when the process is done successfully', () => {
      const redirectTo = jest.fn();
      const wrapper = setUp({ data, redirectTo }, true);
      const textInput = findByTestAttr(wrapper, 'email-input', 'input');
      textInput.instance().value = testEmail;
      textInput.simulate('change', { target: { value: testEmail } });
      wrapper.update();
      wrapper.setProps({ success: true });

      expect(redirectTo).toHaveBeenNthCalledWith(
        1,
        '/tickets/sent/[email]',
        `/tickets/sent/${encodeURIComponent(testEmail)}`
      );
    });
  });
});

*/
