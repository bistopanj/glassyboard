import configureMockStore from 'redux-mock-store';
import { shallow, mount } from 'enzyme';
import { findByTestAttr, checkProps } from '../../utils/test-utils';
import { middlewares } from '../../build-store';
import LoginModal from '../../components/login-modal';
import { initialState as defaultState } from '../../reducers';
import { initialState as initialLoginState } from '../../reducers/login-reducer';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);
const defaultLoginState = { ...initialLoginState, isOpen: true };

const setUp = ({
  loginState = defaultLoginState,
  doMount = false,
  initialState = defaultState,
} = {}) => {
  const state = { ...initialState, login: loginState };
  const store = mockStore(state);
  let wrapper;
  if (doMount) wrapper = mount(<LoginModal store={store} />);
  else {
    const parent = shallow(<LoginModal store={store} />).dive();
    wrapper = parent.dive();
  }
  return [wrapper, store];
};

describe('<LoginModal />', () => {
  const { location } = window;

  beforeAll(() => {
    delete window.location;
    window.location = { replace: jest.fn() };
  });

  afterAll(() => {
    window.location = location;
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        isOpen: true,
        data: { ticketAction: 'SOME-ACTION', payload: { someData: '1' } },
        closeLoginModal: () => {
          const someVar = 1;
          return someVar;
        },
      };
      const propsErrors = checkProps(LoginModal.WrappedComponent, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appearance', () => {
    it('renders a modal when the isOpen property is true in the state', () => {
      const [wrapper] = setUp({ loginState: { ...defaultLoginState, isOpen: true } });
      const modalContainer = findByTestAttr(wrapper, 'login-modal-container');
      expect(modalContainer).toHaveLength(1);
    });

    it('renders nothing when the isOpen property is false in the state', () => {
      const [wrapper] = setUp({ loginState: { ...defaultLoginState, isOpen: false } });
      const modalContainer = findByTestAttr(wrapper, 'login-modal-container');
      expect(modalContainer).toHaveLength(0);
    });

    it('renders correctly', () => {
      const [wrapper] = setUp({ loginState: { ...defaultLoginState, isOpen: true } });
      expect(wrapper).toMatchSnapshot();
    });

    it('renders a close button', () => {
      const [wrapper] = setUp();
      const modalHeader = wrapper.find('ModalHeader').dive();
      const closeButton = modalHeader.find('button.close[aria-label="Close"]');
      expect(closeButton).toHaveLength(1);
    });
  });

  describe('behavior', () => {
    const data = {
      ticketAction: 'SOME-ACTION',
      payload: {
        data1: 'some data',
        data2: 12,
      },
    };

    it('passes the login data from the store to the <SigninDialog />', () => {
      const [wrapper] = setUp({
        loginState: { ...defaultLoginState, data },
      });

      const signinDialog = wrapper.find('SigninDialog');
      expect(signinDialog.prop('signinData')).toEqual(data);
    });

    it('passes down its closeLoginModal prop to the SigninDialog child', () => {
      const closeFn = jest.fn();
      const wrapper = shallow(<LoginModal.WrappedComponent isOpen closeLoginModal={closeFn} />);
      const signinDialog = wrapper.find('SigninDialog');
      signinDialog.prop('onRedirect')();
      signinDialog.prop('onRedirect')();
      expect(closeFn).toHaveBeenCalledTimes(2);
    });

    it('redirects the user to the login page if the the isOpen flag is raised from a subdomain', () => {
      const ticketAction = 'SOME-ACTION';
      const ticketPayload = { someData: 'someValue' };
      const loginData = { ticketAction, payload: ticketPayload };
      const theState = {
        ...defaultState,
        connectionInfo: {
          ...defaultState.connectionInfo,
          subdomain: 'somesubdomain',
          host: 'somesubdomain.glassyboard.com',
        },
      };
      const loginState = { ...defaultLoginState, isOpen: true, data: loginData };
      setUp({ loginState, initialState: theState, doMount: true });
      expect(window.location.replace).toHaveBeenNthCalledWith(
        1,
        `${baseUrl}/login?data=${encodeURIComponent(JSON.stringify(loginData))}`
      );
    });
  });
});
