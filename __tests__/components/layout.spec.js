import { shallow } from 'enzyme';
import { useRouter } from 'next/router';
import Layout from '../../components/layout';
import { findByTestAttr, checkProps } from '../../utils/test-utils';

jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}));

const setUp = (props = {}, { pathname = '/' } = {}) => {
  useRouter.mockReturnValue({ pathname });
  return shallow(<Layout {...props} />);
};

describe('<Layout />', () => {
  const sampleCompDataTestId = `some-sample-component-${Math.floor(3000 * Math.random())}`;
  const SampleComp = () => (
    <div data-testid={sampleCompDataTestId}>
      {`sample component ${Math.floor(5000 * Math.random())}`}
    </div>
  );

  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        children: <SampleComp />,
      };
      const propTypeErrors = checkProps(Layout, expectedProps);
      expect(propTypeErrors).toBeUndefined();
    });

    it('throws warning if wrong props are passed', () => {
      const expectedProps = {
        children: 1,
      };
      const propTypeErrors = checkProps(Layout, expectedProps);
      expect(propTypeErrors).toBeDefined();
    });
  });

  describe('render', () => {
    it('renders without error', () => {
      const wrapper = setUp();
      expect(wrapper).toBeDefined();
    });

    it('renders correctly', () => {
      const wrapper = setUp({ children: <SampleComp /> });
      expect(wrapper).toMatchSnapshot();
    });

    it('renders the top navbar', () => {
      const wrapper = setUp();
      const navbar = findByTestAttr(wrapper, 'top-navbar');
      expect(navbar).toHaveLength(1);
    });

    it('does not render the TopNavbar when the path is in its noNavbarList prop', () => {
      const wrapper = setUp({ noNavbarList: ['/some/path'] }, { pathname: '/some/path' });
      const navbar = findByTestAttr(wrapper, 'top-navbar');
      expect(navbar).toHaveLength(0);
    });

    it('renders the child component passed to it', () => {
      const wrapper = shallow(
        <Layout>
          <SampleComp />
        </Layout>
      );
      const sampleComp = wrapper.childAt(1).dive();
      const sampleContainer = findByTestAttr(sampleComp, sampleCompDataTestId);
      expect(sampleContainer).toHaveLength(1);
    });
  });
});
