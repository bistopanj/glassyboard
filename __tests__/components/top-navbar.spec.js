import configureMockStore from 'redux-mock-store';
import { shallow } from 'enzyme';
import { middlewares } from '../../build-store';
import TopNavbar from '../../components/top-navbar';
import { findByTestAttr, checkProps } from '../../utils/test-utils';
import { initialState as connectionInfoInitialState } from '../../reducers/connection-info-reducer';
import { actionTypes } from '../../actions';

const mockStore = configureMockStore(middlewares);

const initState = {
  boards: {},
  connectionInfo: connectionInfoInitialState,
  login: {},
};

const setUp = ({ initialState = initState } = {}) => {
  const store = mockStore(initialState);
  const wrapper = shallow(<TopNavbar store={store} />)
    .childAt(0)
    .dive();

  return [wrapper, store];
};

describe('<TopNavbar />', () => {
  const sampleEmail = 'test@email.com';
  const loggedInState = {
    ...initState,
    connectionInfo: { token: '123', user: { email: sampleEmail } },
  };

  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        isAuthenticated: true,
        user: {
          id: '123123',
          firstName: 'someFirstName',
          lastName: 'someLastName',
          createdAt: '2020-03-09T00:01:00.515Z',
          email: 'some@email.com',
        },
        login: () => {
          const aaa = 1;
          return aaa;
        },
        logout: () => {
          const bbb = 1;
          return bbb;
        },
      };
      const propsErrors = checkProps(TopNavbar.WrappedComponent, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appreance', () => {
    it('renders without error', () => {
      const [wrapper] = setUp();
      const container = findByTestAttr(wrapper, 'navbar-container');
      expect(container).toHaveLength(1);
    });

    it('shows the brand name', () => {
      const [wrapper] = setUp();
      const brand = findByTestAttr(wrapper, 'brand-name');
      expect(brand).toHaveLength(1);
    });

    it('shows a login link when the user is not logged in', () => {
      const [wrapper] = setUp();
      const login = findByTestAttr(wrapper, 'login');
      expect(login).toHaveLength(1);
    });

    it('shows user email when the user is logged in', () => {
      const [wrapper] = setUp({ initialState: loggedInState });
      const email = findByTestAttr(wrapper, 'user-email');
      expect(email.text()).toBe(sampleEmail);
    });

    it('renders a logout item when the user is logged in', () => {
      const [wrapper] = setUp({ initialState: loggedInState });
      const logout = findByTestAttr(wrapper, 'logout');
      expect(logout).toHaveLength(1);
    });

    it('does not show the login button when the user is logged in', () => {
      const [wrapper] = setUp({ initialState: loggedInState });
      const login = findByTestAttr(wrapper, 'login');
      expect(login).toHaveLength(0);
    });

    it('does not show the logout button when the user is not logged in', () => {
      const [wrapper] = setUp();
      const logout = findByTestAttr(wrapper, 'logout');
      expect(logout).toHaveLength(0);
    });

    it('renders correctly', () => {
      const [wrapper] = setUp();
      expect(wrapper).toMatchSnapshot();
    });
  });
});
