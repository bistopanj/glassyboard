import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import { findByTestAttr, tick } from '../../utils/test-utils';
import { middlewares } from '../../build-store';
import AddCompanyForm from '../../components/add-company-form';
import { initialState as inState } from '../../reducers';
import addCompany from '../../actions/add-company';
import { actionTypes } from '../../actions';

jest.mock('../../actions/add-company');
jest.mock('next/router');

const mockStore = configureMockStore(middlewares);

const setUp = ({ initialState = inState } = {}) => {
  const store = mockStore(initialState);
  const wrapper = mount(
    <Provider store={store}>
      <AddCompanyForm />
    </Provider>
  );

  return [wrapper, store];
};

const addSubdomain = async (wrapper, newSubdomain) => {
  const textInput = wrapper.find('input[data-testid="new-company-subdomain-input"]');
  const form = findByTestAttr(wrapper, 'add-company-form').first();
  textInput.instance().value = newSubdomain;
  textInput.simulate('change', { target: { value: newSubdomain } });
  form.simulate('submit');
  await tick();
};

describe('<AddCompanyForm />', () => {
  let wrapper;
  const { location } = window;

  beforeAll(() => {
    delete window.location;
    window.location = { replace: jest.fn() };
  });

  afterAll(() => {
    window.location = location;
  });

  describe('appearance', () => {
    beforeEach(() => {
      jest.resetAllMocks();
      const [w] = setUp();
      wrapper = w;
    });

    it('renders without errors', () => {
      const container = findByTestAttr(wrapper, 'add-company-form-container');
      expect(container).toHaveLength(1);
    });

    it('renders a text input for entering subdomain', () => {
      const subdomainInput = findByTestAttr(wrapper, 'new-company-subdomain-input', 'input');
      expect(subdomainInput).toHaveLength(1);
    });

    it('renders a button for submitting the subdomain', () => {
      const addButton = findByTestAttr(wrapper, 'add-new-company-btn', 'button');
      expect(addButton).toHaveLength(1);
    });
  });

  describe('behavior', () => {
    let sampleSubdomain;

    beforeEach(() => {
      jest.resetAllMocks();
      sampleSubdomain = `some-sub-domain-${Math.floor(10000 * Math.random())}`;
    });

    it('has disabled submit button at initial render', () => {
      const [w] = setUp();
      const button = findByTestAttr(w, 'add-new-company-btn', 'button');
      expect(button.getDOMNode()).toBeDisabled();
    });

    it('enables the submit button when a valid subdomain is entered', () => {
      const [w] = setUp();
      const textInput = w.find('input[data-testid="new-company-subdomain-input"]');
      textInput.instance().value = sampleSubdomain;
      textInput.simulate('change', { target: { value: sampleSubdomain } });
      const button = findByTestAttr(w, 'add-new-company-btn', 'button');
      expect(button.getDOMNode()).toBeEnabled();
    });

    it('disables the submit button when an invalid subdomain name is entered', () => {
      const invalidValue = 'hi dude';
      const [w] = setUp();
      const textInput = w.find('input[data-testid="new-company-subdomain-input"]');
      textInput.instance().value = invalidValue;
      textInput.simulate('change', { target: { value: invalidValue } });
      const button = findByTestAttr(w, 'add-new-company-btn', 'button');
      expect(button.getDOMNode()).toBeDisabled();
    });

    it('dispatches the addCompany action with current parameters when the submit button is pressed', async () => {
      expect.assertions(2);
      const someAction = { type: 'SOME_ACTION', payload: { someData: 'its value' } };
      const [w, store] = setUp();
      wrapper = w;
      addCompany.mockReturnValue(dispatch => dispatch(someAction));
      const expectedActions = [someAction];
      await addSubdomain(wrapper, sampleSubdomain);
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
      expect(addCompany).toHaveBeenNthCalledWith(
        1,
        {
          user: null,
          token: null,
          isAuthenticated: false,
        },
        { subdomain: sampleSubdomain }
      );
    });

    it('clears the subdomain input when the submit button is pressed', async () => {
      expect.assertions(1);
      const someAction = { type: 'SOME_ACTION', payload: { someData: 'its value' } };
      const [w] = setUp();
      wrapper = w;
      addCompany.mockReturnValue(dispatch => dispatch(someAction));
      await addSubdomain(wrapper, sampleSubdomain);

      const textInput = findByTestAttr(wrapper, 'new-company-subdomain-input', 'input');
      expect(textInput.instance().value).toBe('');
    });

    it('renders a loading spinner instead of the submit button when the new company state is loading', () => {
      const state1 = {
        ...inState,
        company: {
          ...inState.company,
          new: { ...inState.company.new, loading: true },
        },
      };
      const [w] = setUp({ initialState: state1 });
      wrapper = w;
      const loadingSpinner = findByTestAttr(wrapper, 'loading-spinner');
      const submitButton = findByTestAttr(wrapper, 'add-new-company-btn', 'button');
      expect(loadingSpinner.length).toBeGreaterThan(0);
      expect(submitButton).toHaveLength(0);
    });

    it('dispatches the correct action to reset the new company state when the process is successful', () => {
      const companyData = {
        id: '3e285d01-f0ad-4ff6-a712-df69c5b2b3cf',
        subdomain: 'subdomain',
        primary_board: 'dd268330-c1c9-48a5-841f-eba2eb67e11a',
        created_at: '2020-04-16T15:48:39.626Z',
        updated_at: '2020-04-16T15:48:39.643Z',
        url: 'http://subdomain.glassyboard.com/',
        board_slug: 'default-company-board-subdomain-123123',
      };
      const state = {
        ...inState,
        company: {
          ...inState.company,
          new: { ...inState.company.new, success: true },
          data: companyData,
        },
      };
      const expectedActions = [{ type: actionTypes.addCompanyReset }];
      const [, store] = setUp({ initialState: state });
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });

    it('redirects to the subdomain when the process is successful', () => {
      const companyData = {
        id: '3e285d01-f0ad-4ff6-a712-df69c5b2b3cf',
        subdomain: 'subdomain',
        primary_board: 'dd268330-c1c9-48a5-841f-eba2eb67e11a',
        created_at: '2020-04-16T15:48:39.626Z',
        updated_at: '2020-04-16T15:48:39.643Z',
        url: 'http://subdomain.glassyboard.com/',
        board_slug: 'default-company-board-subdomain-123123',
      };
      const state = {
        ...inState,
        company: {
          ...inState.company,
          new: { ...inState.company.new, success: true },
          data: companyData,
        },
      };
      setUp({ initialState: state });
      expect(window.location.replace).toHaveBeenNthCalledWith(1, companyData.url);
    });
  });
});
