import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import NewAnswerCard from '../../components/new-answer-card';
import buildStore, { middlewares } from '../../build-store';
import { findByTestAttr, tick, buildMockResponse } from '../../utils/test-utils';
import { initialState as boardsInitialState } from '../../reducers/boards-reducer';
import { initialState as loginInitialState } from '../../reducers/login-reducer';
import { initialState as connectionInfoInitialState } from '../../reducers/connection-info-reducer';
import { actionTypes } from '../../actions';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);

const inState = {
  boards: boardsInitialState,
  connectionInfo: connectionInfoInitialState,
  login: loginInitialState,
};

const sampleEmail = 'test@email.com';
const loggedInState = {
  ...inState,
  connectionInfo: { token: '123', user: { email: sampleEmail } },
};

const defaultSlug = `some-slug-${Math.floor(1000 * Math.random())}`;
const id = '2c140813-93ed-48fc-b92a-866a78c8a53d';

const setUp = (initialState = inState, props = { slug: defaultSlug, id }, store) => {
  let theStore = store;
  if (!theStore) theStore = mockStore(initialState);
  const wrapper = mount(
    <Provider store={theStore}>
      <NewAnswerCard {...props} />
    </Provider>
  );

  return [wrapper, theStore];
};

const addAnswer = async (wrapper, newAnswer) => {
  const textInput = findByTestAttr(wrapper, 'add-answer-to-board-input', 'input');
  const form = findByTestAttr(wrapper, 'new-answer-form', 'Form');
  textInput.instance().value = newAnswer;
  textInput.simulate('change', { target: { value: newAnswer } });
  form.simulate('submit');
  await tick();
};

const typeCharacter = (wrapper, char) => {
  const textInput = findByTestAttr(wrapper, 'add-answer-to-board-input', 'input');
  const oldChar = textInput.instance().value;
  const newVal = `${oldChar}${char}`;
  textInput.instance().value = newVal;
  textInput.simulate('change', { target: { value: newVal } });
  wrapper.update();
};

describe('<NewAnswerCard />', () => {
  const slug = `some-slug-${Math.floor(1000 * Math.random())}`;

  describe('appearance', () => {
    it('renders without error', () => {
      const [wrapper] = setUp();
      const container = findByTestAttr(wrapper, 'new-answer-form', 'form');
      expect(container).toHaveLength(1);
    });

    it('renders an input field to enter the new answer', () => {
      const [wrapper] = setUp();
      const input = findByTestAttr(wrapper, 'add-answer-to-board-input', 'input');
      expect(input).toHaveLength(1);
    });

    it('renders a button for submitting the new answer', () => {
      const [wrapper] = setUp();
      const addButton = findByTestAttr(wrapper, 'add-answer-btn-wrapper');
      expect(addButton).toHaveLength(1);
    });
  });

  describe('behavior', () => {
    it('dispatches the correct ADD_ANSWER actions when the add button is pressed', async () => {
      expect.assertions(1);
      const newAnswer = `some topic ${Math.floor(1000 * Math.random())}`;
      const [wrapper, store] = setUp(loggedInState, { slug, id });
      const serverAnswer = {
        id: '12345-6789',
        answer: newAnswer,
        boardId: id,
        createdBy: '12345-6789-012345',
      };
      const expectedActions = [
        { type: actionTypes.addAnswerLoading, payload: slug },
        { type: actionTypes.addAnswerSuccess, payload: { answer: serverAnswer, slug } },
      ];
      fetch.doMockIf(`${baseUrl}/api/answers`, buildMockResponse(serverAnswer));
      await addAnswer(wrapper, newAnswer);
      const dispatchedActions = store.getActions();

      expect(dispatchedActions).toEqual(expectedActions);
    });

    it('dispatches a ADD_ANSWER_RESET action when the answer is added successfully', () => {
      const loadingState = {
        ...loggedInState,
        boards: {
          bySlug: { [slug]: { newAnswer: { loading: false, success: true, error: null } } },
        },
      };

      const expectedActions = [{ type: 'ADD_ANSWER_RESET', payload: slug }];

      const [, store] = setUp(loadingState, { slug, id });
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });

    it('disables the add button until at least 5 characters are typed', () => {
      const [wrapper] = setUp();
      let button;
      const inputs = ['1', '2', '3', '4', '5', '6', '7', '8'];
      inputs.forEach((char, index) => {
        typeCharacter(wrapper, char);
        button = findByTestAttr(wrapper, 'add-answer-btn', 'button');
        expect(button.prop('disabled')).toBe(index < 4);
      });
    });

    it('clears the input text after the new answer is successfully submitted', async () => {
      expect.assertions(1);
      const loadingState = {
        ...loggedInState,
        boards: {
          bySlug: { [slug]: { newAnswer: { loading: true } } },
        },
      };
      const simpleReducer = (state, action) => {
        switch (action.type) {
          case 'SAMPLE_ACTION':
            return {
              ...loggedInState,
              boards: {
                bySlug: { [slug]: { newAnswer: { loading: false, success: true, error: null } } },
              },
            };
          default:
            return state;
        }
      };
      const theStore = buildStore(loadingState);
      theStore.replaceReducer(simpleReducer);
      const [wrapper, store] = setUp(loadingState, { slug, id }, theStore);
      const textInput = findByTestAttr(wrapper, 'add-answer-to-board-input', 'input');
      const newAnswer = 'some-text';
      textInput.instance().value = newAnswer;
      textInput.simulate('change', { target: { value: newAnswer } });
      await store.dispatch({ type: 'SAMPLE_ACTION' });
      await tick();

      expect(textInput.instance().value).toHaveLength(0);
    });

    it('passes the loading variable from store to the add button', () => {
      const loadingState = {
        ...loggedInState,
        boards: { bySlug: { [slug]: { newAnswer: { loading: true } } } },
      };
      const [wrapper] = setUp(loadingState, { slug, id });
      const button = findByTestAttr(wrapper, 'add-answer-btn-wrapper', 'AsyncButton');
      expect(button.prop('loading')).toBe(true);
      const notLoadingState = {
        ...loggedInState,
        boards: { bySlug: { [slug]: { newAnswer: { loading: false } } } },
      };
      const [wrapper2] = setUp(notLoadingState, { slug, id });
      const button2 = findByTestAttr(wrapper2, 'add-answer-btn-wrapper', 'AsyncButton');
      expect(button2.prop('loading')).toBe(false);
    });
  });
});
