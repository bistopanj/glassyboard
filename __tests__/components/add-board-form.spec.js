import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import Router from 'next/router';
import { mount } from 'enzyme';
import AddBoardForm from '../../components/add-board-form';
import { findByTestAttr, tick, buildMockResponse } from '../../utils/test-utils';
import { middlewares } from '../../build-store';
import { initialState as boardsInitialState } from '../../reducers/boards-reducer';
import { initialState as loginInitialState } from '../../reducers/login-reducer';
import { initialState as connectionInfoInitialState } from '../../reducers/connection-info-reducer';
import { actionTypes } from '../../actions';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);

jest.mock('next/router');

const inState = {
  boards: boardsInitialState,
  connectionInfo: connectionInfoInitialState,
  login: loginInitialState,
};

const setUp = (initialState = inState) => {
  const store = mockStore(initialState);
  const wrapper = mount(
    <Provider store={store}>
      <AddBoardForm />
    </Provider>
  );
  return [wrapper, store];
};

const addTopic = async (wrapper, newTopic) => {
  const textInput = wrapper.find('input[data-testid="new-board-topic-input"]');
  const form = findByTestAttr(wrapper, 'add-board-form').first();
  textInput.instance().value = newTopic;
  textInput.simulate('change', { target: { value: newTopic } });
  form.simulate('submit');
  await tick();
};

describe('<AddBoardForm />', () => {
  const sampleEmail = 'test@email.com';
  const loggedInState = {
    ...inState,
    connectionInfo: { token: '123', user: { email: sampleEmail } },
  };
  let wrapper;
  describe('appearance', () => {
    beforeEach(() => {
      jest.resetAllMocks();
      Router.push = jest.fn();
      const [w] = setUp();
      wrapper = w;
    });

    it('renders without errors', () => {
      const container = findByTestAttr(wrapper, 'add-board-form-container');
      expect(container).toHaveLength(1);
    });

    it('renders with correct styles', () => {
      expect(wrapper).toMatchSnapshot();
    });

    it('renders a text input for entering form topic', () => {
      const topicInput = wrapper.find('Input[data-testid="new-board-topic-input"]');
      expect(topicInput).toHaveLength(1);
    });

    it('renders a button for submitting the text input', () => {
      const addButton = wrapper.find('button[data-testid="add-new-topic-btn"]');
      expect(addButton).toHaveLength(1);
    });
  });

  describe('behavior', () => {
    let sampleTopic;

    beforeEach(() => {
      jest.resetAllMocks();
      fetch.resetMocks();
      sampleTopic = `some topic ${Math.floor(1000 * Math.random())}`;
    });

    it('redirects to the board page and dispatches an action to reset the new board state when the process is successful', () => {
      const newBoardSlug = `some-board-slug-${Math.floor(1000 * Math.random())}`;
      const state1 = {
        ...inState,
        boards: { ...inState.boards, new: { ...inState.boards.new, success: newBoardSlug } },
      };
      const [w, store] = setUp(state1);
      wrapper = w;
      const dispatchedActions = store.getActions();
      const expectedActions = [{ type: actionTypes.addBoardReset }];
      expect(dispatchedActions).toEqual(expectedActions);
      expect(Router.push).toHaveBeenCalledTimes(1, '/board/[slug]', `/board/${newBoardSlug}`);
    });

    it('renders the loading spinner instead of the submit button when the new board state is loading', () => {
      const state1 = {
        ...inState,
        boards: { ...inState.boards, new: { ...inState.boards.new, loading: true } },
      };
      const [w] = setUp(state1);
      wrapper = w;
      const loadingSpinner = findByTestAttr(wrapper, 'loading-spinner');
      const addButton = findByTestAttr(wrapper, 'add-new-topic-btn');
      expect(loadingSpinner.length).toBeGreaterThan(0);
      expect(addButton).toHaveLength(0);
    });

    it('dispatches correct actions when the add topic button is pressed (not logged in)', async () => {
      expect.assertions(1);
      const [w, store] = setUp();
      wrapper = w;
      const expectedActions = [
        {
          type: actionTypes.openLogin,
          payload: { ticketAction: 'ADD-BOARD', payload: { topic: sampleTopic } },
        },
      ];
      await addTopic(wrapper, sampleTopic);
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });

    it('dispatches correct action when the add topic button is pressed (logged in)', async () => {
      expect.assertions(1);
      const [w, store] = setUp(loggedInState);
      wrapper = w;
      const newBoard = {
        topic: sampleTopic,
        id: '1234-5678',
        createdBy: '123123-12312-asdasd',
        slug: 'some-unique-slug',
      };
      const expectedActions = [
        { type: actionTypes.addBoardLoading },
        { type: actionTypes.addBoardSuccess, payload: newBoard },
      ];
      fetch.doMockIf(`${baseUrl}/api/boards`, buildMockResponse(newBoard));
      await addTopic(wrapper, sampleTopic);
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });

    it('clears the topic input when the add topic button is pressed', async () => {
      expect.assertions(1);
      const [w] = setUp(loggedInState);
      wrapper = w;
      const newBoard = {
        topic: sampleTopic,
        id: '1234-5678',
        createdBy: '123123-12312-asdasd',
        slug: 'some-unique-slug',
      };
      fetch.doMockIf(`${baseUrl}/api/boards`, buildMockResponse(newBoard));
      await addTopic(wrapper, sampleTopic);
      const textInput = wrapper.find('input[data-testid="new-board-topic-input"]');
      expect(textInput.instance().value).toBe('');
    });
  });
});
