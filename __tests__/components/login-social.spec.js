import { shallow, mount } from 'enzyme';
import { useRouter } from 'next/router';
import LoginSocial from '../../components/login-social';
import { findByTestAttr, checkProps } from '../../utils/test-utils';

jest.mock('next/router');

const setUp = () => shallow(<LoginSocial />);

describe('<LoginSocial />', () => {
  const routerPush = jest.fn();
  useRouter.mockReturnValue({ push: routerPush });
  describe('PropTypes', () => {
    it('renders without warning', () => {
      const expectedProps = {
        redirectTo: () => {
          const someVar = 1;
          return someVar;
        },
        data: {
          ticketAction: 'SOME-ACTION',
          payload: {
            anyData: 1,
          },
        },
      };
      const propsErrors = checkProps(LoginSocial, expectedProps);
      expect(propsErrors).toBeUndefined();
    });
  });

  describe('appearance', () => {
    it('renders without error', () => {
      const wrapper = setUp();
      const container = findByTestAttr(wrapper, 'login-social-container');
      expect(container).toHaveLength(1);
    });

    it('renders a button to sign in with google', () => {
      const wrapper = setUp();
      const googleButtonContainer = wrapper.find('#google-sign-in');
      expect(googleButtonContainer).toHaveLength(1);
    });

    it('renders correctly', () => {
      const wrapper = setUp();
      expect(wrapper).toMatchSnapshot();
    });
  });

  describe('behavior', () => {
    const { gapi } = window;

    beforeAll(() => {
      delete window.gapi;
      window.gapi = {
        signin2: {
          render: jest.fn(),
        },
        dispatchSuccess(gUser) {
          window.gapi.onSuccess(gUser);
        },
        dispatchFailure(error) {
          window.gapi.onFailure(error);
        },
        onSucces: () => ({}),
        onFailure: () => ({}),
      };
      window.gapi.signin2.render.mockImplementation((elementId, { onsuccess, onfailure }) => {
        window.gapi.onSuccess = onsuccess;
        window.gapi.onFailure = onfailure;
      });
    });

    afterAll(() => {
      jest.resetAllMocks();
      window.gapi = gapi;
    });

    beforeEach(() => {
      jest.clearAllMocks();
    });

    it('renders the google button after mounting', () => {
      mount(<LoginSocial />);
      expect(window.gapi.signin2.render).toHaveBeenCalledTimes(1);
    });

    it('calls its props.redirectTo method with appropriate path when the sign in process is done successfully', () => {
      const googleIdToken = `some.token.${Math.floor(10000 * Math.random())}`;
      const data = { ticketAction: 'SOME-ACTION', payload: { someData: 'someData-1' } };
      mount(<LoginSocial data={data} />);
      window.gapi.dispatchSuccess({
        getAuthResponse: () => ({ id_token: googleIdToken }),
      });
      expect(routerPush).toHaveBeenNthCalledWith(
        1,
        `/auth/google?token=${googleIdToken}&data=${encodeURIComponent(JSON.stringify(data))}`
      );
    });
  });
});
