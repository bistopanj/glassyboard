import { shallow } from 'enzyme';
import { useDispatch } from 'react-redux';
import AnswerCard from '../../components/answer-card';
import { findByTestAttr } from '../../utils/test-utils';
import removeAnswerAction from '../../actions/remove-answer';

jest.mock('react-redux');
jest.mock('../../actions/remove-answer');

const defaultProps = {
  boardId: '123',
  answerId: 'abc.def',
  answerText: 'answer text',
  upvotes: 10,
  upvoted: false,
  slug: 'some-slug',
  editable: false,
};

describe('<AnswerCard />', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('appearance', () => {
    it('renders without error', () => {
      const wrapper = shallow(<AnswerCard {...defaultProps} />);
      const container = findByTestAttr(wrapper, 'answer');
      expect(container).toHaveLength(1);
    });

    it('renders the answer text', () => {
      const randomText = `some answer ${Math.floor(1000 * Math.random())}`;
      const props = { ...defaultProps, answerText: randomText };
      const wrapper = shallow(<AnswerCard {...props} />);
      const answerText = findByTestAttr(wrapper, 'answer-text');
      expect(answerText.text()).toBe(randomText);
    });

    it('renders an upvote button', () => {
      const wrapper = shallow(<AnswerCard {...defaultProps} />);
      const upvoteButton = wrapper.find('UpvoteButton');
      expect(upvoteButton).toHaveLength(1);
    });
  });

  describe('behavior', () => {
    it('renders a delete button when its editable prop is true', () => {
      const props = { ...defaultProps, editable: true };
      const wrapper = shallow(<AnswerCard {...props} />);
      const removeButton = findByTestAttr(wrapper, 'remove-answer-button');
      expect(removeButton).toHaveLength(1);
    });

    it('does not render a delete button when its editable prop is false', () => {
      const props = { ...defaultProps, editable: false };
      const wrapper = shallow(<AnswerCard {...props} />);
      const removeButton = findByTestAttr(wrapper, 'remove-answer-button');
      expect(removeButton).toHaveLength(0);
    });

    it('dispatches an appropriate action when the remove button is pressed', () => {
      const dispatch = jest.fn();
      const sampelAction = { type: 'SOME-ACTION', payload: { someData: 'someValue' } };
      useDispatch.mockImplementation(() => dispatch);
      removeAnswerAction.mockReturnValue(sampelAction);
      const props = { ...defaultProps, editable: true };
      const wrapper = shallow(<AnswerCard {...props} />);
      const removeButton = findByTestAttr(wrapper, 'remove-answer-button');
      removeButton.simulate('click');
      expect(dispatch).toHaveBeenNthCalledWith(1, sampelAction);
      expect(removeAnswerAction).toHaveBeenNthCalledWith(
        1,
        defaultProps.answerId,
        defaultProps.boardId,
        defaultProps.slug
      );
    });
  });
});
