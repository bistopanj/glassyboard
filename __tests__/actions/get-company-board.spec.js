import configureMockStore from 'redux-mock-store';
import { middlewares } from '../../build-store';
import { buildMockResponse } from '../../utils/test-utils';
import getCompanyBoard from '../../actions/get-company-board';
import { actionTypes } from '../../actions';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);

describe('actions.getCompanyBoard(subdomain, {abortSignal, cookie,  pathInfo})', () => {
  const store = mockStore({});
  const subdomain = `some-subdomain-${Math.floor(10000 * Math.random())}`;
  const cookie = `someCOOKIE`;

  const serverResponse = {
    id: 'e4e24eb9-583d-47c9-93b3-f2e64e231d3f',
    editable: false,
    slug: 'some-slug',
    topic: 'the topic',
    created_at: '2020-03-16T05:04:07.111Z',
    updated_at: '2020-03-16T05:04:07.111Z',
    answers: [
      {
        id: '667086e7-7fa0-407e-b587-428ab0997212',
        editable: false,
        answer: 'first answer',
        created_at: '2020-03-17T15:26:58.230169+03:30',
        updated_at: '2020-03-17T15:26:58.230169+03:30',
      },
      {
        id: '53dc4074-4bb6-458b-b1b1-549e7c774372',
        answer: 'another answer',
        editable: true,
        created_at: '2020-03-17T16:17:55.328538+03:30',
        updated_at: '2020-03-17T16:17:55.328538+03:30',
      },
    ],
  };

  beforeEach(() => {
    fetch.resetMocks();
    store.clearActions();
  });

  it('sends a GET request to /api/boards with appropriate query info', async () => {
    expect.assertions(2);
    fetch.doMockIf(
      `${baseUrl}/api/boards?subdomain=${encodeURIComponent(subdomain)}`,
      buildMockResponse(serverResponse)
    );
    await store.dispatch(getCompanyBoard(subdomain, { cookie }));
    expect(fetch).toHaveBeenCalledTimes(1);
    const apiReq = fetch.mock.calls[0][1];
    expect(apiReq.method).toBe('GET');
  });

  it('dispatches the correct action when the process is successful', async () => {
    expect.assertions(1);
    const expectedActions = [
      { type: actionTypes.getCompanyBoardLoading },
      { type: actionTypes.getCompanyBoardSuccess, payload: { ...serverResponse } },
    ];
    fetch.doMockIf(
      `${baseUrl}/api/boards?subdomain=${encodeURIComponent(subdomain)}`,
      buildMockResponse(serverResponse)
    );
    await store.dispatch(getCompanyBoard(subdomain, { cookie }));
    const dispatchedActions = store.getActions();
    expect(dispatchedActions).toEqual(expectedActions);
  });
});
