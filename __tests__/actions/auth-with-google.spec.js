import configureMockStore from 'redux-mock-store';
import { middlewares } from '../../build-store';
import { buildMockResponse } from '../../utils/test-utils';
import { authWithGoogle, actionTypes } from '../../actions';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);

describe('actions.authWithGoogle()', () => {
  const store = mockStore({});
  const googleToken = 'abcd.ef12.4567';
  const data = { ticketAction: 'SOME-ACTION', payload: { someData: 'its value' } };
  const serverResponse = {
    token: `some.token.${Math.floor(1000 * Math.random())}`,
    redirectTo: `/some/path`,
  };

  beforeEach(() => {
    fetch.resetMocks();
    store.clearActions();
  });

  it('sends a POST request to the /api/auth/google endpoint with appropriate data', async () => {
    expect.assertions(3);
    fetch.doMockIf(`${baseUrl}/api/auth/google`, buildMockResponse(serverResponse));
    await store.dispatch(authWithGoogle({ googleToken, data }));
    const apiReq = fetch.mock.calls[0][1];
    expect(apiReq.method).toBe('POST');
    expect(apiReq.body).toBe(JSON.stringify({ googleToken, data }));
    expect(fetch).toHaveBeenCalledTimes(1);
  });

  it('dispatches correct actions in the auth process when the authentication is successful', async () => {
    expect.assertions(1);
    const expectedActions = [
      { type: actionTypes.authWithGoogleLoading },
      { type: actionTypes.authWithGoogleSuccess, payload: serverResponse },
    ];
    fetch.doMockIf(`${baseUrl}/api/auth/google`, buildMockResponse(serverResponse));
    await store.dispatch(authWithGoogle(googleToken, data));
    const dispatchedActions = store.getActions();
    expect(dispatchedActions).toEqual(expectedActions);
  });
});
