import configureMockStore from 'redux-mock-store';
import * as actions from '../../actions';
import actionTypes from '../../actions/types';
import { middlewares } from '../../build-store';
import { buildMockResponse } from '../../utils/test-utils';
import ApiError from '../../errors/api-error';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);

describe('actions.login', () => {
  const loginInfo = {
    email: 'someemail@mailserver.com',
    ticketInfo: { answerId: 'abcd', boardId: '1234', ticketAction: 'UPVOTE' },
  };

  describe('success response from server', () => {
    beforeEach(() => {
      fetch.resetMocks();
    });

    it('creates one LOGIN_LOADING at start and one LOGIN_SUCCESS when a success response is received from server', async () => {
      expect.assertions(1);
      const theResponse = {
        done: true,
      };
      fetch.doMockIf(`${baseUrl}/api/auth/tickets`, buildMockResponse(theResponse));
      const expectedActions = [
        { type: actionTypes.loginLoading },
        { type: actionTypes.loginSuccess },
      ];
      const store = mockStore({ login: {} });
      await store.dispatch(actions.login(loginInfo));
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });
  });

  describe('error response from server', () => {
    const errorData = {
      code: 1001,
      message: 'something went wrong',
    };
    beforeEach(() => {
      fetch.resetMocks();
    });

    it('creates one LOGIN_LOADING at start and one LOGIN_FAILED when error received', async () => {
      expect.assertions(1);
      fetch.doMockIf(`${baseUrl}/api/auth/tickets`, buildMockResponse(errorData, { status: 500 }));
      const expectedActions = [
        { type: actionTypes.loginLoading },
        {
          type: actionTypes.loginFailed,
          payload: new ApiError({ ...errorData, status: 500 }).toJSON(),
        },
      ];
      const store = mockStore({ login: {} });
      await store.dispatch(actions.login(loginInfo));
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });
  });
});
