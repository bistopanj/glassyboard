import configureMockStore from 'redux-mock-store';
import { middlewares } from '../../build-store';
import { buildMockResponse } from '../../utils/test-utils';
import { actionTypes, askTicket } from '../../actions';
import { initialState as defaultState } from '../../reducers';
import ApiError from '../../errors/api-error';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);

describe('actions.askTicket()', () => {
  const sampleEmail = 'sample@email.com';
  const loginInfo = {
    email: sampleEmail,
    ticketInfo: {
      ticketAction: 'SOME-ACTION',
      payload: {
        someData: 'value',
      },
    },
  };
  const serverResponse = { done: true };
  const store = mockStore(defaultState);

  beforeEach(() => {
    fetch.resetMocks();
    store.clearActions();
  });

  describe('success response from server', () => {
    it('dispatches appropriate actions at start and when the success response is received from server', async () => {
      expect.assertions(1);
      const expectedActions = [
        { type: actionTypes.askTicketLoading },
        { type: actionTypes.askTicketSuccess },
      ];
      fetch.doMockIf(`${baseUrl}/api/tickets`, buildMockResponse(serverResponse));
      await store.dispatch(askTicket(loginInfo));
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });
  });

  describe('error response from server', () => {
    const errorData = {
      code: 1001,
      message: 'something went wrong',
    };

    it('dispatches appropriate actions at start and when the error response is received from the server', async () => {
      expect.assertions(1);
      fetch.doMockIf(`${baseUrl}/api/tickets`, buildMockResponse(errorData, { status: 500 }));
      const expectedActions = [
        { type: actionTypes.askTicketLoading },
        {
          type: actionTypes.askTicketFailed,
          payload: new ApiError({ ...errorData, status: 500 }).toJSON(),
        },
      ];
      await store.dispatch(askTicket(loginInfo));
      const dispatchedActions = store.getActions();
      expect(dispatchedActions).toEqual(expectedActions);
    });
  });
});
