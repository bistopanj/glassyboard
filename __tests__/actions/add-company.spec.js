import configureMockStore from 'redux-mock-store';
import addCompany from '../../actions/add-company';
import { actionTypes } from '../../actions';
import { middlewares } from '../../build-store';
import { buildMockResponse } from '../../utils/test-utils';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);

describe('actions.addCompany(userInfo, { subdomain })', () => {
  const store = mockStore({});
  const subdomain = `some-subdomain-${Math.floor(10000 * Math.random())}`;
  const notLoggedInUserInfo = { user: null, token: null, isAuthenticated: false };
  const loggedInUserInfo = { user: { id: '123' }, token: 'some-token', isAuthenticated: true };
  const serverResponse = {
    id: 'acasd.asdasd',
    subdomain: subdomain,
    created_by: 'user.id',
    created_at: 'some date',
    updated_at: 'some date',
  };

  beforeEach(() => {
    fetch.resetMocks();
    store.clearActions();
  });

  it('is protected', () => {
    const expectedAction = {
      type: actionTypes.openLogin,
      payload: { ticketAction: 'ADD-COMPANY', payload: { subdomain } },
    };
    const action = addCompany(notLoggedInUserInfo, { subdomain });
    expect(action).toEqual(expectedAction);
  });

  it('sends a POST request to /api/companies with correct data', async () => {
    expect.assertions(3);
    fetch.doMockIf(`${baseUrl}/api/companies`, buildMockResponse(serverResponse));
    await store.dispatch(addCompany(loggedInUserInfo, { subdomain }));
    expect(fetch).toHaveBeenCalledTimes(1);
    const apiReq = fetch.mock.calls[0][1];
    expect(apiReq.method).toBe('POST');
    expect(apiReq.body).toBe(JSON.stringify({ subdomain }));
  });

  it('dispatches correct actions when the process is successful', async () => {
    expect.assertions(1);
    const expectedActions = [
      { type: actionTypes.addCompanyLoading },
      { type: actionTypes.addCompanySuccess, payload: { ...serverResponse } },
    ];
    fetch.doMockIf(`${baseUrl}/api/companies`, buildMockResponse(serverResponse));
    await store.dispatch(addCompany(loggedInUserInfo, { subdomain }));
    const dispatchedActions = store.getActions();
    expect(dispatchedActions).toEqual(expectedActions);
  });
});
