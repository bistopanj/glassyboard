import configureMockStore from 'redux-mock-store';
import { buildMockResponse } from '../../utils/test-utils';
import { middlewares } from '../../build-store';
import removeAnswer from '../../actions/remove-answer';
import { actionTypes } from '../../actions';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);

describe('actions.removeAnswer(answerId, boardId, boardSlug)', () => {
  const store = mockStore({});
  const answerId = 'abc.123';
  const slug = `some-slug-${Math.floor(1000 * Math.random())}`;
  const boardId = 'def.789';
  const serverResponse = {
    id: answerId,
    answer: 'answer text',
    created_at: '2020-03-23T05:05:42.127403+04:30',
    updated_at: '2020-03-23T05:05:42.127403+04:30',
    active: false,
    upvotes: 0,
    upvoted: false,
  };

  beforeEach(() => {
    fetch.resetMocks();
    store.clearActions();
  });

  it('sends a DELETE request to /api/answers/[id] with appropriate data', async () => {
    expect.assertions(2);
    fetch.doMockIf(`${baseUrl}/api/answers/${answerId}`, buildMockResponse(serverResponse));
    await store.dispatch(removeAnswer(answerId, boardId, slug));
    expect(fetch).toHaveBeenCalledTimes(1);
    const apiReq = fetch.mock.calls[0][1];
    expect(apiReq.method).toBe('DELETE');
  });

  it('dispatches correct actions when the process is successful', async () => {
    expect.assertions(1);
    const expectedActions = [
      { type: actionTypes.removeAnswerLoading, payload: { answerId, boardSlug: slug } },
      { type: actionTypes.removeAnswerSuccess, payload: { answerId, boardSlug: slug } },
    ];
    fetch.doMockIf(`${baseUrl}/api/answers/${answerId}`, buildMockResponse(serverResponse));
    await store.dispatch(removeAnswer(answerId, boardId, slug));
    const dispatchedActions = store.getActions();
    expect(dispatchedActions).toEqual(expectedActions);
  });
});
