import configureMockStore from 'redux-mock-store';
import { buildMockResponse } from '../../utils/test-utils';
import { middlewares } from '../../build-store';
import updateBoard from '../../actions/update-board';
import { actionTypes } from '../../actions';

const baseUrl = process.env.BASE_URL;
const mockStore = configureMockStore(middlewares);

describe('actions.updateBoard(boardId, boardSlug, newTopic)', () => {
  const store = mockStore({});
  const slug = `some-slug-${Math.floor(1000 * Math.random())}`;
  const newTopic = `some new topic ${Math.floor(10000 * Math.random())}`;
  const boardId = 'abc.123';
  const serverResponse = {
    id: boardId,
    slug,
    topic: newTopic,
    editable: true,
    created_at: 'some data',
    updated_at: 'some data',
  };

  beforeEach(() => {
    fetch.resetMocks();
    store.clearActions();
  });

  it('sends a PUT request to /api/boards/[slug] with appropriate data', async () => {
    expect.assertions(3);
    fetch.doMockIf(`${baseUrl}/api/boards/${slug}`, buildMockResponse(serverResponse));
    await store.dispatch(updateBoard(boardId, slug, newTopic));
    expect(fetch).toHaveBeenCalledTimes(1);
    const apiReq = fetch.mock.calls[0][1];
    expect(apiReq.method).toBe('PUT');
    expect(apiReq.body).toBe(JSON.stringify({ topic: newTopic }));
  });

  it('dispatches correct actions when the process is successful', async () => {
    expect.assertions(1);
    const expectedActions = [
      { type: actionTypes.updateBoardLoading, payload: { slug, topic: newTopic } },
      { type: actionTypes.updateBoardSuccess, payload: { slug, updatedBoard: serverResponse } },
    ];
    fetch.doMockIf(`${baseUrl}/api/boards/${slug}`, buildMockResponse(serverResponse));
    await store.dispatch(updateBoard(boardId, slug, newTopic));
    const dispatchedActions = store.getActions();
    expect(dispatchedActions).toEqual(expectedActions);
  });
});
