import configureMockStore from 'redux-mock-store';
import Cookies from 'js-cookie';
import { middlewares } from '../../build-store';
import * as actions from '../../actions';
import { actionTypes } from '../../actions';

const mockStore = configureMockStore(middlewares);

describe('actions.logout()', () => {
  const token = 'abcd.efgh.ijkl';
  const store = mockStore({ userInfo: {} });

  beforeEach(() => {
    store.clearActions();
    Cookies.set('token', token);
  });
  afterAll(() => {
    Cookies.remove('token');
  });

  it('removes the token cookie', () => {
    expect(Cookies.get('token')).toBe(token);
    store.dispatch(actions.logout());
    expect(Cookies.get('token')).toBeUndefined();
  });

  it('dispatches an AUTH_LOGOUT action', () => {
    const expectedActions = [{ type: actionTypes.authLogout }];
    store.dispatch(actions.logout());
    const dispatchedActions = store.getActions();
    expect(dispatchedActions).toEqual(expectedActions);
  });
});
