import { useRef, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import useOnClickOutside from '../hooks/use-on-click-outside';
import './make-editable.css';

const discardChangesTitle = 'discard changes';
const confirmChangesTitle = 'apply changes';

const getDisplayName = someComponent =>
  someComponent.displayName || someComponent.name || 'component';

const doNothing = () => ({});

const renderButtons = (editing, discard, confirm) => {
  return (
    <div
      className="button-container"
      data-testid="editable-text-button-container"
      style={{ display: editing ? 'block' : 'none' }}
    >
      <Button
        color="link"
        onClick={confirm}
        className="editable-text-confirm-btn"
        data-testid="confirm-edit"
        title={confirmChangesTitle}
      >
        <FontAwesomeIcon icon={{ prefix: 'far', iconName: 'check-circle' }} />
      </Button>
      <Button
        color="link"
        onClick={discard}
        className="editable-text-close-btn"
        data-testid="discard-edit"
        title={discardChangesTitle}
      >
        <FontAwesomeIcon icon={{ prefix: 'far', iconName: 'times-circle' }} />
      </Button>
    </div>
  );
};

const makeEditable = (
  WrappedComponent,
  compName = `Editable (${getDisplayName(WrappedComponent)})`
) => {
  const EditableComponent = ({
    onConfirm = doNothing,
    disableEditing = doNothing,
    enableEditing = doNothing,
    editing = false,
    text = '',
    className = '',
    ...otherProps
  }) => {
    const compRef = useRef();
    const wrapperRef = useRef();

    useEffect(() => {
      if (editing && compRef.current) {
        compRef.current.focus();
      }
    }, [editing, compRef]);

    const resetText = useCallback(() => {
      if (compRef.current) compRef.current.innerHTML = text;
    }, [compRef, disableEditing]);

    const discard = useCallback(() => {
      resetText();
      disableEditing();
    }, [compRef, text, disableEditing]);

    const confirm = useCallback(() => {
      if (compRef.current && text !== compRef.current.innerHTML) {
        onConfirm(compRef.current.innerHTML);
      }
      disableEditing();
    }, [onConfirm, compRef, disableEditing]);

    const handleKeyDown = useCallback(
      e => {
        switch (e.key) {
          case 'Enter':
            confirm();
            break;
          case 'Escape':
            discard();
            break;
          default:
            break;
        }
      },
      [confirm, discard]
    );

    const handleClickOutside = useCallback(() => {
      if (editing) discard();
    }, [editing, discard]);

    useOnClickOutside(wrapperRef, handleClickOutside, !editing);

    const containerBaseClassName = 'd-flex align-items-center';
    const containerClassName = className
      ? `${className} ${containerBaseClassName}`
      : containerBaseClassName;

    return (
      <div className={containerClassName} ref={wrapperRef} data-testid="editable-text-container">
        <WrappedComponent
          className={editing ? 'editable-text editing' : 'editable-text'}
          ref={typeof WrappedComponent === 'string' ? compRef : null}
          onKeyDown={editing ? handleKeyDown : doNothing}
          onClick={!editing ? enableEditing : doNothing}
          contentEditable={editing}
          suppressContentEditableWarning={editing}
          {...otherProps}
        >
          {text}
        </WrappedComponent>
        {renderButtons(editing, discard, confirm)}
      </div>
    );
  };

  EditableComponent.displayName = compName;
  EditableComponent.WrappedComponent = WrappedComponent;
  EditableComponent.propTypes = {
    onConfirm: PropTypes.func,
    disableEditing: PropTypes.func,
    enableEditing: PropTypes.func,
    editing: PropTypes.bool,
    text: PropTypes.string,
    className: PropTypes.string,
  };
  EditableComponent.defaultProps = {
    onConfirm: doNothing,
    disableEditing: doNothing,
    enableEditing: doNothing,
    editing: false,
    text: '',
    className: '',
  };

  return EditableComponent;
};

export default makeEditable;
