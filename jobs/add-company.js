import { createCompany, updateCompanyPrimaryBoard } from '../services/company-services';
import { createCompanyBoard } from '../services/board-services';
import { buildSubdomainUrl } from '../utils/subdomain-utils';

const baseUrl = process.env.BASE_URL;

const addCompany = async (payload, userInfo) => {
  try {
    const {
      user: { id: userId },
    } = userInfo;
    const { subdomain } = payload;
    const newCompany = await createCompany({
      subdomain: subdomain.toLowerCase(),
      createdBy: userId,
    });
    const { subdomain: createdSubdomain, id: companyId } = newCompany;
    const primaryBoard = await createCompanyBoard({ createdBy: userId, company: newCompany });
    const updatedCompany = await updateCompanyPrimaryBoard({ companyId, boardId: primaryBoard.id });
    const theUrl = buildSubdomainUrl(baseUrl, createdSubdomain);
    return [{ ...updatedCompany, url: theUrl, board_slug: primaryBoard.slug }, theUrl];
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export default addCompany;
