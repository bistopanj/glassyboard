import { insertAnswer } from '../services/answer-services';

const addAnswer = async (payload, userInfo) => {
  try {
    const {
      user: { id: userId },
    } = userInfo;
    const { answer, boardId, slug } = payload;
    const newAnswer = await insertAnswer({ answer, boardId, createdBy: userId });
    return [newAnswer, `/board/${slug}`];
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export default addAnswer;
