import { upsertPanelBoardRel } from '../services/panel-services';
import { limitToOwner } from '../services/auth-services';

const addBoardToPanel = async (payload, userInfo) => {
  try {
    const {
      user: { id: userId },
    } = userInfo;
    const { boardSlug, panelId } = payload;
    const panelBoardRel = await upsertPanelBoardRel({ boardSlug, panelId, createdBy: userId });
    return [panelBoardRel, '/'];
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export default limitToOwner(addBoardToPanel);
