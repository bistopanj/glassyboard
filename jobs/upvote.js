import { insertUpvote } from '../services/upvote-services';

const handleUpvoteJob = async (payload, userInfo) => {
  try {
    const {
      user: { id: userId },
    } = userInfo;
    const { answerId, slug, boardId } = payload;
    const newUpvote = await insertUpvote({ answerId, boardId, createdBy: userId });
    return [newUpvote, `/board/${slug}`];
  } catch (error) {
    console.log('error');
    throw error;
  }
};

export default handleUpvoteJob;
