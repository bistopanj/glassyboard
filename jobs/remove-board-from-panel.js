import { limitToOwner } from '../services/auth-services';
import { deactivatePanelBoardRel } from '../services/panel-services';

const removeBoardFromPanel = async (payload, userInfo) => {
  try {
    const { boardSlug, panelId } = payload;
    const updatedRel = await deactivatePanelBoardRel({ boardSlug, panelId });
    return [updatedRel, '/'];
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export default limitToOwner(removeBoardFromPanel);
