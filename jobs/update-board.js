import { updateBoard as updateBoardService } from '../services/board-services';
import { allowOwner } from '../services/auth-services';
import ServerError from '../errors/server-error';

const updateBoard = async (payload, userInfo) => {
  try {
    const {
      user,
      user: { id: userId },
    } = userInfo;
    const { topic, slug } = payload;
    const updateValues = { topic };
    const filter = { slug, createdBy: userId };
    const updatedFiler = allowOwner(filter, user);
    const updatedBoard = await updateBoardService(updatedFiler, updateValues);
    if (!updatedBoard)
      throw new ServerError({
        code: 2060,
        status: 400,
        message: 'You cannot update this board',
      });
    return [updatedBoard, `/board/${slug}`];
  } catch (error) {
    const error2 = error;
    throw error;
  }
};

export default updateBoard;
