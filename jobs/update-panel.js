import { updatePanel as updatePanelService } from '../services/panel-services';
import ServerError from '../errors/server-error';
import { allowOwner } from '../services/auth-services';

const updatePanel = async (payload, userInfo) => {
  try {
    const {
      user,
      user: { id: userId },
    } = userInfo;
    const { title, id } = payload;
    const updateValues = { title };
    const filter = { id, createdBy: userId };
    const updatedFilter = allowOwner(filter, user);
    const updatedPanel = await updatePanelService(updatedFilter, updateValues);
    if (!updatedPanel)
      throw new ServerError({
        code: 2062,
        status: 400,
        message: 'You cannot update this panel',
      });
    return [updatedPanel, '/'];
  } catch (error) {
    const error2 = error;
    throw error;
  }
};

export default updatePanel;
