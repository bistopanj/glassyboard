import upvote from './upvote';
import addBoard from './add-board';
import updateBoard from './update-board';
import updatePanel from './update-panel';
import addAnswer from './add-answer';
import addPanel from './add-panel';
import addCompany from './add-company';
import addBoardToPanel from './add-board-to-panel';
import removeBoardFromPanel from './remove-board-from-panel';
import deactivateAnswer from './deactivate-answer';

export {
  upvote,
  addBoard,
  addCompany,
  updateBoard,
  updatePanel,
  addAnswer,
  deactivateAnswer,
  addPanel,
  addBoardToPanel,
  removeBoardFromPanel,
};
