import { createBoard } from '../services/board-services';

const addBoard = async (payload, userInfo) => {
  try {
    const {
      user: { id: userId },
    } = userInfo;
    const { topic } = payload;
    const newBoard = await createBoard({ topic, createdBy: userId });
    const { slug } = newBoard;
    return [newBoard, `/board/${slug}`];
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export default addBoard;
