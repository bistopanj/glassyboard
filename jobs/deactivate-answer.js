import { deactivateAnswer as deactivateAnswerService } from '../services/answer-services';
import ServerError from '../errors/server-error';
import { allowOwner } from '../services/auth-services';

const deactivateAnswer = async (payload, userInfo) => {
  try {
    const {
      user,
      user: { id: userId },
    } = userInfo;
    const { id } = payload;
    const filter = { id, createdBy: userId };
    const updatedFilter = allowOwner(filter, user);
    const updatedAnswer = await deactivateAnswerService(updatedFilter);
    if (!updatedAnswer)
      throw new ServerError({
        code: 2061,
        status: 400,
        message: 'You cannot delete this answer',
      });
    return [updatedAnswer, '/'];
  } catch (error) {
    const error2 = error;
    throw error;
  }
};

export default deactivateAnswer;
