import { createPanel } from '../services/panel-services';

const addPanel = async (payload, userInfo) => {
  try {
    const {
      user: { id: userId },
    } = userInfo;
    const { title, featured = false } = payload;
    const newPanel = await createPanel({ title, featured, createdBy: userId });
    return [newPanel, '/'];
  } catch (error) {
    // TODO: handle error better
    console.log(error);
    throw error;
  }
};

export default addPanel;
