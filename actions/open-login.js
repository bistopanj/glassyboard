import actionTypes from './types';

const openLogin = payload => ({
  type: actionTypes.openLogin,
  payload,
});

export default openLogin;
