import removeAnswerAdapter from '../adapters/remove-answer';
import actionTypes from './types';

const loading = (answerId, boardSlug) => ({
  type: actionTypes.removeAnswerLoading,
  payload: { answerId, boardSlug },
});
const success = (answerId, boardSlug) => ({
  type: actionTypes.removeAnswerSuccess,
  payload: { answerId, boardSlug },
});
const failed = (answerId, boardSlug, error) => ({
  type: actionTypes.removeAnswerFailed,
  payload: { answerId, boardSlug, error },
});

const removeAnswer = (answerId, boardId, boardSlug) => async dispatch => {
  dispatch(loading(answerId, boardSlug));
  try {
    await removeAnswerAdapter(answerId);
    dispatch(success(answerId, boardSlug));
  } catch (error) {
    dispatch(failed(answerId, boardSlug, error));
  }
};

export default removeAnswer;
