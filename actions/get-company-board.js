import actionTypes from './types';
import getPrimaryBoard from '../adapters/get-primary-board';

const loading = () => ({ type: actionTypes.getCompanyBoardLoading });
const success = boardData => ({ type: actionTypes.getCompanyBoardSuccess, payload: boardData });
const failed = error => ({ type: actionTypes.getCompanyBoardFailed, payload: error.toJSON() });

const getCompanyBoard = (
  subdomain,
  { abortSignal = undefined, cookie = undefined, pathInfo = undefined }
) => async dispatch => {
  dispatch(loading());
  try {
    const boardData = await getPrimaryBoard(subdomain, { abortSignal, cookie, pathInfo });
    dispatch(success(boardData));
  } catch (error) {
    dispatch(failed(error));
  }
};

export default getCompanyBoard;
