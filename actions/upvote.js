import actionTypes from './types';
import protectAction from './protect-action';
import { upvoteAnswer } from '../adapters';

const loading = ({ answerId, slug }) => ({
  type: actionTypes.upvoteLoading,
  payload: { answerId, slug },
});

const success = (answerId, slug, upvote) => ({
  type: actionTypes.upvoteSuccess,
  payload: { answerId, slug, upvote },
});

const failed = (answerId, slug, error) => ({
  type: actionTypes.upvoteFailed,
  payload: { answerId, slug, error: error.toJSON() },
});

const upvote = ({ answerId, boardId, slug }) => async dispatch => {
  dispatch(loading({ answerId, slug }));
  try {
    const upvoteState = await upvoteAnswer({ answerId, boardId, slug });
    dispatch(success(answerId, slug, upvoteState));
  } catch (error) {
    dispatch(failed(answerId, slug, error));
  }
};

export default protectAction(upvote)('UPVOTE');
