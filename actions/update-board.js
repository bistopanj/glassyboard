import updateBoardAdapter from '../adapters/update-board';
import actionTypes from './types';

const loading = (boardId, boardSlug, newTopic) => ({
  type: actionTypes.updateBoardLoading,
  payload: { slug: boardSlug, topic: newTopic },
});
const failed = (slug, error, oldTopic) => ({
  type: actionTypes.updateBoardFailed,
  payload: { slug, error, oldTopic },
});
const success = (slug, updatedBoard) => ({
  type: actionTypes.updateBoardSuccess,
  payload: { slug, updatedBoard },
});

const updateBoard = (boardId, boardSlug, newTopic, oldTopic) => async dispatch => {
  dispatch(loading(boardId, boardSlug, newTopic));
  try {
    const updatedBoard = await updateBoardAdapter(boardSlug, { topic: newTopic });
    dispatch(success(boardSlug, updatedBoard));
  } catch (error) {
    dispatch(failed(boardSlug, error, oldTopic));
  }
};

export default updateBoard;
