import { getBoardBySlug } from '../adapters';
import actionTypes from './types';

const loading = slug => ({ type: actionTypes.getBoardLoading, payload: slug });
const success = (slug, data) => ({
  type: actionTypes.getBoardSuccess,
  payload: { slug, data },
});
const failed = (slug, error) => ({
  type: actionTypes.getBoardFailed,
  payload: { slug, error: error.toJSON() },
});

const getBoard = (boardSlug, abortSignal = undefined, cookie = undefined) => async dispatch => {
  dispatch(loading(boardSlug));
  try {
    const boardData = await getBoardBySlug(boardSlug, abortSignal, cookie);
    dispatch(success(boardSlug, boardData));
  } catch (error) {
    dispatch(failed(boardSlug, error));
  }
};

export default getBoard;
