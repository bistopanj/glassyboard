import actionTypes from './types';
import { askTicket as requestTicket } from '../adapters';

const loading = () => ({ type: actionTypes.askTicketLoading });
const success = () => ({ type: actionTypes.askTicketSuccess });
const failed = error => ({ type: actionTypes.askTicketFailed, payload: error.toJSON() });

const askTicket = loginInfo => async dispatch => {
  dispatch(loading());
  try {
    await requestTicket(loginInfo);
    dispatch(success());
  } catch (error) {
    dispatch(failed(error));
  }
};

export default askTicket;
