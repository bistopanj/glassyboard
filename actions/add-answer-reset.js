import actionTypes from './types';

const addAnswerReset = slug => ({ type: actionTypes.addAnswerReset, payload: slug });

export default addAnswerReset;
