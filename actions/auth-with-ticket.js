import actionTypes from './types';
import authenticateWithTicket from '../adapters/authenticate-with-ticket';

const loading = () => ({ type: actionTypes.authWithTicketLoading });
const success = authResult => ({ type: actionTypes.authWithTicketSuccess, payload: authResult });
const failed = error => ({ type: actionTypes.authWithTicketFailed, payload: error.toJSON() });

const authWithTicket = (
  ticketId,
  { cookie = undefined, pathInfo = undefined }
) => async dispatch => {
  dispatch(loading());
  try {
    const authResult = await authenticateWithTicket(ticketId, { cookie, pathInfo });
    dispatch(success(authResult));
  } catch (error) {
    dispatch(failed(error));
  }
};

export default authWithTicket;
