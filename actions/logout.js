import Cookies from 'js-cookie';
import actionTypes from './types';

const logout = () => {
  Cookies.remove('token');
  return { type: actionTypes.authLogout };
};

export default logout;
