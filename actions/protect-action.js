import openLogin from './open-login';

const isAuthenticated = userInfo => {
  try {
    const { token } = userInfo;
    return !!token;
  } catch (error) {
    return false;
  }
};

const protectAction = actionCreator => ticketAction => (userInfo, payload) => {
  if (isAuthenticated(userInfo)) return actionCreator(payload);
  return openLogin({ ticketAction, payload });
};

export default protectAction;
