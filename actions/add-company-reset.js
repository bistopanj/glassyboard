import actionTypes from './types';

const addCompanyReset = () => ({ type: actionTypes.addCompanyReset });

export default addCompanyReset;
