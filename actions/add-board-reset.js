import actionTypes from './types';

const addBoardReset = () => ({ type: actionTypes.addBoardReset });

export default addBoardReset;
