import actionTypes from './types';
import protectAction from './protect-action';
import { addBoard as createBoard } from '../adapters';

const loading = () => ({ type: actionTypes.addBoardLoading });
const success = board => ({ type: actionTypes.addBoardSuccess, payload: board });
const failed = error => ({ type: actionTypes.addBoardFailed, payload: error.toJSON() });

const addBoard = newBoard => async dispatch => {
  dispatch(loading());
  try {
    const theBoard = await createBoard(newBoard);
    dispatch(success(theBoard));
  } catch (error) {
    dispatch(failed(error));
  }
};

export default protectAction(addBoard)('ADD-BOARD');
