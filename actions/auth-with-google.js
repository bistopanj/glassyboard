import actionTypes from './types';
import authenticateWithGoogle from '../adapters/authenticate-with-google';

const loading = () => ({ type: actionTypes.authWithGoogleLoading });
const success = authResult => ({ type: actionTypes.authWithGoogleSuccess, payload: authResult });
const failed = error => ({ type: actionTypes.authWithGoogleFailed, payload: error.toJSON() });

const authWithGoogle = ({ googleToken, data }, { pathInfo = undefined } = {}) => async dispatch => {
  dispatch(loading());
  try {
    const authResult = await authenticateWithGoogle({ googleToken, data }, { pathInfo });
    dispatch(success(authResult));
  } catch (error) {
    dispatch(failed(error));
  }
};

export default authWithGoogle;
