import actionTypes from './types';

const closeLoginModal = () => ({ type: actionTypes.closeLoginModal });

export default closeLoginModal;
