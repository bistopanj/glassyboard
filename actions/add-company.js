import actionTypes from './types';
import protectAction from './protect-action';
import addCompanyAdapter from '../adapters/add-company';

const loading = () => ({ type: actionTypes.addCompanyLoading });
const success = company => ({ type: actionTypes.addCompanySuccess, payload: company });
const failed = error => ({ type: actionTypes.addCompanyFailed, payload: error.toJSON() });

const addCompany = company => async dispatch => {
  dispatch(loading());
  try {
    const theCompany = await addCompanyAdapter(company);
    dispatch(success(theCompany));
  } catch (error) {
    dispatch(failed(error));
  }
};

export default protectAction(addCompany)('ADD-COMPANY');
