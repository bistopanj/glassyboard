import actionTypes from './types';
import { extractUser } from '../utils/token-utils';

// TODO: get user info from server to ensure that it is up to date

const updateConnectionInfo = (token, { host, protocol, subdomain }) => ({
  type: actionTypes.updateConnectionInfo,
  payload: { token, user: extractUser(token), host, protocol, subdomain },
});

export default updateConnectionInfo;
