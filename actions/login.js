import actionTypes from './types';
import { getTicket } from '../adapters';

const loading = () => ({ type: actionTypes.loginLoading });
const success = () => ({ type: actionTypes.loginSuccess });
const failed = error => ({ type: actionTypes.loginFailed, payload: error.toJSON() });

const login = loginInfo => async dispatch => {
  dispatch(loading());
  try {
    await getTicket(loginInfo);
    dispatch(success());
  } catch (error) {
    dispatch(failed(error));
  }
};

export default login;
