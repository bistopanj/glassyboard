import protectAction from './protect-action';
import actionTypes from './types';
import { addAnswer as createAnswer } from '../adapters';

const loading = slug => ({ type: actionTypes.addAnswerLoading, payload: slug });
const success = (slug, answer) => ({
  type: actionTypes.addAnswerSuccess,
  payload: { slug, answer },
});
const failed = (slug, error) => ({
  type: actionTypes.addAnswerFailed,
  payload: { slug, error: error.toJSON() },
});

const addAnswer = ({ answer, slug, id }) => async dispatch => {
  dispatch(loading(slug));
  try {
    const theAnswer = await createAnswer({ answer, boardId: id, slug });
    dispatch(success(slug, theAnswer));
  } catch (error) {
    dispatch(failed(slug, error));
  }
};

export default protectAction(addAnswer)('ADD-ANSWER');
