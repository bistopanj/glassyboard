import React from 'react';
import PropTypes from 'prop-types';

export function FontAwesomeIcon(props) {
  const {
    icon: { prefix, iconName },
  } = props;
  return <i className={`${prefix} ${iconName}`} />;
}

FontAwesomeIcon.propTypes = {
  icon: PropTypes.shape({
    prefix: PropTypes.string,
    iconName: PropTypes.string,
  }),
};
FontAwesomeIcon.defaultProps = {
  icon: {
    prefix: 'far',
    iconName: '',
  },
};

export const a = 'a';
