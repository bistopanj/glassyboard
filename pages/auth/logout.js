import { useEffect } from 'react';
import Cookies from 'js-cookie';
import Router from 'next/router';

const initialText = 'Signing out...';
const googleSignInClientId = process.env.GOOGLE_SIGN_IN_CLIENT_ID;
const cookieDomain = process.env.COOKIE_DOMAIN;

const LogoutPage = () => {
  useEffect(() => {
    if (window.gapi) {
      window.gapi.load('auth2', () => {
        window.gapi.auth2
          .init({
            client_id: googleSignInClientId,
            cookiepolicy: 'single_host_origin',
          })
          .then(authInstance => {
            authInstance.disconnect();
            Cookies.remove('token', { path: '/', domain: cookieDomain });
            Router.push('/');
          })
          .catch(error => {
            console.log(error);
          });
      });
    }
  }, []);
  return (
    <main className="full-page-centered" data-testid="logout-initial-message">
      {initialText}
    </main>
  );
};

export default LogoutPage;
