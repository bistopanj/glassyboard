import { useEffect } from 'react';
import { useRouter } from 'next/router';
import Cookies from 'js-cookie';

const initialText = 'One moment please...';
const cookieDomain = process.env.COOKIE_DOMAIN;

const LoginPage = () => {
  const router = useRouter();
  const { query: { token, redirectTo = '/' } = {} } = router;

  useEffect(() => {
    if (token) Cookies.set('token', token, { path: '/', domain: cookieDomain });
    window.location.replace(redirectTo);
  }, [token]);

  return (
    <main className="full-page-centered" data-testid="login-initial-message">
      {initialText}
    </main>
  );
};

export default LoginPage;
