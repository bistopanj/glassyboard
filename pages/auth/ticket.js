import { useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import Router from 'next/router';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import Link from 'next/link';
import authWithTicket from '../../actions/auth-with-ticket';
import makeLoginWithTicketInfoSelector from '../../selectors/get-login-with-ticket-info';
import ContentCard from '../../components/content-card';

const initialText = 'One moment please...';
const loadingText = 'another moment please...';

const errorMessage = (
  <p>
    Something went wrong .
    <br />
    Sorry for the inconvenience 😞.
  </p>
);
const bottomLink = (
  <div className="align-self-center mt-4 text-center text-black-50">
    <p>
      <br />
      <Link href="/">
        <a href="/">go back to home page</a>
      </Link>
    </p>
  </div>
);

const AuthWithTicketPage = ({
  ticketId = null,
  error = null,
  loading = false,
  success = false,
  authResult = null,
}) => {
  useEffect(() => {
    if (success) {
      const { token, redirectTo } = authResult;
      Router.push(
        `/auth/login?token=${token}&redirectTo=${encodeURIComponent(redirectTo)}`,
        '/auth/login'
      );
    }
  }, [success]);

  if (loading)
    return (
      <main className="full-page-centered" data-testid="auth-ticket-loading-message">
        {loadingText}
      </main>
    );

  // TODO: handle the errors better
  if (error)
    return (
      <main className="full-page-centered" data-testid="auth-ticket-error-card">
        <Container className="d-flex flex-column justify-content-center align-items-center">
          <ContentCard>
            {errorMessage}
            {bottomLink}
          </ContentCard>
        </Container>
      </main>
    );

  return (
    <main className="full-page-centered" data-testid="auth-ticket-initial-message">
      {initialText}
    </main>
  );
};

AuthWithTicketPage.getInitialProps = async ({ store, query, req, isServer, pathInfo }) => {
  const { ticketId } = query;
  if (ticketId) {
    await store.dispatch(authWithTicket(ticketId, { pathInfo }));
    return { ticketId };
  }
  return {};
};

AuthWithTicketPage.propTypes = {
  ticketId: PropTypes.string.isRequired,
  error: PropTypes.shape(),
  loading: PropTypes.bool,
  success: PropTypes.bool,
  authResult: PropTypes.shape({
    token: PropTypes.string,
    redirectTo: PropTypes.string,
    user: PropTypes.shape({
      id: PropTypes.string,
      first_name: PropTypes.string,
      last_name: PropTypes.string,
      created_at: PropTypes.string,
      email: PropTypes.string,
    }),
  }),
};
AuthWithTicketPage.defaultProps = {
  error: null,
  loading: false,
  success: false,
  authResult: null,
};

const mapDispatchToProps = () => ({});

export default connect(makeLoginWithTicketInfoSelector, mapDispatchToProps)(AuthWithTicketPage);
