import { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Container } from 'reactstrap';
import Router from 'next/router';
import Link from 'next/link';
import authWithGoogle from '../../actions/auth-with-google';
import makeLoginWithGoogleInfoSelector from '../../selectors/get-login-with-google-info';
import ContentCard from '../../components/content-card';

const initialText = 'One moment please...';
const loadingText = 'another moment please...';

const errorMessage = (
  <p>
    Something went wrong .
    <br />
    Sorry for the inconvenience 😞.
  </p>
);
const bottomLink = (
  <div className="align-self-center mt-4 text-center text-black-50">
    <p>
      <br />
      <Link href="/">
        <a href="/">go back to home page</a>
      </Link>
    </p>
  </div>
);

const AuthWithGooglePage = ({
  error = null,
  loading = false,
  success = false,
  authResult = null,
}) => {
  useEffect(() => {
    if (success) {
      const { token: glassyboardToken, redirectTo } = authResult;
      Router.push(
        `/auth/login?token=${glassyboardToken}&redirectTo=${encodeURIComponent(redirectTo)}`,
        '/auth/login'
      );
    }
  }, [success]);

  if (loading)
    return (
      <main className="full-page-centered" data-testid="auth-google-loading-message">
        {loadingText}
      </main>
    );

  if (error)
    return (
      <main className="full-page-centered" data-testid="auth-google-error-card">
        <Container className="d-flex flex-column justify-content-center align-items-center">
          <ContentCard>
            {errorMessage}
            {bottomLink}
          </ContentCard>
        </Container>
      </main>
    );
  return (
    <main className="full-page-centered" data-testid="auth-google-initial-message">
      {initialText}
    </main>
  );
};

AuthWithGooglePage.getInitialProps = async ({ store, query, pathInfo }) => {
  const { token: googleToken, data: dataStr } = query;
  let data;
  if (dataStr) data = JSON.parse(dataStr);
  await store.dispatch(authWithGoogle({ googleToken, data }, { pathInfo }));

  return {};
};

AuthWithGooglePage.propTypes = {
  error: PropTypes.shape(),
  loading: PropTypes.bool,
  success: PropTypes.bool,
  authResult: PropTypes.shape({
    token: PropTypes.string,
    redirectTo: PropTypes.string,
    user: PropTypes.shape({
      id: PropTypes.string,
      first_name: PropTypes.string,
      last_name: PropTypes.string,
      created_at: PropTypes.string,
      email: PropTypes.string,
    }),
  }),
};

AuthWithGooglePage.defaultProps = {
  error: null,
  loading: false,
  success: false,
  authResult: null,
};

const mapDispatchToProps = () => ({});

export default connect(makeLoginWithGoogleInfoSelector, mapDispatchToProps)(AuthWithGooglePage);
