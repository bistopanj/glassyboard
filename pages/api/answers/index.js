import { authMiddleware } from '../../../services/auth-services';
import { parseServerError } from '../../../errors';
import { raiseJob } from '../../../services/job-services';

const handlePost = async (req, res) => {
  try {
    const { user, body } = req;
    const [result] = await raiseJob({ job: 'add-answer', payload: body }, { user });
    res.json(result);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'POST':
      return authMiddleware(handlePost)(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;
