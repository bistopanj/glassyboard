import { authMiddleware } from '../../../services/auth-services';
import { parseServerError } from '../../../errors';
import { raiseJob } from '../../../services/job-services';

const handleDelete = async (req, res) => {
  try {
    const {
      user,
      query: { id },
    } = req;
    const [result] = await raiseJob({ job: 'deactivate-answer', payload: { id } }, { user });
    res.json(result);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'DELETE':
      return authMiddleware(handleDelete)(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;
