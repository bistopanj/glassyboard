import { parseServerError } from '../../../errors';
import { validateGoogleIdToken } from '../../../services/google-auth-services';
import { loginUser } from '../../../services/auth-services';
import { raiseJob } from '../../../services/job-services';

const handlePost = async (req, res) => {
  try {
    const { body: { googleToken, data = {} } = {} } = req;
    const userInfo = await validateGoogleIdToken(googleToken);
    const { token, user } = await loginUser(userInfo);
    const { ticketAction, payload = {} } = data;
    let redirectTo = '/';
    if (ticketAction) {
      const [, redTo] = await raiseJob({ job: ticketAction, payload }, { token, user });
      redirectTo = redTo;
    }
    res.json({ token, user, redirectTo });
  } catch (error) {
    console.log(error);
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'POST':
      return handlePost(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;
