import parseServerError from '../../../errors/parse-server-error';
import { validateTicket, consumeTicket } from '../../../services/ticket-services';
import { loginUser } from '../../../services/auth-services';

const handlePost = async (req, res) => {
  try {
    const { body: { ticketId } = {} } = req;
    const loginInfo = await validateTicket(ticketId);
    const { ticketInfo = {} } = loginInfo;
    const { token, user } = await loginUser(loginInfo);
    const [, redirectTo] = await consumeTicket({ ticketId, ticketInfo, token, user });
    res.json({ token, redirectTo, user });
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'POST':
      return handlePost(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;
