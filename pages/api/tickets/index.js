import { sendEmail } from '../../../services/mail-services';
import { generateTicket } from '../../../services/ticket-services';

const handlePost = async (req, res) => {
  try {
    const { body } = req;
    const { email } = body;
    if (!email) throw new Error('email is required');
    const ticketId = await generateTicket(body);
    await sendEmail({ to: email, template: 'ticket', data: { ticketId } });
    res.json({ done: true });
  } catch (error) {
    res.status(400).json({ data: error });
  }
};

const handleReqs = async (req, res) => {
  const { method } = req;

  switch (method) {
    case 'POST':
      await handlePost(req, res);
      break;
    default:
      res.status(404).send('endpoint not found');
  }
};

export default handleReqs;
