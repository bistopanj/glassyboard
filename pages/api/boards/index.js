import Cors from 'cors';
import { authMiddleware, extractUserFromReq } from '../../../services/auth-services';
import parseServerError from '../../../errors/parse-server-error';
import { raiseJob } from '../../../services/job-services';
import { findCompanyBoard } from '../../../services/board-services';
import ServerError from '../../../errors/server-error';

const cors = Cors();

const handleGet = async (req, res) => {
  try {
    const {
      query: { subdomain },
    } = req;
    if (!subdomain) {
      // TODO: handle this error better
      throw new ServerError({
        status: 400,
        code: 2053,
        message: `the query is not supported`,
      });
    }
    const user = await extractUserFromReq(req);
    const board = await findCompanyBoard(subdomain, user);
    res.json(board);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handlePost = async (req, res) => {
  try {
    const { user, body } = req;
    const [result] = await raiseJob({ job: 'add-board', payload: body }, { user });
    res.json(result);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'POST':
      return authMiddleware(handlePost)(req, res);
    case 'GET':
      return handleGet(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;
