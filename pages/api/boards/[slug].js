import parseServerError from '../../../errors/parse-server-error';
import { findBoardBySlug } from '../../../services/board-services';
import { extractUserFromReq, authMiddleware } from '../../../services/auth-services';
import { raiseJob } from '../../../services/job-services';

const handleGet = async (req, res) => {
  try {
    const {
      query: { slug },
    } = req;
    const user = await extractUserFromReq(req);
    const board = await findBoardBySlug(slug, user);
    res.json(board);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handlePut = async (req, res) => {
  try {
    const {
      user,
      body,
      query: { slug },
    } = req;
    const [result] = await raiseJob({ job: 'update-board', payload: { ...body, slug } }, { user });
    res.json(result);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'GET':
      return handleGet(req, res);
    case 'PUT':
      return authMiddleware(handlePut)(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;
