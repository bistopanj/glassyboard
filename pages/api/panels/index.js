import { parseServerError } from '../../../errors';
import { extractUserFromReq, authMiddleware } from '../../../services/auth-services';
import { fetchFeaturedPanels } from '../../../services/panel-services';
import { raiseJob } from '../../../services/job-services';

const handleGet = async (req, res) => {
  try {
    const user = await extractUserFromReq(req);
    const panels = await fetchFeaturedPanels(user);
    res.json(panels);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handlePost = async (req, res) => {
  try {
    const { user, body } = req;
    const [result] = await raiseJob({ job: 'add-panel', payload: body }, { user });
    res.json(result);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'GET':
      return handleGet(req, res);
    case 'POST':
      return authMiddleware(handlePost)(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;
