import { authMiddleware } from '../../../../../services/auth-services';
import { raiseJob } from '../../../../../services/job-services';
import { parseServerError } from '../../../../../errors';

const handlePost = async (req, res) => {
  try {
    const {
      user,
      body,
      query: { id: panelId },
    } = req;
    const [result] = await raiseJob(
      { job: 'add-board-to-panel', payload: { ...body, panelId } },
      { user }
    );
    res.json(result);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handleDelete = async (req, res) => {
  try {
    const {
      user,
      body,
      query: { id: panelId },
    } = req;
    const [result] = await raiseJob(
      { job: 'remove-board-from-panel', payload: { ...body, panelId } },
      { user }
    );
    res.json(result);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'POST':
      return authMiddleware(handlePost)(req, res);
    case 'DELETE':
      return authMiddleware(handleDelete)(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;
