import { authMiddleware } from '../../../../services/auth-services';
import { raiseJob } from '../../../../services/job-services';
import parseServerError from '../../../../errors/parse-server-error';

const handlePut = async (req, res) => {
  try {
    const {
      user,
      body,
      query: { id },
    } = req;
    const [result] = await raiseJob({ job: 'update-panel', payload: { ...body, id } }, { user });
    res.json(result);
  } catch (error) {
    const [status, body] = parseServerError(error);
    res.status(status).json(body);
  }
};

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'PUT':
      return authMiddleware(handlePut)(req, res);
    default:
      return res.status(405).send('endpoint not found');
  }
};

export default handleReqs;
