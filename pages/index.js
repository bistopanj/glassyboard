import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container } from 'reactstrap';
import getCompanyBoard from '../actions/get-company-board';
import AddCompanyForm from '../components/add-company-form';
import CompanyBoard from '../components/company-board';
import UclaDashboard from '../components/ucla-dashboard';
import makeGetPrimaryBoardSelector from '../selectors/get-primary-board';

const subdomainMap = {
  ucla: { component: UclaDashboard, testId: 'ucla-dashboard' },
  hi: { component: CompanyBoard, testId: 'company-board' },
};

const renderSubdomainComp = (subdomain, board, pathInfo) => {
  if (subdomain in subdomainMap) {
    const SubdomainComponent = subdomainMap[subdomain].component;
    return (
      <SubdomainComponent
        board={board}
        pathInfo={pathInfo}
        data-testid={subdomainMap[subdomain].testId}
      />
    );
  }
  if (subdomain.length === 0 || subdomain === 'www')
    return <AddCompanyForm board={board} data-testid="add-company-form" />;

  return <CompanyBoard board={board} pathInfo={pathInfo} data-testid="company-board" />;
};

const IndexPage = ({ pathInfo = {}, board }) => {
  const { subdomain } = pathInfo;

  return (
    <main className="full-page" data-testid="home-page-container">
      <Container>{renderSubdomainComp(subdomain, board, pathInfo)}</Container>
    </main>
  );
};

IndexPage.getInitialProps = async ({ store, req, isServer, pathInfo }) => {
  const { subdomain } = pathInfo;
  let cookie = null;
  if (isServer) {
    const { cookie: k } = req.headers || {};
    cookie = k;
  }
  await store.dispatch(getCompanyBoard(subdomain, { cookie, pathInfo }));
  return {};
};

IndexPage.propTypes = {
  pathInfo: PropTypes.shape({
    host: PropTypes.string.isRequired,
    subdomain: PropTypes.string.isRequired,
    protocol: PropTypes.string.isRequired,
  }).isRequired,
  board: PropTypes.shape({
    data: PropTypes.shape({
      id: PropTypes.string,
      topic: PropTypes.string,
      slug: PropTypes.string,
      editable: PropTypes.bool,
      created_at: PropTypes.string,
      updated_at: PropTypes.string,
    }),
    newAnswer: PropTypes.shape({
      error: PropTypes.shape(),
      loading: PropTypes.bool,
      success: PropTypes.bool,
    }),
    answers: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        answer: PropTypes.string.isRequired,
        created_at: PropTypes.string,
        updated_at: PropTypes.string,
        upvotes: PropTypes.number,
        upvoted: PropTypes.bool,
      })
    ),
  }),
};

IndexPage.defaultProps = {
  board: {},
};

const mapDispatchToProps = () => ({});

export default connect(makeGetPrimaryBoardSelector, mapDispatchToProps)(IndexPage);
