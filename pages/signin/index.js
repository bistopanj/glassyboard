import { Container, Button } from 'reactstrap';
import { useRouter } from 'next/router';
import ContentCard from '../../components/content-card';
import SigninDialog from '../../components/signin-dialog';

const goBackText = 'Go Back';

const SigninPage = () => {
  const router = useRouter();
  const {
    query: { data: signinDataStr = null },
  } = router;
  const data = signinDataStr ? JSON.parse(signinDataStr) : null;
  return (
    <main className="full-page-centered" data-testid="login-page-container">
      <Container className="d-flex flex-column justify-content-center align-items-center">
        <ContentCard>
          <SigninDialog signinData={data} />
        </ContentCard>
        <Button color="link" onClick={router.back} data-testid="go-back-button">
          {goBackText}
        </Button>
      </Container>
    </main>
  );
};

export default React.memo(SigninPage);
