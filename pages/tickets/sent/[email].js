import { useRouter } from 'next/router';
import { Container } from 'reactstrap';
import Link from 'next/link';
import ContentCard from '../../../components/content-card';

const bottomLink = (
  <div className="align-self-center mt-4 text-center text-black-50">
    <p>
      or
      <br />
      <Link href="/">
        <a href="/">go back to home page</a>
      </Link>
    </p>
  </div>
);

const sentGuide = email => (
  <p>
    An email has been sent to the following address:
    <br />
    <br />
    <span id="user-email" className="text-success" data-testid="user-email">
      <strong>{email}</strong>
    </span>
    <br />
    <br />
    Please go to your inbox and click on the link in the email to verify your email address.
  </p>
);

const TicketSent = () => {
  const router = useRouter();
  const { query: { email = '' } = {} } = router || {};
  return (
    <main className="full-page-centered" data-testid="ticket-sent-confirmation">
      <Container className="d-flex flex-column justify-content-center align-items-center">
        <ContentCard>
          {sentGuide(email)}
          {bottomLink}
        </ContentCard>
      </Container>
    </main>
  );
};

export default TicketSent;
