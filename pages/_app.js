import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import cookies from 'next-cookies';
import { config } from '@fortawesome/fontawesome-svg-core';
import { getFullPathInfo } from '../utils/next-utils';
import updateConnectionInfo from '../actions/update-connection-info';
import buildStore from '../build-store';
import LoginModal from '../components/login-modal';
import Layout from '../components/layout';
import '@fortawesome/fontawesome-svg-core/styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles.css';
import '../resources/icons/fontawesome';
import DefaultHead from '../components/default-head';

config.autoAddCss = false;

const noNavbarList = ['/auth/google', '/auth/login', '/auth/logout', '/auth/ticket', '/signin'];

const MyApp = ({ Component, pageProps, store }) => {
  return (
    <>
      <DefaultHead />
      <Provider store={store}>
        <Layout noNavbarList={noNavbarList}>
          <Component {...pageProps} />
        </Layout>
        <LoginModal data-testid="login-modal-comp" />
      </Provider>
    </>
  );
};

MyApp.getInitialProps = async ({ Component, ctx }) => {
  const cooks = cookies(ctx);
  const { token } = cooks || {};
  const { host, protocol, subdomain } = getFullPathInfo(ctx);
  ctx.pathInfo = { host, protocol, subdomain };
  ctx.store.dispatch(updateConnectionInfo(token, { host, protocol, subdomain }));
  const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
  return { pageProps: { ...pageProps, pathInfo: { host, protocol, subdomain } } };
};

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.shape(),
  store: PropTypes.shape().isRequired,
};

MyApp.defaultProps = {
  pageProps: {},
};

export { MyApp };
const MyAppConnected = withRedux(buildStore)(MyApp);

export default MyAppConnected;
