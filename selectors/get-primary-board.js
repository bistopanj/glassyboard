import { createSelector } from 'reselect';

export const buildFinalBoardData = bySlugData => {
  const { answers, answerById, ...boardData } = bySlugData;
  const finalAnswers = answers.map(id => answerById[id]);
  boardData.answers = finalAnswers;
  return boardData;
};

const makeSelector = () =>
  createSelector(
    state => state.boards.bySlug,
    state => state.boards.primary,
    (boardsBySlug = {}, slug) =>
      slug in boardsBySlug ? buildFinalBoardData(boardsBySlug[slug]) : null
  );

const makeGetPrimaryBoardSelector = () => {
  const getPrimaryBoard = makeSelector();
  return state => ({ board: getPrimaryBoard(state) });
};

export default makeGetPrimaryBoardSelector;
