import { createSelector } from 'reselect';

const makeSelector = () =>
  createSelector(
    state => state.connectionInfo.subdomain,
    subdomain => ({ subdomain })
  );

const makeGetSubdomainSelector = () => {
  const getSubdomain = makeSelector();
  return state => getSubdomain(state);
};

export default makeGetSubdomainSelector;
