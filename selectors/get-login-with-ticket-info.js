import { createSelector } from 'reselect';

const makeSelector = () =>
  createSelector(
    state => state.login.ticket,
    state => state
  );

const makeLoginWithTicketInfoSelector = () => {
  const getLoginWithTicketInfo = makeSelector();
  return state => getLoginWithTicketInfo(state);
};

export default makeLoginWithTicketInfoSelector;
