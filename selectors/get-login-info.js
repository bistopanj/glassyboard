import { createSelector } from 'reselect';

const makeSelector = () =>
  createSelector(
    state => state.login.isOpen,
    state => state.login.data,
    state => state.connectionInfo.subdomain,
    (isOpen, data, subdomain) => ({
      isOpen,
      data,
      subdomain,
    })
  );

const makeLoginInfoSelector = () => {
  const getLoginInfo = makeSelector();
  return state => getLoginInfo(state);
};

export default makeLoginInfoSelector;
