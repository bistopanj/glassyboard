import { createSelector } from 'reselect';

export const buildFinalBoardData = (slug, bySlugData) => {
  if (!bySlugData) return { slug };
  const { answers, answerById, ...boardData } = bySlugData;
  const finalAnswers = answers.map(id => answerById[id]);
  boardData.answers = finalAnswers;
  boardData.slug = slug;
  return boardData;
};

const makeSelector = () =>
  createSelector(
    state => state.boards.bySlug,
    state => state.panels.error,
    state => state.panels.loading,
    state => state.panels.list,
    (boardBySlug, error, loading, panels) => ({
      error,
      loading,
      panels: panels.map(p => ({
        ...p,
        boards: p.boards.map(slug => buildFinalBoardData(slug, boardBySlug[slug])),
      })),
    })
  );

const makeGetPanelsSelector = () => {
  const getPanels = makeSelector();
  return state => getPanels(state);
};

export default makeGetPanelsSelector;
