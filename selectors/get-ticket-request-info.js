import { createSelector } from 'reselect';

const makeSelector = () =>
  createSelector(
    state => state.login.ticketRequest,
    ({ error, success, loading }) => ({ error, success, loading })
  );

const makeTicketRequestInfoSelector = () => {
  const getTicketRequestInfo = makeSelector();
  return state => getTicketRequestInfo(state);
};

export default makeTicketRequestInfoSelector;
