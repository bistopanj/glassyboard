import { createSelector } from 'reselect';

const makeSelector = () =>
  createSelector(
    state => !!state.connectionInfo.token,
    state => state.connectionInfo.user,
    state => state.connectionInfo.token,
    (isAuthenticated, user, token) => ({ user, token, isAuthenticated })
  );

const makeUserInfoSelector = () => {
  const getUserInfo = makeSelector();
  return state => getUserInfo(state);
};

export default makeUserInfoSelector;
