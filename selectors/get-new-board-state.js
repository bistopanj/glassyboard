import { createSelector } from 'reselect';

const makeSelector = () =>
  createSelector(
    state => state.boards.new,
    newBoardState => newBoardState
  );

const makeNewBoardStateSelector = () => {
  const getNewBoardState = makeSelector();
  return state => getNewBoardState(state);
};

export default makeNewBoardStateSelector;
