import { createSelector } from 'reselect';

const makeSelector = () =>
  createSelector(
    state => state.login.google,
    state => state
  );

const makeLoginWithGoogleInfoSelector = () => {
  const getLoginWithGoogleInfo = makeSelector();
  return state => getLoginWithGoogleInfo(state);
};

export default makeLoginWithGoogleInfoSelector;
