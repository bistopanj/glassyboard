import { createSelector } from 'reselect';
import { buildFinalBoardData } from './get-board-by-slug';

// const makeSelector = () =>
//   createSelector(
//     state => state.boards.list,
//     state => state.boards.bySlug,
//     (list, bySlug) => list.map(slug => ({ ...buildFinalBoardData(bySlug[slug]), slug }))
//   );

const makeSelector = () =>
  createSelector(
    state => state.boards.list,
    state => state.boards.bySlug,
    (list, bySlug) => list.map(slug => ({ ...buildFinalBoardData(bySlug[slug]), slug }))
  );

const makeGetBoardListSelector = () => {
  const getBoardList = makeSelector();
  return state => ({ boards: getBoardList(state) });
};

export default makeGetBoardListSelector;
