import { createSelector } from 'reselect';

const makeSelector = () =>
  createSelector(
    state => state.panels.error,
    state => state.panels.loading,
    state => state.panels.list,
    (error, loading, panels) => ({ error, loading, panels })
  );

const makeGetPanelsInfoSelector = () => {
  const getPanelsInfo = makeSelector();
  return state => getPanelsInfo(state);
};

export default makeGetPanelsInfoSelector;
