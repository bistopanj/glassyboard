import { createSelector } from 'reselect';

const getBoards = state => state.boards.list;

const getBoardsState = createSelector(getBoards, list => list);

export default getBoardsState;
