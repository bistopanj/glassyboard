import { createSelector } from 'reselect';

const defaultState = {
  error: null,
  success: null,
  loading: false,
};

const makeSelector = () =>
  createSelector(
    state => state.boards.bySlug,
    (_, slug) => slug,
    (boardsBySlug = {}, slug) => {
      try {
        return boardsBySlug[slug].newAnswer || defaultState;
      } catch (err) {
        return defaultState;
      }
    }
  );

const makeGetNewAnswerStatusSelector = () => {
  const getNewAnswerStatus = makeSelector();
  return (state, slug) => getNewAnswerStatus(state, slug);
};

export default makeGetNewAnswerStatusSelector;
