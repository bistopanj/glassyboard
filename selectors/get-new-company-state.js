import { createSelector } from 'reselect';

const makeSelector = () =>
  createSelector(
    state => state.company.new,
    state => state.company.data,
    ({ error, loading, success }, company) => ({ error, loading, success, company })
  );

const makeNewCompanyStateSelector = () => {
  const getNewCompanyState = makeSelector();
  return state => getNewCompanyState(state);
};

export default makeNewCompanyStateSelector;
