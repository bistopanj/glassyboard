import { createSelector } from 'reselect';

export const buildFinalBoardData = bySlugData => {
  const { answers, answerById, ...boardData } = bySlugData;
  const finalAnswers = answers.map(id => answerById[id]);
  boardData.answers = finalAnswers;
  return boardData;
};

const makeSelector = () =>
  createSelector(
    state => state.boards.bySlug,
    (_, { slug }) => slug,
    (boardsBySlug = {}, slug) =>
      slug in boardsBySlug ? buildFinalBoardData(boardsBySlug[slug]) : null
  );

const makeGetBoardBySlugSelector = () => {
  const getBoardBySlug = makeSelector();
  return (state, props) => ({ boardData: getBoardBySlug(state, props) });
};

export default makeGetBoardBySlugSelector;
