import { buildFinalError } from '../errors';
import { fetchData } from '../utils/api-utils';

const getPrimaryBoard = async (
  subdomain,
  { abortSignal = undefined, cookie = undefined, pathInfo = undefined }
) => {
  try {
    const boardData = fetchData(
      `/api/boards?subdomain=${subdomain}`,
      {
        method: 'GET',
        signal: abortSignal,
        headers: cookie ? { cookie } : undefined,
      },
      pathInfo
    );
    return boardData;
  } catch (error) {
    throw buildFinalError(error, 1);
  }
};

export default getPrimaryBoard;
