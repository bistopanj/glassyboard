import { fetchData } from '../utils/api-utils';
import { buildFinalError } from '../errors';

const addCompany = async company => {
  try {
    const result = await fetchData('/api/companies', {
      method: 'POST',
      body: JSON.stringify(company),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    return result;
  } catch (error) {
    // TODO: handle errors better
    throw buildFinalError(error);
  }
};

export default addCompany;
