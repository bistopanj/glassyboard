import { fetchData } from '../utils/api-utils';
import { buildFinalError } from '../errors';

const addBoard = async boardData => {
  try {
    const result = await fetchData('/api/boards', {
      method: 'POST',
      body: JSON.stringify(boardData),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    return result;
  } catch (error) {
    // TODO: handle errors better
    throw buildFinalError(error);
  }
};

export default addBoard;
