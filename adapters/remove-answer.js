import { buildFinalError } from '../errors';
import { fetchData } from '../utils/api-utils';

const removeAnswer = async answerId => {
  try {
    const result = await fetchData(`/api/answers/${answerId}`, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
      },
    });
    return result;
  } catch (error) {
    // TODO: handle errors better
    throw buildFinalError(error);
  }
};

export default removeAnswer;
