import { fetchData } from '../utils/api-utils';
import { buildFinalError } from '../errors';

const getTicket = async (loginInfo, url = '/api/auth/tickets') => {
  try {
    await fetchData(url, {
      method: 'POST',
      body: JSON.stringify(loginInfo),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
  } catch (error) {
    const finalError = buildFinalError(error, 1);
    throw finalError;
  }
};

export default getTicket;
