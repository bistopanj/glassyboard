import { fetchData } from '../utils/api-utils';
import { buildFinalError } from '../errors';

const getTicket = async (loginInfo, url = '/api/tickets') => {
  try {
    const res = await fetchData(url, {
      method: 'POST',
      body: JSON.stringify(loginInfo),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    return res;
  } catch (error) {
    const finalError = buildFinalError(error, 1);
    throw finalError;
  }
};

export default getTicket;
