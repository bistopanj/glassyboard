import { buildFinalError } from '../errors';
import { fetchData } from '../utils/api-utils';

const getPanels = async (abortSignal = undefined, cookie = undefined, url = '/api/panels') => {
  try {
    const panels = await fetchData(url, {
      signal: abortSignal,
      headers: cookie ? { cookie } : undefined,
    });
    return panels;
  } catch (error) {
    // TODO: handle different errors better
    throw buildFinalError(error, 1);
  }
};

export default getPanels;
