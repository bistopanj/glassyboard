import { fetchData } from '../utils/api-utils';
import { buildFinalError } from '../errors';

const authenticateWithTicket = async (
  ticketId,
  { abortSignal = undefined, cookie = undefined, pathInfo = undefined }
) => {
  try {
    const headers = { Accept: 'application/json', 'Content-Type': 'application/json' };
    if (cookie) headers.cookie = cookie;
    const result = await fetchData(
      '/api/auth/ticket',
      {
        method: 'POST',
        body: JSON.stringify({ ticketId }),
        headers,
      },
      pathInfo
    );
    return result;
  } catch (error) {
    throw buildFinalError(error);
  }
};

export default authenticateWithTicket;
