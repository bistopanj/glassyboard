import { buildFinalError } from '../errors';
import { fetchData } from '../utils/api-utils';

const updatePanel = async (id, newData) => {
  try {
    const updatedPanel = await fetchData(`/api/panels/${id}`, {
      method: 'PUT',
      body: JSON.stringify(newData),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    return updatedPanel;
  } catch (error) {
    // TODO: handle errors better
    throw buildFinalError(error);
  }
};

export default updatePanel;
