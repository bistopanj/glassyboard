import { buildFinalError } from '../errors';
import { fetchData } from '../utils/api-utils';

const addAnswer = async answer => {
  try {
    const result = await fetchData('/api/answers', {
      method: 'POST',
      body: JSON.stringify(answer),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    return result;
  } catch (error) {
    // TODO: handle errors better
    throw buildFinalError(error);
  }
};

export default addAnswer;
