import authenticate from './authenticate';
import addBoard from './add-board';
import getTicket from './get-ticket';
import getBoardBySlug from './get-board-by-slug';
import getPanels from './get-panels';
import addAnswer from './add-answer';
import askTicket from './ask-ticket';
import upvoteAnswer from './upvote-answer';
import updateBoard from './update-board';
import removeAnswer from './remove-answer';

export {
  authenticate,
  getTicket,
  getBoardBySlug,
  getPanels,
  addBoard,
  addAnswer,
  askTicket,
  upvoteAnswer,
  updateBoard,
  removeAnswer,
};
