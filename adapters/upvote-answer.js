import { buildFinalError } from '../errors';
import { fetchData } from '../utils/api-utils';

const upvoteAnswer = async ({ answerId, boardId, slug }) => {
  try {
    const upvoteState = await fetchData('/api/upvotes', {
      method: 'POST',
      body: JSON.stringify({ answerId, boardId, slug }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    return upvoteState;
  } catch (error) {
    // TODO: handle errors better
    throw buildFinalError(error);
  }
};

export default upvoteAnswer;
