import { fetchData } from '../utils/api-utils';
import { buildFinalError } from '../errors';

const authenticateWithGoogle = async (
  { googleToken, data = undefined },
  { pathInfo = undefined }
) => {
  try {
    const result = await fetchData(
      '/api/auth/google',
      {
        method: 'POST',
        body: JSON.stringify({ googleToken, data }),
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
      pathInfo
    );
    return result;
  } catch (error) {
    throw buildFinalError(error);
  }
};

export default authenticateWithGoogle;
