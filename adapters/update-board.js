import { buildFinalError } from '../errors';
import { fetchData } from '../utils/api-utils';

const updateBoard = async (slug, newData) => {
  try {
    const updatedBoard = await fetchData(`/api/boards/${slug}`, {
      method: 'PUT',
      body: JSON.stringify(newData),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    return updatedBoard;
  } catch (error) {
    // TODO: handle errors better
    throw buildFinalError(error);
  }
};

export default updateBoard;
