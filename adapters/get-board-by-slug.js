import { fetchData } from '../utils/api-utils';
import { buildFinalError } from '../errors';

const getBoardBySlug = async (slug, abortSignal = undefined, cookie, url = '/api/boards') => {
  try {
    const boardData = await fetchData(`${url}/${encodeURIComponent(slug)}`, {
      signal: abortSignal,
      headers: cookie ? { cookie } : undefined,
    });
    return boardData;
  } catch (error) {
    // TODO: handle different error cases
    throw buildFinalError(error, 1);
  }
};

export default getBoardBySlug;
