import { buildFinalError } from '../errors';
import { fetchData } from '../utils/api-utils';

const addBoardToPanel = async (boardSlug, panelId) => {
  try {
    const result = await fetchData(`/api/panels/${panelId}/boards`, {
      method: 'POST',
      body: JSON.stringify({ boardSlug }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    return result;
  } catch (error) {
    throw buildFinalError(error);
  }
};

export default addBoardToPanel;
