import { fetchData } from '../utils/api-utils';
import { buildFinalError } from '../errors';

const authenticate = async ticketId => {
  try {
    const result = await fetchData('/api/auth', {
      method: 'POST',
      body: JSON.stringify({ ticketId }),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });
    return result;
  } catch (error) {
    throw buildFinalError(error, 1003, { 401: 1001, 404: 1002 });
  }
};

export default authenticate;
