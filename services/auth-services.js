import jwt from 'jsonwebtoken';
import { upsertUser } from './user-services';
import ServerError from '../errors/server-error';
import { buildFinalServerError } from '../errors/build-final-error';
import { parseServerError } from '../errors';

const ownerEmail = process.env.OWNER_EMAIL;
const jwtSecret = process.env.JWT_SECRET;
const defaultOptions = { expiresIn: '60 days', issuer: 'glassyboard.com' };

export const generateToken = async (user, options = defaultOptions) => {
  return new Promise((resolve, reject) => {
    const {
      ex_id: id,
      first_name: firstName,
      last_name: lastName,
      created_at: createdAt,
      email,
    } = user;
    const exUser = { id, firstName, lastName, createdAt: `${createdAt}`, email };
    const payload = { user: exUser };
    jwt.sign(payload, jwtSecret, options, (err, token) => {
      if (err)
        reject(
          new ServerError({
            code: 2002,
            status: 500,
            message: err.message || 'error in generating jwt token',
          })
        );
      else resolve({ token, user: exUser });
    });
  });
};

export const issueToken = async intUser => {
  try {
    const { token, user } = await generateToken(intUser);
    // TODO: probably save the token in db
    return { token, user };
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export const loginUser = async (userInfo = {}) => {
  try {
    const intUser = await upsertUser(userInfo);
    const { token, user } = await issueToken(intUser);
    return { token, user };
  } catch (err) {
    console.log(err);
    throw err;
  }
};

const extractTokenFromReq = req => {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      const tokenInCookie = req.cookies.token;
      return tokenInCookie || null;
    }
    const authHeaderParts = authHeader.split(' ');
    if (authHeaderParts[0] !== 'Bearer') throw new Error('');
    return authHeaderParts[1];
  } catch (err) {
    return null;
  }
};

export const verifyToken = async token => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, jwtSecret, (err, decoded) => {
      if (err)
        reject(
          new ServerError({
            status: 401,
            code: 2005,
            message: err.message || 'jwt token is invalid',
          })
        );
      else resolve(decoded);
    });
  });
};

export const extractUserFromReq = async req => {
  try {
    const token = extractTokenFromReq(req);
    if (!token) return null;
    const payload = await verifyToken(token);
    const { user } = payload;
    return user;
  } catch (error) {
    // TODO: handle the error better
    const finalError = buildFinalServerError(error);
    throw finalError;
  }
};

export const allowOwner = (filter, user) => {
  const { email } = user;
  if (ownerEmail && email === ownerEmail) {
    const { createdBy, ...rest } = filter;
    return rest;
  }
  return filter;
};

export const limitToOwner = job => (payload, userInfo) => {
  try {
    const {
      user: { email },
    } = userInfo;

    if (email !== ownerEmail)
      throw new ServerError({
        code: 2006,
        status: 403,
        message: "You don't have the required privileges",
      });

    return job(payload, userInfo);
  } catch (error) {
    const finalError = buildFinalServerError(error, 2006);
    throw finalError;
  }
};

export const authMiddleware = reqHandler => async (req, res) => {
  try {
    const token = extractTokenFromReq(req);
    if (!token)
      throw new ServerError({
        code: 2004,
        status: 401,
        message: 'you should provide a valid token',
      });
    const payload = await verifyToken(token);
    const { user } = payload;
    req.user = user;
    return reqHandler(req, res);
  } catch (error) {
    const finalError = buildFinalServerError(error, 2004);
    const [status, body] = parseServerError(finalError);
    return res.status(status).json(body);
  }
};
