import { query } from '../dbs/pg-db';
import upsertUserQuery from '../dbs/queries/upsert-user';
import translateDbError from '../errors/translate-db-error';

export const upsertUser = async ({ email, firstName = null, lastName = null }) => {
  try {
    const { rows } = await query(upsertUserQuery, [email, firstName, lastName]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const users = {};
