import Mustache from 'mustache';
import templates from '../templates/email';

const findTemplate = template => (template in templates ? templates[template] : templates.sample);

export const generateEmail = ({ template = 'sample', data = {} }) => {
  const { Subject, TextPart, HTMLPart } = findTemplate(template);
  return {
    Subject: Mustache.render(Subject, data),
    TextPart: Mustache.render(TextPart, data),
    HTMLPart: Mustache.render(HTMLPart, data),
  };
};

export default generateEmail;
