import { translateDbError } from '../errors';
import { query } from '../dbs/pg-db';
import {
  insertCompanyQuery,
  findCompanyByExIdQuery,
  deleteCompanyByExIdQuery,
  updateCompanyPrimaryBoardQuery,
} from '../dbs/queries';

export const createCompany = async ({ subdomain, createdBy }) => {
  try {
    const { rows } = await query(insertCompanyQuery, [subdomain, createdBy]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const updateCompanyPrimaryBoard = async ({ companyId, boardId }) => {
  try {
    const { rows } = await query(updateCompanyPrimaryBoardQuery, [companyId, boardId]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const findCompanyById = async exId => {
  try {
    const { rows } = await query(findCompanyByExIdQuery, [exId]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const deleteCompanyById = async exId => {
  try {
    const res = await query(deleteCompanyByExIdQuery, [exId]);
    return res;
  } catch (error) {
    throw translateDbError(error);
  }
};
