import { v4 as uuidv4 } from 'uuid';
import redisClient from '../dbs/redis-cache';
import ServerError from '../errors/server-error';
import { buildFinalServerError } from '../errors/build-final-error';
import { raiseJob } from './job-services';

const ticketRedisKeyPrefix = 'TICKET:';

export const ticketExpirationHours = 12;

export const ticketExpirationSec = ticketExpirationHours * 60 * 60;

export const getRedisKey = ticketId => `${ticketRedisKeyPrefix}${ticketId}`;

export const generateTicket = async loginInfo => {
  try {
    const uuid = uuidv4();
    const ticketId = Buffer.from(uuid).toString('base64');
    await redisClient.setexAsync(
      getRedisKey(ticketId),
      ticketExpirationSec,
      JSON.stringify(loginInfo)
    );
    return ticketId;
  } catch (error) {
    throw new ServerError({
      code: 2003,
      status: 500,
      message: error.message || 'something went wrong when trying to generate a new ticket',
      moreInfo: '',
    });
  }
};

export const consumeTicket = async ({ ticketId, ticketInfo = {}, token, user }) => {
  const { ticketAction, payload = {} } = ticketInfo || {};
  if (ticketAction) {
    const [jobResult, redirectTo] = await raiseJob(
      { job: ticketAction, payload },
      { token, ticketId, user }
    );
    await redisClient.delAsync(getRedisKey(ticketId));
    return [jobResult, redirectTo];
  }
  await redisClient.delAsync(getRedisKey(ticketId));
  return [null, '/'];
};

export const validateTicket = async ticketId => {
  try {
    const loginInfoStr = await redisClient.getAsync(getRedisKey(ticketId));
    if (!loginInfoStr)
      throw new ServerError({
        code: 1001,
        message: 'authentication failed',
        moreInfo:
          'the given ticket is expired or wrong. please try to login again and get a new ticket.',
        status: 403,
      });
    const loginInfo = JSON.parse(loginInfoStr);
    return loginInfo;
  } catch (err) {
    throw buildFinalServerError(err, 2000);
  }
};
