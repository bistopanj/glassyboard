import { translateDbError } from '../errors';
import { query } from '../dbs/pg-db';
import {
  insertAnswerQuery,
  findAnswerByExIdQuery,
  deleteAnswerByExIdQuery,
  deactivateAnswerQuery,
} from '../dbs/queries';

export const expose = (answerData, createdBy = undefined, boardId = undefined) => {
  const {
    id,
    ex_id: externalId,
    created_by: __,
    board_id: ___,
    active: ____,
    ...rest
  } = answerData;
  return { id: externalId, created_by: createdBy, board_id: boardId, ...rest };
};

export const findAnswerById = async (exId, internal = false) => {
  try {
    const { rows } = await query(findAnswerByExIdQuery, [exId]);
    return internal ? rows[0] : expose(rows[0]);
  } catch (error) {
    throw translateDbError(error);
  }
};

export const deleteAnswerById = async exId => {
  try {
    const res = await query(deleteAnswerByExIdQuery, [exId]);
    return res;
  } catch (error) {
    throw translateDbError(error);
  }
};

export const insertAnswer = async ({ answer, createdBy, boardId }) => {
  try {
    const { rows } = await query(insertAnswerQuery, [answer, createdBy, boardId]);
    return expose(rows[0], createdBy, boardId);
  } catch (error) {
    throw translateDbError(error);
  }
};

export const deactivateAnswer = async filter => {
  try {
    const { createdBy = '00000000-0000-0000-0000-000000000000', id } = filter;
    const { rows } = await query(deactivateAnswerQuery, [id, createdBy]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};
