import { translateDbError } from '../errors';
import { query } from '../dbs/pg-db';
import { insertUpvoteQuery, findUpvoteByExIdQuery, deleteUpvoteByExIdQuery } from '../dbs/queries';

export const insertUpvote = async ({ answerId, boardId, createdBy }) => {
  try {
    const { rows } = await query(insertUpvoteQuery, [answerId, boardId, createdBy]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const findUpvoteById = async exId => {
  try {
    const { rows } = await query(findUpvoteByExIdQuery, [exId]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const deleteUpvoteById = async exId => {
  try {
    const res = await query(deleteUpvoteByExIdQuery, [exId]);
    return res;
  } catch (error) {
    throw translateDbError(error);
  }
};
