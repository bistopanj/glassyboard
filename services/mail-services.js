import Mailjet from 'node-mailjet';
import { generateEmail } from './template-services';

const mailjet = Mailjet.connect(
  process.env.MAILJET_APIKEY_PUBLIC,
  process.env.MAILJET_APIKEY_PRIVATE
);
export const sender = mailjet.post('send', { version: 'v3.1' });

const senderEmail = 'test-postman@glassyboard.com';
const senderName = 'GlassyBoard Postman';

// for tests, run the mailjet in sandbox mode.
const SandboxMode = process.env.NODE_ENV === 'test';

export const sendEmail = async ({ to, template, data = {}, customId = undefined }) => {
  try {
    const emailContent = generateEmail({ template, data });
    const result = await sender.request({
      SandboxMode,
      Messages: [
        {
          From: {
            Email: senderEmail,
            Name: senderName,
          },
          To: [
            {
              Email: to,
            },
          ],
          ...emailContent,
          CustomID: customId,
        },
      ],
    });
    const {
      Messages: [
        {
          Status: status,
          Errors: errors,
          To: [
            {
              Email: email,
              MessageUUID: messageUUID,
              MessageID: messageId,
              MessageHref: messageHref,
            },
          ],
        },
      ],
    } = result.body;
    const sendResult = { status, errors, email, messageUUID, messageId, messageHref };
    return sendResult;
  } catch (error) {
    // TODO: handle errors
    console.log(error);
    throw error;
  }
};
