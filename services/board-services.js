import buildSlug from '../utils/slug-utils';
import { query } from '../dbs/pg-db';
import {
  insertBoardQuery,
  insertCompanyBoardQuery,
  findBoardByExIdQuery,
  findBoardBySubdomainQuery,
  deleteBoardByExIdQuery,
  findBoardBySlugQuery,
  getFeaturedPanelQuery,
  updateBoardBySlugQuery,
} from '../dbs/queries';
import { translateDbError, buildFinalServerError } from '../errors';
import ServerError from '../errors/server-error';

const defaultCompanyBoardTopic = 'Suggestions';

export const panelQueries = {
  featured: getFeaturedPanelQuery,
};

export const expose = (boardData, userExId = undefined) => {
  const { id, ex_id: externalId, created_by: __, active: ___, ...rest } = boardData;
  return { id: externalId, created_by: userExId, ...rest };
};

export const createBoard = async ({ topic, createdBy }) => {
  try {
    const slug = buildSlug(topic);
    const { rows } = await query(insertBoardQuery, [topic, createdBy, slug]);
    return expose(rows[0], createdBy);
  } catch (error) {
    throw translateDbError(error);
  }
};

export const createCompanyBoard = async ({
  topic = defaultCompanyBoardTopic,
  createdBy,
  company,
}) => {
  try {
    const slug = buildSlug(`default-company-board-${company.subdomain}`);
    const { rows } = await query(insertCompanyBoardQuery, [topic, createdBy, slug, company.id]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const updateBoard = async (filter, updateValues) => {
  try {
    const { createdBy = '00000000-0000-0000-0000-000000000000', slug } = filter;
    const { topic } = updateValues;
    const { rows } = await query(updateBoardBySlugQuery, [topic, slug, createdBy]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const findBoardById = async exId => {
  try {
    const { rows } = await query(findBoardByExIdQuery, [exId]);
    return expose(rows[0]);
  } catch (error) {
    throw translateDbError(error);
  }
};

export const findBoardBySlug = async (slug, user = undefined) => {
  try {
    const { id: userId = null } = user || {};
    const { rows } = await query(findBoardBySlugQuery, [slug, userId]);
    if (rows.length === 0)
      throw new ServerError({
        status: 404,
        code: 2051,
        message: `board with slug ${slug} not found`,
      });
    return rows[0];
  } catch (error) {
    let err = error;
    if (!(err instanceof ServerError)) err = translateDbError(error);
    throw buildFinalServerError(err, 2050);
  }
};

export const findCompanyBoard = async (subdomain, user = undefined) => {
  try {
    const { id: userId = null } = user || {};
    const { rows } = await query(findBoardBySubdomainQuery, [subdomain, userId]);
    if (rows.length === 0)
      throw new ServerError({
        status: 404,
        code: 2051,
        message: `no board found for company ${subdomain}`,
      });
    return rows[0];
  } catch (error) {
    let err = error;
    if (!(err instanceof ServerError)) err = translateDbError(error);
    throw buildFinalServerError(err, 2050);
  }
};

export const deleteBoardById = async exId => {
  try {
    const res = await query(deleteBoardByExIdQuery, [exId]);
    return res;
  } catch (error) {
    throw translateDbError(error);
  }
};

export const someBoards = [];
