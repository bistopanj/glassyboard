import jwt from 'jsonwebtoken';
import fetch from 'cross-fetch';
import ServerError from '../errors/server-error';

const googleCertsUrl = 'https://www.googleapis.com/oauth2/v1/certs';
const googleClientId = process.env.GOOGLE_SIGN_IN_CLIENT_ID;

const getGoogleCerts = async () => {
  const response = await fetch(googleCertsUrl);
  return response.json();
};

export const verifyGoogleIdToken = (idToken, cert) =>
  new Promise((resolve, reject) => {
    jwt.verify(
      idToken,
      cert,
      { audience: googleClientId, issuer: ['accounts.google.com', 'https://accounts.google.com'] },
      (error, payload) => {
        if (error) {
          reject(
            new ServerError({
              code: 1004,
              message: 'authentication failed',
              moreInfo: 'the given google id token is expired or wrong. please try to login again.',
              status: 403,
            })
          );
          return;
        }
        resolve(payload);
      }
    );
  });

export const validateGoogleIdToken = async googleIdToken => {
  try {
    const certs = await getGoogleCerts();
    const { header } = jwt.decode(googleIdToken, { complete: true });
    const cert = certs[header.kid];
    const payload = await verifyGoogleIdToken(googleIdToken, cert);
    const {
      email,
      email_verified: emailVerified,
      given_name: firstName,
      family_name: lastName,
      picture: profilePicture,
      locale,
    } = payload;
    if (!emailVerified) throw new Error('The email is not verified');
    return { email, firstName, lastName, profilePicture, locale };
  } catch (error) {
    throw new ServerError({
      code: 1004,
      message: error.message || 'authentication failed',
      moreInfo: 'the given google id token is expired or wrong. please try to login again.',
      status: 403,
    });
  }
};
