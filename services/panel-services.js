import ServerError from '../errors/server-error';
import { translateDbError, buildFinalServerError } from '../errors';
import { query } from '../dbs/pg-db';
import {
  getFeaturedPanelsQuery,
  insertPanelQuery,
  findPanelByExIdQuery,
  findPanelBoardRelByExIdQuery,
  deactivatePanelBoardRelQuery,
  deletePanelByExIdQuery,
  deletePanelBoardRelByExIdQuery,
  upsertPanelBoardRelQuery,
  updatePanelByExIdQuery,
} from '../dbs/queries';

export const fetchFeaturedPanels = async (user = undefined) => {
  try {
    const { id: userId = null } = user || {};
    const { rows } = await query(getFeaturedPanelsQuery, [userId]);
    return rows;
  } catch (error) {
    let err = error;
    if (!(err instanceof ServerError)) err = translateDbError(error);
    throw buildFinalServerError(err);
  }
};

export const createPanel = async ({ title, featured = false, createdBy }) => {
  try {
    const { rows } = await query(insertPanelQuery, [title, featured, createdBy]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const updatePanel = async (filter, updateValues) => {
  try {
    const { createdBy = '00000000-0000-0000-0000-000000000000', id } = filter;
    const { title } = updateValues;
    const { rows } = await query(updatePanelByExIdQuery, [title, id, createdBy]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const findPanelById = async exId => {
  try {
    const { rows } = await query(findPanelByExIdQuery, [exId]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const deletePanelById = async exId => {
  try {
    const res = await query(deletePanelByExIdQuery, [exId]);
    return res;
  } catch (error) {
    throw translateDbError(error);
  }
};

export const upsertPanelBoardRel = async ({ boardSlug, panelId, createdBy }) => {
  try {
    const { rows } = await query(upsertPanelBoardRelQuery, [boardSlug, panelId, createdBy]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const findPanelBoardRelById = async exId => {
  try {
    const { rows } = await query(findPanelBoardRelByExIdQuery, [exId]);
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};

export const deletePanelBoardRelById = async exId => {
  try {
    const res = await query(deletePanelBoardRelByExIdQuery, [exId]);
    return res;
  } catch (error) {
    throw translateDbError(error);
  }
};

export const deactivatePanelBoardRel = async ({ boardSlug, panelId }) => {
  try {
    const { rows } = await query(deactivatePanelBoardRelQuery, [boardSlug, panelId]);
    if (rows.length === 0)
      throw new ServerError({
        status: 404,
        code: 2052,
        message: 'The board is not in the panel',
      });
    return rows[0];
  } catch (error) {
    throw translateDbError(error);
  }
};
