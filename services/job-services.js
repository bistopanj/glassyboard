import * as jobs from '../jobs';
import { buildFinalServerError } from '../errors/build-final-error';

export const jobHandlers = {
  upvote: jobs.upvote,
  'add-board': jobs.addBoard,
  'add-company': jobs.addCompany,
  'update-board': jobs.updateBoard,
  'add-answer': jobs.addAnswer,
  'add-panel': jobs.addPanel,
  'add-board-to-panel': jobs.addBoardToPanel,
  'remove-board-from-panel': jobs.removeBoardFromPanel,
  'deactivate-answer': jobs.deactivateAnswer,
  'update-panel': jobs.updatePanel,
};

export const raiseJob = async ({ job, payload }, userInfo) => {
  try {
    const jobTitle = job.toLowerCase();
    if (jobTitle in jobHandlers) return jobHandlers[jobTitle](payload, userInfo);
    return [null, '/'];
  } catch (error) {
    throw buildFinalServerError(error, 2100);
  }
};
