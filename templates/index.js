import emailTemplates from './email';

const templates = { email: emailTemplates };

export default templates;
export { emailTemplates };
