const baseUrl = process.env.BASE_URL;

export default {
  Subject: 'Glassy Board: Please confirm your email address',
  TextPart: `To verify your account, please click on the link below: \n\n ${baseUrl}/auth/ticket?ticketId={{ticketId}}`,
  HTMLPart: `<p>Dear user,</p><p>To confirm your email address, please click on the link below:</p><p>${baseUrl}/auth/ticket?ticketId={{ticketId}}</p>`,
};
