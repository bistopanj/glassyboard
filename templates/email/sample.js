export default {
  Subject: 'sample {{test}}',
  TextPart: 'sample text part {{test}}',
  HTMLPart: '<p>sample html part {{test}}</p>',
};
