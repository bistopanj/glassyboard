import sample from './sample';
import ticket from './ticket';

const templates = {
  sample,
  ticket,
};

export default templates;
// export { sample, ticket };
