const errorTypes = {
  ApiError: 'ApiError',
  UnknownError: 'UnknownError',
  ServerError: 'ServerError',
};

export default errorTypes;
