import ServerError from './server-error';

const parseServerError = error => {
  try {
    if (error instanceof ServerError) return [error.status, { error: error.body }];
    throw new Error('The error in not raised in correct form');
  } catch (err) {
    return [
      500,
      {
        error: {
          code: 1199,
          message: 'The error in not raised in correct form',
          moreInfo: 'refer to error code',
          type: 'ServerError',
        },
      },
    ];
  }
};

export default parseServerError;
