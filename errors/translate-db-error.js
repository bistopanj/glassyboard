import ServerError from './server-error';

// const pgErrorCodeMap = {
//   '22P02': { status: 400, code: 2201 },
// };

const pgErrorClassMap = {
  '00': { status: 418, code: 2202, message: 'Successful Completion' },
  '01': { status: 418, code: 2203, message: 'Warning' },
  '02': { status: 418, code: 2204, message: 'No Data' },
  '03': { status: 500, code: 2205, message: 'SQL Statement Not Yet Complete' },
  '08': { status: 500, code: 2206, message: 'Connection Exception' },
  '09': { status: 500, code: 2207, message: 'Triggered Action Exception' },
  '0A': { status: 500, code: 2208, message: 'Feature Not Supported' },
  '0B': { status: 500, code: 2209, message: 'Invalid Transaction Initiation' },
  '0F': { status: 500, code: 2210, message: 'Locator Exception' },
  '0L': { status: 500, code: 2211, message: 'Invalid Grantor' },
  '0P': { status: 500, code: 2212, message: 'Invalid Role Specification' },
  '0Z': { status: 500, code: 2213, message: 'Diagnostics Exception' },
  '20': { status: 404, code: 2214, message: 'Case Not Found' },
  '21': { status: 500, code: 2215, message: 'Cardinality Violation' },
  '22': { status: 400, code: 2201, message: 'Data Exception' },
  '23': { status: 400, code: 2216, message: 'Integrity Constraint Violation' },
  '24': { status: 500, code: 2217, message: 'Invalid Cursor State' },
  '25': { status: 500, code: 2218, message: 'Invalid Transaction State' },
  '26': { status: 500, code: 2219, message: 'Invalid SQL Statement Name' },
  '27': { status: 500, code: 2220, message: 'Triggered Data Change Violation' },
  '28': { status: 500, code: 2221, message: 'Invalid Authorization Specification' },
  '2B': { status: 500, code: 2222, message: 'Dependent Privilege Descriptors Still Exist' },
  '2D': { status: 500, code: 2223, message: 'Invalid Transaction Termination' },
  '2F': { status: 500, code: 2224, message: 'SQL Routine Exception' },
  '34': { status: 500, code: 2225, message: 'Invalid Cursor Name' },
  '38': { status: 500, code: 2226, message: 'External Routine Exception' },
  '39': { status: 500, code: 2227, message: 'External Routine Invocation Exception' },
  '3B': { status: 500, code: 2228, message: 'Savepoint Exception' },
  '3D': { status: 500, code: 2229, message: 'Invalid Catalog Name' },
  '3F': { status: 500, code: 2230, message: 'Invalid Schema Name' },
  '40': { status: 500, code: 2231, message: 'Transaction Rollback' },
  '42': { status: 500, code: 2232, message: 'Syntax Error or Access Rule Violation' },
  '44': { status: 500, code: 2233, message: 'WITH CHECK OPTION Violation' },
  '53': { status: 500, code: 2234, message: 'Insufficient Resources' },
  '54': { status: 500, code: 2235, message: 'Program Limit Exceeded' },
  '55': { status: 500, code: 2236, message: 'Object Not In Prerequisite State' },
  '57': { status: 500, code: 2237, message: 'Operator Intervention' },
  '58': { status: 500, code: 2238, message: 'System Error (errors external to PostgreSQL itself)' },
  '72': { status: 500, code: 2239, message: 'Snapshot Failure' },
  F0: { status: 500, code: 2240, message: 'Configuration File Error' },
  HV: { status: 500, code: 2241, message: 'Foreign Data Wrapper Error (SQL/MED)' },
  P0: { status: 500, code: 2242, message: 'PL/pgSQL Error' },
  XX: { status: 500, code: 2243, message: 'Internal Error' },
};

const translateDbError = dbError => {
  try {
    const { code } = dbError;
    const errorClass = code.substring(0, 2);
    return new ServerError({
      code: pgErrorClassMap[errorClass].code,
      status: pgErrorClassMap[errorClass].status,
      message: dbError.message || `${code}:${pgErrorClassMap[errorClass].message}`,
    });
  } catch (error) {
    return new ServerError({
      code: 2200,
      status: 500,
      message: dbError.message || 'generic database error',
    });
  }
};

export default translateDbError;
