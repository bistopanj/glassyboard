import UnknownError from './unknown-error';
import ServerError from './server-error';

export const buildFinalError = (error, replaceUnknownWith = 1, statusMap = {}) => {
  let errorMessage = '';
  try {
    errorMessage = error.message;
    const { code, status } = error.toJSON();
    if (code === 1) {
      if (status && status in statusMap) {
        error.updateCode(statusMap[status]);
      } else if (replaceUnknownWith !== 1) {
        error.updateCode(replaceUnknownWith);
      }
    }
    return error;
  } catch (err) {
    return new UnknownError(errorMessage || err.message);
  }
};

export const buildFinalServerError = (error, replaceUnknownWith = 1, statusMap = {}) => {
  const finalError = buildFinalError(error, replaceUnknownWith, statusMap);
  if (finalError instanceof ServerError) return finalError;
  return new ServerError({ status: 500, ...finalError.toJSON() });
};

// export default buildFinalError;
