import CodedError from './coded-error';

class ServerError extends CodedError {
  constructor(props) {
    super(props);
    const { status, moreInfo = '' } = props;
    this.type = 'ServerError';
    this.status = status;
    this.moreInfo = moreInfo;
  }

  get body() {
    const { status, ...rest } = this.toJSON();
    return rest;
  }
}

export default ServerError;
