class CodedError extends Error {
  constructor(props) {
    if (typeof props === 'string') {
      super(props);
      this.message = props;
      this.code = 1;
    } else {
      const { message = '', code = 1 } = props || {};
      super(message);
      this.message = message;
      this.code = code;
    }
  }

  updateCode(newCode) {
    this.code = newCode;
  }

  toJSON() {
    return {
      ...this,
      message: this.message,
    };
  }
}

export default CodedError;
