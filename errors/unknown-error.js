import CodedError from './coded-error';

class UnknownError extends CodedError {
  constructor(props) {
    super(props);
    this.type = 'UnknownError';
  }
}

export default UnknownError;
