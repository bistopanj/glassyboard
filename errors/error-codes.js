import errorTypes from './error-types';

const errorCodes = {
  1: {
    description: 'general error',
    moreInfo: 'this is the general error generated without providing an error code',
    toUser: '',
    type: errorTypes.UnknownError,
  },
  1001: {
    description: 'authentication failed',
    moreInfo:
      'probably because the given token is expired or wrong. The user should try to log in again.',
    toUser: '',
    type: errorTypes.ApiError,
  },
  1002: {
    description: 'authentication service unavailable',
    moreInfo: 'our authentication api is not available. The user should try again later',
    toUser: '',
    type: errorTypes.ApiError,
  },
  1003: {
    description: 'authentication failed for unknown reason',
    moreInfo: 'probably there was a problem in internet connection',
    toUser: '',
    type: errorTypes.UnknownError,
  },
  1004: {
    description: 'google authentication failed',
    moreInfo: 'Probably the given google id token was wrong or expired',
    toUser: '',
    type: errorTypes.ServerError,
  },
  1199: {
    description: 'The error is not a valid Error',
    moreInfo:
      'This error happens at the last step of building the api response. All errors in previous stages of the server must be thrown as an instance of ServerError class.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2000: {
    description: 'error in ticket validation process',
    moreInfo:
      'some internal error prevented us from processing the given authentication ticket. please try again later.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2001: {
    description: 'error in upserting user in database',
    moreInfo:
      'some internal error prevented us from identifying the user. The user should try again later',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2002: {
    description: 'error in generating jwt token',
    moreInfo:
      'some internal error prevented us from issuing a valid authentication token for the user. The user should try again later',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2003: {
    description: 'error in generating new ticket',
    moreInfo:
      'Due to internal problems in services.ticket.generateTicket we were unable to generate a new ticket. The user should try again later.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2004: {
    description: 'no authorization token is provided',
    moreInfo:
      'Access to this route is limited. A valid authorization token is required but none provided in the request.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2005: {
    description: 'authetication token is invalid',
    moreInfo:
      'It may be expired or in incorrect form. Search the internet for the correct way to supply the json web token to an api.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2006: {
    description: 'not enough privilege',
    moreInfo: 'The resource you are trying to reach is limited.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2050: {
    description: 'general error in finding board',
    moreInfo: 'something went wrong when trying to find a board',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2051: {
    description: 'the specified board does not exist',
    moreInfo: 'The slug provided to access a board is wrong or does not exist',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2052: {
    description: 'the specified board does not exist in the panel',
    moreInfo: 'The relationship does not exist',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2053: {
    description: 'more data is needed to find a board',
    moreInfo: 'The query parameters provided are not supported',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2060: {
    description: 'cannot update the board',
    moreInfo: "Either you don't have the required permission or the board does not exist",
    toUser: '',
    type: errorTypes.ServerError,
  },
  2061: {
    description: 'cannot delete the answer',
    moreInfo: "Either you don't have the required permission or the answer does not exist",
    toUser: '',
    type: errorTypes.ServerError,
  },
  2062: {
    description: 'cannot update the panel',
    moreInfo: "Either you don't have the required permission or the panel does not exist",
    toUser: '',
    type: errorTypes.ServerError,
  },
  2100: {
    description: 'error in handling jobs',
    moreInfo:
      'Due to internal problems in jobs being handled at services.job, we were unable to finishe the job.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2200: {
    description: 'generic database error',
    moreInfo: 'Some error occurred in database operation',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2201: {
    description: 'database error: data exception',
    moreInfo: 'There is some problem in the input.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2202: {
    description: 'database error: successful completion',
    moreInfo: 'There is no error!',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2203: {
    description: 'database error: warning',
    moreInfo:
      'There is a warning in database operation. Please refer to the exact db error code provided.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2204: {
    description: 'database error: no data',
    moreInfo:
      'There is a warning in database operation. Please refer to the exact db error code provided.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2205: {
    description: 'database error: sql statement not yet complete',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2206: {
    description: 'database error: connection exception',
    moreInfo: 'There is a problem connecting to database.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2207: {
    description: 'database error: triggered action exception',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2208: {
    description: 'database error: feature not supported',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2209: {
    description: 'database error: invalid transaction initiation',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2210: {
    description: 'database error: locator exception',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2211: {
    description: 'database error: invalid grantor',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2212: {
    description: 'database error: invalid role specification',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2213: {
    description: 'database error: diagnostics exception',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2214: {
    description: 'database error: case not found',
    moreInfo: 'The resource not found.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2215: {
    description: 'database error: cardinality violation',
    moreInfo: 'The resource not found.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2216: {
    description: 'database error: integrity constraint violation',
    moreInfo: 'Some constraints on database violated.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2217: {
    description: 'database error: invalid cursor state',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2218: {
    description: 'database error: invalid transaction state',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2219: {
    description: 'database error: invalid sql statement name',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2220: {
    description: 'database error: triggered data change violation',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2221: {
    description: 'database error: invalid authorization specification',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2222: {
    description: 'database error: dependent privilege descriptors still exist',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2223: {
    description: 'database error: invalid transaction termination',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2224: {
    description: 'database error: sql routine exception',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2225: {
    description: 'database error: invalid cursor name',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2226: {
    description: 'database error: external routine exception',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2227: {
    description: 'database error: external routine invocation exception',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2228: {
    description: 'database error: savepoint exception',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2229: {
    description: 'database error: invalid catalog name',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2230: {
    description: 'database error: invalid schema name',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2231: {
    description: 'database error: transaction rollback',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2232: {
    description: 'database error: syntax error or access rule violation',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2233: {
    description: 'database error: with check option violation',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2234: {
    description: 'database error: insufficient resources',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2235: {
    description: 'database error: program limit exceeded',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2236: {
    description: 'database error: object not in prerequisite state',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2237: {
    description: 'database error: operator intervention',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2238: {
    description: 'database error: system error',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2239: {
    description: 'database error: snapshot failure',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2240: {
    description: 'database error: configuration file error',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2241: {
    description: 'database error: foreign data wrapper error (sql/med)',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2242: {
    description: 'database error: pl/pgsql error',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
  2243: {
    description: 'database error: internal error',
    moreInfo: 'Please refer to the postgres documentation.',
    toUser: '',
    type: errorTypes.ServerError,
  },
};

export default errorCodes;
