import errorTypes from './error-types';
import CodedError from './coded-error';
import ApiError from './api-error';
import ServerError from './server-error';
import UnknownError from './unknown-error';

const errorClasses = {
  [errorTypes.ApiError]: ApiError,
  [errorTypes.CodedError]: CodedError,
  [errorTypes.UnknownError]: UnknownError,
  [errorTypes.ServerError]: ServerError,
};

export default errorClasses;
